import Vue from 'vue';

function functionalThemeClasses(context) {
  const vm = { ...context.props,
    ...context.injections
  };
  const isDark = Themeable.options.computed.isDark.call(vm);
  return Themeable.options.computed.themeClasses.call({
    isDark
  });
}
/* @vue/component */

const Themeable = Vue.extend().extend({
  name: 'themeable',

  provide() {
    return {
      theme: this.themeableProvide
    };
  },

  inject: {
    theme: {
      default: {
        isDark: false
      }
    }
  },
  props: {
    dark: {
      type: Boolean,
      default: null
    },
    light: {
      type: Boolean,
      default: null
    }
  },

  data() {
    return {
      themeableProvide: {
        isDark: false
      }
    };
  },

  computed: {
    appIsDark() {
      return this.$vuetify.theme.dark || false;
    },

    isDark() {
      if (this.dark === true) {
        // explicitly dark
        return true;
      } else if (this.light === true) {
        // explicitly light
        return false;
      } else {
        // inherit from parent, or default false if there is none
        return this.theme.isDark;
      }
    },

    themeClasses() {
      return {
        'theme--dark': this.isDark,
        'theme--light': !this.isDark
      };
    },

    /** Used by menus and dialogs, inherits from v-app instead of the parent */
    rootIsDark() {
      if (this.dark === true) {
        // explicitly dark
        return true;
      } else if (this.light === true) {
        // explicitly light
        return false;
      } else {
        // inherit from v-app
        return this.appIsDark;
      }
    },

    rootThemeClasses() {
      return {
        'theme--dark': this.rootIsDark,
        'theme--light': !this.rootIsDark
      };
    }

  },
  watch: {
    isDark: {
      handler(newVal, oldVal) {
        if (newVal !== oldVal) {
          this.themeableProvide.isDark = this.isDark;
        }
      },

      immediate: true
    }
  }
});

/* eslint-disable max-len, import/export, no-use-before-define */
function mixins(...args) {
  return Vue.extend({
    mixins: args
  });
}

/**
 * This mixin provides `attrs$` and `listeners$` to work around
 * vue bug https://github.com/vuejs/vue/issues/10115
 */

function makeWatcher(property) {
  return function (val, oldVal) {
    for (const attr in oldVal) {
      if (!Object.prototype.hasOwnProperty.call(val, attr)) {
        this.$delete(this.$data[property], attr);
      }
    }

    for (const attr in val) {
      this.$set(this.$data[property], attr, val[attr]);
    }
  };
}

var BindsAttrs = Vue.extend({
  data: () => ({
    attrs$: {},
    listeners$: {}
  }),

  created() {
    // Work around unwanted re-renders: https://github.com/vuejs/vue/issues/10115
    // Make sure to use `attrs$` instead of `$attrs` (confusing right?)
    this.$watch('$attrs', makeWatcher('attrs$'), {
      immediate: true
    });
    this.$watch('$listeners', makeWatcher('listeners$'), {
      immediate: true
    });
  }

});

function createMessage(message, vm, parent) {
  if (parent) {
    vm = {
      _isVue: true,
      $parent: parent,
      $options: vm
    };
  }

  if (vm) {
    // Only show each message once per instance
    vm.$_alreadyWarned = vm.$_alreadyWarned || [];
    if (vm.$_alreadyWarned.includes(message)) return;
    vm.$_alreadyWarned.push(message);
  }

  return `[Vuetify] ${message}` + (vm ? generateComponentTrace(vm) : '');
}
function consoleWarn(message, vm, parent) {
  const newMessage = createMessage(message, vm, parent);
  newMessage != null && console.warn(newMessage);
}
function consoleError(message, vm, parent) {
  const newMessage = createMessage(message, vm, parent);
  newMessage != null && console.error(newMessage);
}
function breaking(original, replacement, vm, parent) {
  consoleError(`[BREAKING] '${original}' has been removed, use '${replacement}' instead. For more information, see the upgrade guide https://github.com/vuetifyjs/vuetify/releases/tag/v2.0.0#user-content-upgrade-guide`, vm, parent);
}
function removed(original, vm, parent) {
  consoleWarn(`[REMOVED] '${original}' has been removed. You can safely omit it.`, vm, parent);
}
/**
 * Shamelessly stolen from vuejs/vue/blob/dev/src/core/util/debug.js
 */

const classifyRE = /(?:^|[-_])(\w)/g;

const classify = str => str.replace(classifyRE, c => c.toUpperCase()).replace(/[-_]/g, '');

function formatComponentName(vm, includeFile) {
  if (vm.$root === vm) {
    return '<Root>';
  }

  const options = typeof vm === 'function' && vm.cid != null ? vm.options : vm._isVue ? vm.$options || vm.constructor.options : vm || {};
  let name = options.name || options._componentTag;
  const file = options.__file;

  if (!name && file) {
    const match = file.match(/([^/\\]+)\.vue$/);
    name = match && match[1];
  }

  return (name ? `<${classify(name)}>` : `<Anonymous>`) + (file && includeFile !== false ? ` at ${file}` : '');
}

function generateComponentTrace(vm) {
  if (vm._isVue && vm.$parent) {
    const tree = [];
    let currentRecursiveSequence = 0;

    while (vm) {
      if (tree.length > 0) {
        const last = tree[tree.length - 1];

        if (last.constructor === vm.constructor) {
          currentRecursiveSequence++;
          vm = vm.$parent;
          continue;
        } else if (currentRecursiveSequence > 0) {
          tree[tree.length - 1] = [last, currentRecursiveSequence];
          currentRecursiveSequence = 0;
        }
      }

      tree.push(vm);
      vm = vm.$parent;
    }

    return '\n\nfound in\n\n' + tree.map((vm, i) => `${i === 0 ? '---> ' : ' '.repeat(5 + i * 2)}${Array.isArray(vm) ? `${formatComponentName(vm[0])}... (${vm[1]} recursive calls)` : formatComponentName(vm)}`).join('\n');
  } else {
    return `\n\n(found in ${formatComponentName(vm)})`;
  }
}

function isCssColor(color) {
  return !!color && !!color.match(/^(#|var\(--|(rgb|hsl)a?\()/);
}

var Colorable = Vue.extend({
  name: 'colorable',
  props: {
    color: String
  },
  methods: {
    setBackgroundColor(color, data = {}) {
      if (typeof data.style === 'string') {
        // istanbul ignore next
        consoleError('style must be an object', this); // istanbul ignore next

        return data;
      }

      if (typeof data.class === 'string') {
        // istanbul ignore next
        consoleError('class must be an object', this); // istanbul ignore next

        return data;
      }

      if (isCssColor(color)) {
        data.style = { ...data.style,
          'background-color': `${color}`,
          'border-color': `${color}`
        };
      } else if (color) {
        data.class = { ...data.class,
          [color]: true
        };
      }

      return data;
    },

    setTextColor(color, data = {}) {
      if (typeof data.style === 'string') {
        // istanbul ignore next
        consoleError('style must be an object', this); // istanbul ignore next

        return data;
      }

      if (typeof data.class === 'string') {
        // istanbul ignore next
        consoleError('class must be an object', this); // istanbul ignore next

        return data;
      }

      if (isCssColor(color)) {
        data.style = { ...data.style,
          color: `${color}`,
          'caret-color': `${color}`
        };
      } else if (color) {
        const [colorName, colorModifier] = color.toString().trim().split(' ', 2);
        data.class = { ...data.class,
          [colorName + '--text']: true
        };

        if (colorModifier) {
          data.class['text--' + colorModifier] = true;
        }
      }

      return data;
    }

  }
});

var Elevatable = Vue.extend({
  name: 'elevatable',
  props: {
    elevation: [Number, String]
  },
  computed: {
    computedElevation() {
      return this.elevation;
    },

    elevationClasses() {
      const elevation = this.computedElevation;
      if (elevation == null) return {};
      if (isNaN(parseInt(elevation))) return {};
      return {
        [`elevation-${this.elevation}`]: true
      };
    }

  }
});

function createSimpleFunctional(c, el = 'div', name) {
  return Vue.extend({
    name: name || c.replace(/__/g, '-'),
    functional: true,

    render(h, {
      data,
      children
    }) {
      data.staticClass = `${c} ${data.staticClass || ''}`.trim();
      return h(el, data, children);
    }

  });
}
function addOnceEventListener(el, eventName, cb, options = false) {
  var once = event => {
    cb(event);
    el.removeEventListener(eventName, once, options);
  };

  el.addEventListener(eventName, once, options);
}
let passiveSupported = false;

try {
  if (typeof window !== 'undefined') {
    const testListenerOpts = Object.defineProperty({}, 'passive', {
      get: () => {
        passiveSupported = true;
      }
    });
    window.addEventListener('testListener', testListenerOpts, testListenerOpts);
    window.removeEventListener('testListener', testListenerOpts, testListenerOpts);
  }
} catch (e) {
  console.warn(e);
}
function addPassiveEventListener(el, event, cb, options) {
  el.addEventListener(event, cb, passiveSupported ? options : false);
}
function getNestedValue(obj, path, fallback) {
  const last = path.length - 1;
  if (last < 0) return obj === undefined ? fallback : obj;

  for (let i = 0; i < last; i++) {
    if (obj == null) {
      return fallback;
    }

    obj = obj[path[i]];
  }

  if (obj == null) return fallback;
  return obj[path[last]] === undefined ? fallback : obj[path[last]];
}
function deepEqual(a, b) {
  if (a === b) return true;

  if (a instanceof Date && b instanceof Date) {
    // If the values are Date, they were convert to timestamp with getTime and compare it
    if (a.getTime() !== b.getTime()) return false;
  }

  if (a !== Object(a) || b !== Object(b)) {
    // If the values aren't objects, they were already checked for equality
    return false;
  }

  const props = Object.keys(a);

  if (props.length !== Object.keys(b).length) {
    // Different number of props, don't bother to check
    return false;
  }

  return props.every(p => deepEqual(a[p], b[p]));
}
function getObjectValueByPath(obj, path, fallback) {
  // credit: http://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-with-string-key#comment55278413_6491621
  if (obj == null || !path || typeof path !== 'string') return fallback;
  if (obj[path] !== undefined) return obj[path];
  path = path.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties

  path = path.replace(/^\./, ''); // strip a leading dot

  return getNestedValue(obj, path.split('.'), fallback);
}
function getPropertyFromItem(item, property, fallback) {
  if (property == null) return item === undefined ? fallback : item;
  if (item !== Object(item)) return fallback === undefined ? item : fallback;
  if (typeof property === 'string') return getObjectValueByPath(item, property, fallback);
  if (Array.isArray(property)) return getNestedValue(item, property, fallback);
  if (typeof property !== 'function') return fallback;
  const value = property(item, fallback);
  return typeof value === 'undefined' ? fallback : value;
}
function getZIndex(el) {
  if (!el || el.nodeType !== Node.ELEMENT_NODE) return 0;
  const index = +window.getComputedStyle(el).getPropertyValue('z-index');
  if (!index) return getZIndex(el.parentNode);
  return index;
}
const tagsToReplace = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;'
};
function escapeHTML(str) {
  return str.replace(/[&<>]/g, tag => tagsToReplace[tag] || tag);
}
function filterObjectOnKeys(obj, keys) {
  const filtered = {};

  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];

    if (typeof obj[key] !== 'undefined') {
      filtered[key] = obj[key];
    }
  }

  return filtered;
}
function convertToUnit(str, unit = 'px') {
  if (str == null || str === '') {
    return undefined;
  } else if (isNaN(+str)) {
    return String(str);
  } else {
    return `${Number(str)}${unit}`;
  }
}
function kebabCase(str) {
  return (str || '').replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
}

const keyCodes = Object.freeze({
  enter: 13,
  tab: 9,
  delete: 46,
  esc: 27,
  space: 32,
  up: 38,
  down: 40,
  left: 37,
  right: 39,
  end: 35,
  home: 36,
  del: 46,
  backspace: 8,
  insert: 45,
  pageup: 33,
  pagedown: 34
}); // This remaps internal names like '$cancel' or '$vuetify.icons.cancel'
// to the current name or component for that icon.

function remapInternalIcon(vm, iconName) {
  if (!iconName.startsWith('$')) {
    return iconName;
  } // Get the target icon name


  const iconPath = `$vuetify.icons.values.${iconName.split('$').pop().split('.').pop()}`; // Now look up icon indirection name,
  // e.g. '$vuetify.icons.values.cancel'

  return getObjectValueByPath(vm, iconPath, iconName);
}
function keys(o) {
  return Object.keys(o);
}
/**
 * Camelize a hyphen-delimited string.
 */

const camelizeRE = /-(\w)/g;
const camelize = str => {
  return str.replace(camelizeRE, (_, c) => c ? c.toUpperCase() : '');
};
/**
 * Makes the first character of a string uppercase
 */

function upperFirst(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}
function groupItems(items, groupBy, groupDesc) {
  const key = groupBy[0];
  const groups = [];
  let current = null;

  for (var i = 0; i < items.length; i++) {
    const item = items[i];
    const val = getObjectValueByPath(item, key);

    if (current !== val) {
      current = val;
      groups.push({
        name: val,
        items: []
      });
    }

    groups[groups.length - 1].items.push(item);
  }

  return groups;
}
function wrapInArray(v) {
  return v != null ? Array.isArray(v) ? v : [v] : [];
}
function sortItems(items, sortBy, sortDesc, locale, customSorters) {
  if (sortBy === null || !sortBy.length) return items;
  const stringCollator = new Intl.Collator(locale, {
    sensitivity: 'accent',
    usage: 'sort'
  });
  return items.sort((a, b) => {
    for (let i = 0; i < sortBy.length; i++) {
      const sortKey = sortBy[i];
      let sortA = getObjectValueByPath(a, sortKey);
      let sortB = getObjectValueByPath(b, sortKey);

      if (sortDesc[i]) {
        [sortA, sortB] = [sortB, sortA];
      }

      if (customSorters && customSorters[sortKey]) {
        const customResult = customSorters[sortKey](sortA, sortB);
        if (!customResult) continue;
        return customResult;
      } // Check if both cannot be evaluated


      if (sortA === null && sortB === null) {
        continue;
      }

      [sortA, sortB] = [sortA, sortB].map(s => (s || '').toString().toLocaleLowerCase());

      if (sortA !== sortB) {
        if (!isNaN(sortA) && !isNaN(sortB)) return Number(sortA) - Number(sortB);
        return stringCollator.compare(sortA, sortB);
      }
    }

    return 0;
  });
}
function defaultFilter(value, search, item) {
  return value != null && search != null && typeof value !== 'boolean' && value.toString().toLocaleLowerCase().indexOf(search.toLocaleLowerCase()) !== -1;
}
function searchItems(items, search) {
  if (!search) return items;
  search = search.toString().toLowerCase();
  if (search.trim() === '') return items;
  return items.filter(item => Object.keys(item).some(key => defaultFilter(getObjectValueByPath(item, key), search)));
}
/**
 * Returns:
 *  - 'normal' for old style slots - `<template slot="default">`
 *  - 'scoped' for old style scoped slots (`<template slot="default" slot-scope="data">`) or bound v-slot (`#default="data"`)
 *  - 'v-slot' for unbound v-slot (`#default`) - only if the third param is true, otherwise counts as scoped
 */

function getSlotType(vm, name, split) {
  if (vm.$slots[name] && vm.$scopedSlots[name] && vm.$scopedSlots[name].name) {
    return split ? 'v-slot' : 'scoped';
  }

  if (vm.$slots[name]) return 'normal';
  if (vm.$scopedSlots[name]) return 'scoped';
}
function getPrefixedScopedSlots(prefix, scopedSlots) {
  return Object.keys(scopedSlots).filter(k => k.startsWith(prefix)).reduce((obj, k) => {
    obj[k.replace(prefix, '')] = scopedSlots[k];
    return obj;
  }, {});
}
function getSlot(vm, name = 'default', data, optional = false) {
  if (vm.$scopedSlots[name]) {
    return vm.$scopedSlots[name](data instanceof Function ? data() : data);
  } else if (vm.$slots[name] && (!data || optional)) {
    return vm.$slots[name];
  }

  return undefined;
}
function camelizeObjectKeys(obj) {
  if (!obj) return {};
  return Object.keys(obj).reduce((o, key) => {
    o[camelize(key)] = obj[key];
    return o;
  }, {});
}

// Helpers
var Measurable = Vue.extend({
  name: 'measurable',
  props: {
    height: [Number, String],
    maxHeight: [Number, String],
    maxWidth: [Number, String],
    minHeight: [Number, String],
    minWidth: [Number, String],
    width: [Number, String]
  },
  computed: {
    measurableStyles() {
      const styles = {};
      const height = convertToUnit(this.height);
      const minHeight = convertToUnit(this.minHeight);
      const minWidth = convertToUnit(this.minWidth);
      const maxHeight = convertToUnit(this.maxHeight);
      const maxWidth = convertToUnit(this.maxWidth);
      const width = convertToUnit(this.width);
      if (height) styles.height = height;
      if (minHeight) styles.minHeight = minHeight;
      if (minWidth) styles.minWidth = minWidth;
      if (maxHeight) styles.maxHeight = maxHeight;
      if (maxWidth) styles.maxWidth = maxWidth;
      if (width) styles.width = width;
      return styles;
    }

  }
});

// Styles
/* @vue/component */

var VSheet = mixins(BindsAttrs, Colorable, Elevatable, Measurable, Themeable).extend({
  name: 'v-sheet',
  props: {
    tag: {
      type: String,
      default: 'div'
    },
    tile: Boolean
  },
  computed: {
    classes() {
      return {
        'v-sheet': true,
        'v-sheet--tile': this.tile,
        ...this.themeClasses,
        ...this.elevationClasses
      };
    },

    styles() {
      return this.measurableStyles;
    }

  },

  render(h) {
    const data = {
      class: this.classes,
      style: this.styles,
      on: this.listeners$
    };
    return h(this.tag, this.setBackgroundColor(this.color, data), this.$slots.default);
  }

});

function inserted(el, binding) {
  const modifiers = binding.modifiers || {};
  const value = binding.value;
  const {
    handler,
    options
  } = typeof value === 'object' ? value : {
    handler: value,
    options: {}
  };
  const observer = new IntersectionObserver((entries = [], observer) => {
    /* istanbul ignore if */
    if (!el._observe) return; // Just in case, should never fire
    // If is not quiet or has already been
    // initted, invoke the user callback

    if (handler && (!modifiers.quiet || el._observe.init)) {
      const isIntersecting = Boolean(entries.find(entry => entry.isIntersecting));
      handler(entries, observer, isIntersecting);
    } // If has already been initted and
    // has the once modifier, unbind


    if (el._observe.init && modifiers.once) unbind(el); // Otherwise, mark the observer as initted
    else el._observe.init = true;
  }, options);
  el._observe = {
    init: false,
    observer
  };
  observer.observe(el);
}

function unbind(el) {
  /* istanbul ignore if */
  if (!el._observe) return;

  el._observe.observer.unobserve(el);

  delete el._observe;
}

const Intersect = {
  inserted,
  unbind
};

/* @vue/component */

var VResponsive = mixins(Measurable).extend({
  name: 'v-responsive',
  props: {
    aspectRatio: [String, Number]
  },
  computed: {
    computedAspectRatio() {
      return Number(this.aspectRatio);
    },

    aspectStyle() {
      return this.computedAspectRatio ? {
        paddingBottom: 1 / this.computedAspectRatio * 100 + '%'
      } : undefined;
    },

    __cachedSizer() {
      if (!this.aspectStyle) return [];
      return this.$createElement('div', {
        style: this.aspectStyle,
        staticClass: 'v-responsive__sizer'
      });
    }

  },
  methods: {
    genContent() {
      return this.$createElement('div', {
        staticClass: 'v-responsive__content'
      }, this.$slots.default);
    }

  },

  render(h) {
    return h('div', {
      staticClass: 'v-responsive',
      style: this.measurableStyles,
      on: this.$listeners
    }, [this.__cachedSizer, this.genContent()]);
  }

});

// Styles
const hasIntersect = typeof window !== 'undefined' && 'IntersectionObserver' in window;
/* @vue/component */

var VImg = VResponsive.extend({
  name: 'v-img',
  directives: {
    intersect: Intersect
  },
  props: {
    alt: String,
    contain: Boolean,
    eager: Boolean,
    gradient: String,
    lazySrc: String,
    options: {
      type: Object,
      // For more information on types, navigate to:
      // https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API
      default: () => ({
        root: undefined,
        rootMargin: undefined,
        threshold: undefined
      })
    },
    position: {
      type: String,
      default: 'center center'
    },
    sizes: String,
    src: {
      type: [String, Object],
      default: ''
    },
    srcset: String,
    transition: {
      type: [Boolean, String],
      default: 'fade-transition'
    }
  },

  data() {
    return {
      currentSrc: '',
      image: null,
      isLoading: true,
      calculatedAspectRatio: undefined,
      naturalWidth: undefined
    };
  },

  computed: {
    computedAspectRatio() {
      return Number(this.normalisedSrc.aspect || this.calculatedAspectRatio);
    },

    normalisedSrc() {
      return typeof this.src === 'string' ? {
        src: this.src,
        srcset: this.srcset,
        lazySrc: this.lazySrc,
        aspect: Number(this.aspectRatio || 0)
      } : {
        src: this.src.src,
        srcset: this.srcset || this.src.srcset,
        lazySrc: this.lazySrc || this.src.lazySrc,
        aspect: Number(this.aspectRatio || this.src.aspect)
      };
    },

    __cachedImage() {
      if (!(this.normalisedSrc.src || this.normalisedSrc.lazySrc)) return [];
      const backgroundImage = [];
      const src = this.isLoading ? this.normalisedSrc.lazySrc : this.currentSrc;
      if (this.gradient) backgroundImage.push(`linear-gradient(${this.gradient})`);
      if (src) backgroundImage.push(`url("${src}")`);
      const image = this.$createElement('div', {
        staticClass: 'v-image__image',
        class: {
          'v-image__image--preload': this.isLoading,
          'v-image__image--contain': this.contain,
          'v-image__image--cover': !this.contain
        },
        style: {
          backgroundImage: backgroundImage.join(', '),
          backgroundPosition: this.position
        },
        key: +this.isLoading
      });
      /* istanbul ignore if */

      if (!this.transition) return image;
      return this.$createElement('transition', {
        attrs: {
          name: this.transition,
          mode: 'in-out'
        }
      }, [image]);
    }

  },
  watch: {
    src() {
      // Force re-init when src changes
      if (!this.isLoading) this.init(undefined, undefined, true);else this.loadImage();
    },

    '$vuetify.breakpoint.width': 'getSrc'
  },

  mounted() {
    this.init();
  },

  methods: {
    init(entries, observer, isIntersecting) {
      // If the current browser supports the intersection
      // observer api, the image is not observable, and
      // the eager prop isn't being used, do not load
      if (hasIntersect && !isIntersecting && !this.eager) return;

      if (this.normalisedSrc.lazySrc) {
        const lazyImg = new Image();
        lazyImg.src = this.normalisedSrc.lazySrc;
        this.pollForSize(lazyImg, null);
      }
      /* istanbul ignore else */


      if (this.normalisedSrc.src) this.loadImage();
    },

    onLoad() {
      this.getSrc();
      this.isLoading = false;
      this.$emit('load', this.src);
    },

    onError() {
      consoleError(`Image load failed\n\n` + `src: ${this.normalisedSrc.src}`, this);
      this.$emit('error', this.src);
    },

    getSrc() {
      /* istanbul ignore else */
      if (this.image) this.currentSrc = this.image.currentSrc || this.image.src;
    },

    loadImage() {
      const image = new Image();
      this.image = image;

      image.onload = () => {
        /* istanbul ignore if */
        if (image.decode) {
          image.decode().catch(err => {
            consoleWarn(`Failed to decode image, trying to render anyway\n\n` + `src: ${this.normalisedSrc.src}` + (err.message ? `\nOriginal error: ${err.message}` : ''), this);
          }).then(this.onLoad);
        } else {
          this.onLoad();
        }
      };

      image.onerror = this.onError;
      image.src = this.normalisedSrc.src;
      this.sizes && (image.sizes = this.sizes);
      this.normalisedSrc.srcset && (image.srcset = this.normalisedSrc.srcset);
      this.aspectRatio || this.pollForSize(image);
      this.getSrc();
    },

    pollForSize(img, timeout = 100) {
      const poll = () => {
        const {
          naturalHeight,
          naturalWidth
        } = img;

        if (naturalHeight || naturalWidth) {
          this.naturalWidth = naturalWidth;
          this.calculatedAspectRatio = naturalWidth / naturalHeight;
        } else {
          timeout != null && setTimeout(poll, timeout);
        }
      };

      poll();
    },

    genContent() {
      const content = VResponsive.options.methods.genContent.call(this);

      if (this.naturalWidth) {
        this._b(content.data, 'div', {
          style: {
            width: `${this.naturalWidth}px`
          }
        });
      }

      return content;
    },

    __genPlaceholder() {
      if (this.$slots.placeholder) {
        const placeholder = this.isLoading ? [this.$createElement('div', {
          staticClass: 'v-image__placeholder'
        }, this.$slots.placeholder)] : [];
        if (!this.transition) return placeholder[0];
        return this.$createElement('transition', {
          props: {
            appear: true,
            name: this.transition
          }
        }, placeholder);
      }
    }

  },

  render(h) {
    const node = VResponsive.options.render.call(this, h);
    node.data.staticClass += ' v-image'; // Only load intersect directive if it
    // will work in the current browser.

    if (hasIntersect) {
      node.data.directives = [{
        name: 'intersect',
        modifiers: {
          once: true
        },
        value: {
          handler: this.init,
          options: this.options
        }
      }];
    }

    node.data.attrs = {
      role: this.alt ? 'img' : undefined,
      'aria-label': this.alt
    };
    node.children = [this.__cachedSizer, this.__cachedImage, this.__genPlaceholder(), this.genContent()];
    return h(node.tag, node.data, node.children);
  }

});

const availableProps = {
  absolute: Boolean,
  bottom: Boolean,
  fixed: Boolean,
  left: Boolean,
  right: Boolean,
  top: Boolean
};
function factory(selected = []) {
  return Vue.extend({
    name: 'positionable',
    props: selected.length ? filterObjectOnKeys(availableProps, selected) : availableProps
  });
}
var Positionable = factory(); // Add a `*` before the second `/`

/* Tests /
let single = factory(['top']).extend({
  created () {
    this.top
    this.bottom
    this.absolute
  }
})

let some = factory(['top', 'bottom']).extend({
  created () {
    this.top
    this.bottom
    this.absolute
  }
})

let all = factory().extend({
  created () {
    this.top
    this.bottom
    this.absolute
    this.foobar
  }
})
/**/

function applicationable(value, events = []) {
  /* @vue/component */
  return mixins(factory(['absolute', 'fixed'])).extend({
    name: 'applicationable',
    props: {
      app: Boolean
    },
    computed: {
      applicationProperty() {
        return value;
      }

    },
    watch: {
      // If previous value was app
      // reset the provided prop
      app(x, prev) {
        prev ? this.removeApplication(true) : this.callUpdate();
      },

      applicationProperty(newVal, oldVal) {
        this.$vuetify.application.unregister(this._uid, oldVal);
      }

    },

    activated() {
      this.callUpdate();
    },

    created() {
      for (let i = 0, length = events.length; i < length; i++) {
        this.$watch(events[i], this.callUpdate);
      }

      this.callUpdate();
    },

    mounted() {
      this.callUpdate();
    },

    deactivated() {
      this.removeApplication();
    },

    destroyed() {
      this.removeApplication();
    },

    methods: {
      callUpdate() {
        if (!this.app) return;
        this.$vuetify.application.register(this._uid, this.applicationProperty, this.updateApplication());
      },

      removeApplication(force = false) {
        if (!force && !this.app) return;
        this.$vuetify.application.unregister(this._uid, this.applicationProperty);
      },

      updateApplication: () => 0
    }
  });
}

function closeConditional() {
  return false;
}

function directive(e, el, binding) {
  // Args may not always be supplied
  binding.args = binding.args || {}; // If no closeConditional was supplied assign a default

  const isActive = binding.args.closeConditional || closeConditional; // The include element callbacks below can be expensive
  // so we should avoid calling them when we're not active.
  // Explicitly check for false to allow fallback compatibility
  // with non-toggleable components

  if (!e || isActive(e) === false) return; // If click was triggered programmaticaly (domEl.click()) then
  // it shouldn't be treated as click-outside
  // Chrome/Firefox support isTrusted property
  // IE/Edge support pointerType property (empty if not triggered
  // by pointing device)

  if ('isTrusted' in e && !e.isTrusted || 'pointerType' in e && !e.pointerType) return; // Check if additional elements were passed to be included in check
  // (click must be outside all included elements, if any)

  const elements = (binding.args.include || (() => []))(); // Add the root element for the component this directive was defined on


  elements.push(el); // Check if it's a click outside our elements, and then if our callback returns true.
  // Non-toggleable components should take action in their callback and return falsy.
  // Toggleable can return true if it wants to deactivate.
  // Note that, because we're in the capture phase, this callback will occur before
  // the bubbling click event on any outside elements.

  !elements.some(el => el.contains(e.target)) && setTimeout(() => {
    isActive(e) && binding.value && binding.value(e);
  }, 0);
}

const ClickOutside = {
  // [data-app] may not be found
  // if using bind, inserted makes
  // sure that the root element is
  // available, iOS does not support
  // clicks on body
  inserted(el, binding) {
    const onClick = e => directive(e, el, binding); // iOS does not recognize click events on document
    // or body, this is the entire purpose of the v-app
    // component and [data-app], stop removing this


    const app = document.querySelector('[data-app]') || document.body; // This is only for unit tests

    app.addEventListener('click', onClick, true);
    el._clickOutside = onClick;
  },

  unbind(el) {
    if (!el._clickOutside) return;
    const app = document.querySelector('[data-app]') || document.body; // This is only for unit tests

    app && app.removeEventListener('click', el._clickOutside, true);
    delete el._clickOutside;
  }

};

function inserted$1(el, binding) {
  const callback = binding.value;
  const options = binding.options || {
    passive: true
  };
  window.addEventListener('resize', callback, options);
  el._onResize = {
    callback,
    options
  };

  if (!binding.modifiers || !binding.modifiers.quiet) {
    callback();
  }
}

function unbind$1(el) {
  if (!el._onResize) return;
  const {
    callback,
    options
  } = el._onResize;
  window.removeEventListener('resize', callback, options);
  delete el._onResize;
}

const Resize = {
  inserted: inserted$1,
  unbind: unbind$1
};

// Styles

function transform(el, value) {
  el.style['transform'] = value;
  el.style['webkitTransform'] = value;
}

function opacity(el, value) {
  el.style['opacity'] = value.toString();
}

function isTouchEvent(e) {
  return e.constructor.name === 'TouchEvent';
}

function isKeyboardEvent(e) {
  return e.constructor.name === 'KeyboardEvent';
}

const calculate = (e, el, value = {}) => {
  let localX = 0;
  let localY = 0;

  if (!isKeyboardEvent(e)) {
    const offset = el.getBoundingClientRect();
    const target = isTouchEvent(e) ? e.touches[e.touches.length - 1] : e;
    localX = target.clientX - offset.left;
    localY = target.clientY - offset.top;
  }

  let radius = 0;
  let scale = 0.3;

  if (el._ripple && el._ripple.circle) {
    scale = 0.15;
    radius = el.clientWidth / 2;
    radius = value.center ? radius : radius + Math.sqrt((localX - radius) ** 2 + (localY - radius) ** 2) / 4;
  } else {
    radius = Math.sqrt(el.clientWidth ** 2 + el.clientHeight ** 2) / 2;
  }

  const centerX = `${(el.clientWidth - radius * 2) / 2}px`;
  const centerY = `${(el.clientHeight - radius * 2) / 2}px`;
  const x = value.center ? centerX : `${localX - radius}px`;
  const y = value.center ? centerY : `${localY - radius}px`;
  return {
    radius,
    scale,
    x,
    y,
    centerX,
    centerY
  };
};

const ripples = {
  /* eslint-disable max-statements */
  show(e, el, value = {}) {
    if (!el._ripple || !el._ripple.enabled) {
      return;
    }

    const container = document.createElement('span');
    const animation = document.createElement('span');
    container.appendChild(animation);
    container.className = 'v-ripple__container';

    if (value.class) {
      container.className += ` ${value.class}`;
    }

    const {
      radius,
      scale,
      x,
      y,
      centerX,
      centerY
    } = calculate(e, el, value);
    const size = `${radius * 2}px`;
    animation.className = 'v-ripple__animation';
    animation.style.width = size;
    animation.style.height = size;
    el.appendChild(container);
    const computed = window.getComputedStyle(el);

    if (computed && computed.position === 'static') {
      el.style.position = 'relative';
      el.dataset.previousPosition = 'static';
    }

    animation.classList.add('v-ripple__animation--enter');
    animation.classList.add('v-ripple__animation--visible');
    transform(animation, `translate(${x}, ${y}) scale3d(${scale},${scale},${scale})`);
    opacity(animation, 0);
    animation.dataset.activated = String(performance.now());
    setTimeout(() => {
      animation.classList.remove('v-ripple__animation--enter');
      animation.classList.add('v-ripple__animation--in');
      transform(animation, `translate(${centerX}, ${centerY}) scale3d(1,1,1)`);
      opacity(animation, 0.25);
    }, 0);
  },

  hide(el) {
    if (!el || !el._ripple || !el._ripple.enabled) return;
    const ripples = el.getElementsByClassName('v-ripple__animation');
    if (ripples.length === 0) return;
    const animation = ripples[ripples.length - 1];
    if (animation.dataset.isHiding) return;else animation.dataset.isHiding = 'true';
    const diff = performance.now() - Number(animation.dataset.activated);
    const delay = Math.max(250 - diff, 0);
    setTimeout(() => {
      animation.classList.remove('v-ripple__animation--in');
      animation.classList.add('v-ripple__animation--out');
      opacity(animation, 0);
      setTimeout(() => {
        const ripples = el.getElementsByClassName('v-ripple__animation');

        if (ripples.length === 1 && el.dataset.previousPosition) {
          el.style.position = el.dataset.previousPosition;
          delete el.dataset.previousPosition;
        }

        animation.parentNode && el.removeChild(animation.parentNode);
      }, 300);
    }, delay);
  }

};

function isRippleEnabled(value) {
  return typeof value === 'undefined' || !!value;
}

function rippleShow(e) {
  const value = {};
  const element = e.currentTarget;
  if (!element || !element._ripple || element._ripple.touched) return;

  if (isTouchEvent(e)) {
    element._ripple.touched = true;
    element._ripple.isTouch = true;
  } else {
    // It's possible for touch events to fire
    // as mouse events on Android/iOS, this
    // will skip the event call if it has
    // already been registered as touch
    if (element._ripple.isTouch) return;
  }

  value.center = element._ripple.centered || isKeyboardEvent(e);

  if (element._ripple.class) {
    value.class = element._ripple.class;
  }

  ripples.show(e, element, value);
}

function rippleHide(e) {
  const element = e.currentTarget;
  if (!element) return;
  window.setTimeout(() => {
    if (element._ripple) {
      element._ripple.touched = false;
    }
  });
  ripples.hide(element);
}

let keyboardRipple = false;

function keyboardRippleShow(e) {
  if (!keyboardRipple && (e.keyCode === keyCodes.enter || e.keyCode === keyCodes.space)) {
    keyboardRipple = true;
    rippleShow(e);
  }
}

function keyboardRippleHide(e) {
  keyboardRipple = false;
  rippleHide(e);
}

function updateRipple(el, binding, wasEnabled) {
  const enabled = isRippleEnabled(binding.value);

  if (!enabled) {
    ripples.hide(el);
  }

  el._ripple = el._ripple || {};
  el._ripple.enabled = enabled;
  const value = binding.value || {};

  if (value.center) {
    el._ripple.centered = true;
  }

  if (value.class) {
    el._ripple.class = binding.value.class;
  }

  if (value.circle) {
    el._ripple.circle = value.circle;
  }

  if (enabled && !wasEnabled) {
    el.addEventListener('touchstart', rippleShow, {
      passive: true
    });
    el.addEventListener('touchend', rippleHide, {
      passive: true
    });
    el.addEventListener('touchcancel', rippleHide);
    el.addEventListener('mousedown', rippleShow);
    el.addEventListener('mouseup', rippleHide);
    el.addEventListener('mouseleave', rippleHide);
    el.addEventListener('keydown', keyboardRippleShow);
    el.addEventListener('keyup', keyboardRippleHide); // Anchor tags can be dragged, causes other hides to fail - #1537

    el.addEventListener('dragstart', rippleHide, {
      passive: true
    });
  } else if (!enabled && wasEnabled) {
    removeListeners(el);
  }
}

function removeListeners(el) {
  el.removeEventListener('mousedown', rippleShow);
  el.removeEventListener('touchstart', rippleShow);
  el.removeEventListener('touchend', rippleHide);
  el.removeEventListener('touchcancel', rippleHide);
  el.removeEventListener('mouseup', rippleHide);
  el.removeEventListener('mouseleave', rippleHide);
  el.removeEventListener('keydown', keyboardRippleShow);
  el.removeEventListener('keyup', keyboardRippleHide);
  el.removeEventListener('dragstart', rippleHide);
}

function directive$1(el, binding, node) {
  updateRipple(el, binding, false);
}

function unbind$2(el) {
  delete el._ripple;
  removeListeners(el);
}

function update(el, binding) {
  if (binding.value === binding.oldValue) {
    return;
  }

  const wasEnabled = isRippleEnabled(binding.oldValue);
  updateRipple(el, binding, wasEnabled);
}

const Ripple = {
  bind: directive$1,
  unbind: unbind$2,
  update
};

const handleGesture = wrapper => {
  const {
    touchstartX,
    touchendX,
    touchstartY,
    touchendY
  } = wrapper;
  const dirRatio = 0.5;
  const minDistance = 16;
  wrapper.offsetX = touchendX - touchstartX;
  wrapper.offsetY = touchendY - touchstartY;

  if (Math.abs(wrapper.offsetY) < dirRatio * Math.abs(wrapper.offsetX)) {
    wrapper.left && touchendX < touchstartX - minDistance && wrapper.left(wrapper);
    wrapper.right && touchendX > touchstartX + minDistance && wrapper.right(wrapper);
  }

  if (Math.abs(wrapper.offsetX) < dirRatio * Math.abs(wrapper.offsetY)) {
    wrapper.up && touchendY < touchstartY - minDistance && wrapper.up(wrapper);
    wrapper.down && touchendY > touchstartY + minDistance && wrapper.down(wrapper);
  }
};

function touchstart(event, wrapper) {
  const touch = event.changedTouches[0];
  wrapper.touchstartX = touch.clientX;
  wrapper.touchstartY = touch.clientY;
  wrapper.start && wrapper.start(Object.assign(event, wrapper));
}

function touchend(event, wrapper) {
  const touch = event.changedTouches[0];
  wrapper.touchendX = touch.clientX;
  wrapper.touchendY = touch.clientY;
  wrapper.end && wrapper.end(Object.assign(event, wrapper));
  handleGesture(wrapper);
}

function touchmove(event, wrapper) {
  const touch = event.changedTouches[0];
  wrapper.touchmoveX = touch.clientX;
  wrapper.touchmoveY = touch.clientY;
  wrapper.move && wrapper.move(Object.assign(event, wrapper));
}

function createHandlers(value) {
  const wrapper = {
    touchstartX: 0,
    touchstartY: 0,
    touchendX: 0,
    touchendY: 0,
    touchmoveX: 0,
    touchmoveY: 0,
    offsetX: 0,
    offsetY: 0,
    left: value.left,
    right: value.right,
    up: value.up,
    down: value.down,
    start: value.start,
    move: value.move,
    end: value.end
  };
  return {
    touchstart: e => touchstart(e, wrapper),
    touchend: e => touchend(e, wrapper),
    touchmove: e => touchmove(e, wrapper)
  };
}

function inserted$2(el, binding, vnode) {
  const value = binding.value;
  const target = value.parent ? el.parentElement : el;
  const options = value.options || {
    passive: true
  }; // Needed to pass unit tests

  if (!target) return;
  const handlers = createHandlers(binding.value);
  target._touchHandlers = Object(target._touchHandlers);
  target._touchHandlers[vnode.context._uid] = handlers;
  keys(handlers).forEach(eventName => {
    target.addEventListener(eventName, handlers[eventName], options);
  });
}

function unbind$3(el, binding, vnode) {
  const target = binding.value.parent ? el.parentElement : el;
  if (!target || !target._touchHandlers) return;
  const handlers = target._touchHandlers[vnode.context._uid];
  keys(handlers).forEach(eventName => {
    target.removeEventListener(eventName, handlers[eventName]);
  });
  delete target._touchHandlers[vnode.context._uid];
}

const Touch = {
  inserted: inserted$2,
  unbind: unbind$3
};

/**
 * SSRBootable
 *
 * @mixin
 *
 * Used in layout components (drawer, toolbar, content)
 * to avoid an entry animation when using SSR
 */

var SSRBootable = Vue.extend({
  name: 'ssr-bootable',
  data: () => ({
    isBooted: false
  }),

  mounted() {
    // Use setAttribute instead of dataset
    // because dataset does not work well
    // with unit tests
    window.requestAnimationFrame(() => {
      this.$el.setAttribute('data-booted', 'true');
      this.isBooted = true;
    });
  }

});

function factory$1(prop = 'value', event = 'input') {
  return Vue.extend({
    name: 'toggleable',
    model: {
      prop,
      event
    },
    props: {
      [prop]: {
        required: false
      }
    },

    data() {
      return {
        isActive: !!this[prop]
      };
    },

    watch: {
      [prop](val) {
        this.isActive = !!val;
      },

      isActive(val) {
        !!val !== this[prop] && this.$emit(event, val);
      }

    }
  });
}
/* eslint-disable-next-line no-redeclare */

const Toggleable = factory$1();

var Sizeable = Vue.extend({
  name: 'sizeable',
  props: {
    large: Boolean,
    small: Boolean,
    xLarge: Boolean,
    xSmall: Boolean
  },
  computed: {
    medium() {
      return Boolean(!this.xSmall && !this.small && !this.large && !this.xLarge);
    },

    sizeableClasses() {
      return {
        'v-size--x-small': this.xSmall,
        'v-size--small': this.small,
        'v-size--default': this.medium,
        'v-size--large': this.large,
        'v-size--x-large': this.xLarge
      };
    }

  }
});

var SIZE_MAP;

(function (SIZE_MAP) {
  SIZE_MAP["xSmall"] = "12px";
  SIZE_MAP["small"] = "16px";
  SIZE_MAP["default"] = "24px";
  SIZE_MAP["medium"] = "28px";
  SIZE_MAP["large"] = "36px";
  SIZE_MAP["xLarge"] = "40px";
})(SIZE_MAP || (SIZE_MAP = {}));

function isFontAwesome5(iconType) {
  return ['fas', 'far', 'fal', 'fab', 'fad'].some(val => iconType.includes(val));
}

function isSvgPath(icon) {
  return /^[mzlhvcsqta]\s*[-+.0-9][^mlhvzcsqta]+/i.test(icon) && /[\dz]$/i.test(icon) && icon.length > 4;
}

const VIcon = mixins(BindsAttrs, Colorable, Sizeable, Themeable
/* @vue/component */
).extend({
  name: 'v-icon',
  props: {
    dense: Boolean,
    disabled: Boolean,
    left: Boolean,
    right: Boolean,
    size: [Number, String],
    tag: {
      type: String,
      required: false,
      default: 'i'
    }
  },
  computed: {
    medium() {
      return false;
    },

    hasClickListener() {
      return Boolean(this.listeners$.click || this.listeners$['!click']);
    }

  },
  methods: {
    getIcon() {
      let iconName = '';
      if (this.$slots.default) iconName = this.$slots.default[0].text.trim();
      return remapInternalIcon(this, iconName);
    },

    getSize() {
      const sizes = {
        xSmall: this.xSmall,
        small: this.small,
        medium: this.medium,
        large: this.large,
        xLarge: this.xLarge
      };
      const explicitSize = keys(sizes).find(key => sizes[key]);
      return explicitSize && SIZE_MAP[explicitSize] || convertToUnit(this.size);
    },

    // Component data for both font and svg icon.
    getDefaultData() {
      const data = {
        staticClass: 'v-icon notranslate',
        class: {
          'v-icon--disabled': this.disabled,
          'v-icon--left': this.left,
          'v-icon--link': this.hasClickListener,
          'v-icon--right': this.right,
          'v-icon--dense': this.dense
        },
        attrs: {
          'aria-hidden': !this.hasClickListener,
          disabled: this.hasClickListener && this.disabled,
          type: this.hasClickListener ? 'button' : undefined,
          ...this.attrs$
        },
        on: this.listeners$
      };
      return data;
    },

    applyColors(data) {
      data.class = { ...data.class,
        ...this.themeClasses
      };
      this.setTextColor(this.color, data);
    },

    renderFontIcon(icon, h) {
      const newChildren = [];
      const data = this.getDefaultData();
      let iconType = 'material-icons'; // Material Icon delimiter is _
      // https://material.io/icons/

      const delimiterIndex = icon.indexOf('-');
      const isMaterialIcon = delimiterIndex <= -1;

      if (isMaterialIcon) {
        // Material icon uses ligatures.
        newChildren.push(icon);
      } else {
        iconType = icon.slice(0, delimiterIndex);
        if (isFontAwesome5(iconType)) iconType = '';
      }

      data.class[iconType] = true;
      data.class[icon] = !isMaterialIcon;
      const fontSize = this.getSize();
      if (fontSize) data.style = {
        fontSize
      };
      this.applyColors(data);
      return h(this.hasClickListener ? 'button' : this.tag, data, newChildren);
    },

    renderSvgIcon(icon, h) {
      const fontSize = this.getSize();
      const wrapperData = { ...this.getDefaultData(),
        style: fontSize ? {
          fontSize,
          height: fontSize,
          width: fontSize
        } : undefined
      };
      wrapperData.class['v-icon--svg'] = true;
      this.applyColors(wrapperData);
      const svgData = {
        attrs: {
          xmlns: 'http://www.w3.org/2000/svg',
          viewBox: '0 0 24 24',
          height: fontSize || '24',
          width: fontSize || '24',
          role: 'img',
          'aria-hidden': true
        }
      };
      return h(this.hasClickListener ? 'button' : 'span', wrapperData, [h('svg', svgData, [h('path', {
        attrs: {
          d: icon
        }
      })])]);
    },

    renderSvgIconComponent(icon, h) {
      const data = this.getDefaultData();
      data.class['v-icon--is-component'] = true;
      const size = this.getSize();

      if (size) {
        data.style = {
          fontSize: size,
          height: size,
          width: size
        };
      }

      this.applyColors(data);
      const component = icon.component;
      data.props = icon.props;
      data.nativeOn = data.on;
      return h(component, data);
    }

  },

  render(h) {
    const icon = this.getIcon();

    if (typeof icon === 'string') {
      if (isSvgPath(icon)) {
        return this.renderSvgIcon(icon, h);
      }

      return this.renderFontIcon(icon, h);
    }

    return this.renderSvgIconComponent(icon, h);
  }

});
var VIcon$1 = Vue.extend({
  name: 'v-icon',
  $_wrapperFor: VIcon,
  functional: true,

  render(h, {
    data,
    children
  }) {
    let iconName = ''; // Support usage of v-text and v-html

    if (data.domProps) {
      iconName = data.domProps.textContent || data.domProps.innerHTML || iconName; // Remove nodes so it doesn't
      // overwrite our changes

      delete data.domProps.textContent;
      delete data.domProps.innerHTML;
    }

    return h(VIcon, data, iconName ? [iconName] : children);
  }

});

// Styles
/* @vue/component */

var VProgressCircular = Colorable.extend({
  name: 'v-progress-circular',
  props: {
    button: Boolean,
    indeterminate: Boolean,
    rotate: {
      type: [Number, String],
      default: 0
    },
    size: {
      type: [Number, String],
      default: 32
    },
    width: {
      type: [Number, String],
      default: 4
    },
    value: {
      type: [Number, String],
      default: 0
    }
  },
  data: () => ({
    radius: 20
  }),
  computed: {
    calculatedSize() {
      return Number(this.size) + (this.button ? 8 : 0);
    },

    circumference() {
      return 2 * Math.PI * this.radius;
    },

    classes() {
      return {
        'v-progress-circular--indeterminate': this.indeterminate,
        'v-progress-circular--button': this.button
      };
    },

    normalizedValue() {
      if (this.value < 0) {
        return 0;
      }

      if (this.value > 100) {
        return 100;
      }

      return parseFloat(this.value);
    },

    strokeDashArray() {
      return Math.round(this.circumference * 1000) / 1000;
    },

    strokeDashOffset() {
      return (100 - this.normalizedValue) / 100 * this.circumference + 'px';
    },

    strokeWidth() {
      return Number(this.width) / +this.size * this.viewBoxSize * 2;
    },

    styles() {
      return {
        height: convertToUnit(this.calculatedSize),
        width: convertToUnit(this.calculatedSize)
      };
    },

    svgStyles() {
      return {
        transform: `rotate(${Number(this.rotate)}deg)`
      };
    },

    viewBoxSize() {
      return this.radius / (1 - Number(this.width) / +this.size);
    }

  },
  methods: {
    genCircle(name, offset) {
      return this.$createElement('circle', {
        class: `v-progress-circular__${name}`,
        attrs: {
          fill: 'transparent',
          cx: 2 * this.viewBoxSize,
          cy: 2 * this.viewBoxSize,
          r: this.radius,
          'stroke-width': this.strokeWidth,
          'stroke-dasharray': this.strokeDashArray,
          'stroke-dashoffset': offset
        }
      });
    },

    genSvg() {
      const children = [this.indeterminate || this.genCircle('underlay', 0), this.genCircle('overlay', this.strokeDashOffset)];
      return this.$createElement('svg', {
        style: this.svgStyles,
        attrs: {
          xmlns: 'http://www.w3.org/2000/svg',
          viewBox: `${this.viewBoxSize} ${this.viewBoxSize} ${2 * this.viewBoxSize} ${2 * this.viewBoxSize}`
        }
      }, children);
    },

    genInfo() {
      return this.$createElement('div', {
        staticClass: 'v-progress-circular__info'
      }, this.$slots.default);
    }

  },

  render(h) {
    return h('div', this.setTextColor(this.color, {
      staticClass: 'v-progress-circular',
      attrs: {
        role: 'progressbar',
        'aria-valuemin': 0,
        'aria-valuemax': 100,
        'aria-valuenow': this.indeterminate ? undefined : this.normalizedValue
      },
      class: this.classes,
      style: this.styles,
      on: this.$listeners
    }), [this.genSvg(), this.genInfo()]);
  }

});

function generateWarning(child, parent) {
  return () => consoleWarn(`The ${child} component must be used inside a ${parent}`);
}

function inject(namespace, child, parent) {
  const defaultImpl = child && parent ? {
    register: generateWarning(child, parent),
    unregister: generateWarning(child, parent)
  } : null;
  return Vue.extend({
    name: 'registrable-inject',
    inject: {
      [namespace]: {
        default: defaultImpl
      }
    }
  });
}

// Mixins
function factory$2(namespace, child, parent) {
  // TODO: ts 3.4 broke directly returning this
  const R = inject(namespace, child, parent).extend({
    name: 'groupable',
    props: {
      activeClass: {
        type: String,

        default() {
          if (!this[namespace]) return undefined;
          return this[namespace].activeClass;
        }

      },
      disabled: Boolean
    },

    data() {
      return {
        isActive: false
      };
    },

    computed: {
      groupClasses() {
        if (!this.activeClass) return {};
        return {
          [this.activeClass]: this.isActive
        };
      }

    },

    created() {
      this[namespace] && this[namespace].register(this);
    },

    beforeDestroy() {
      this[namespace] && this[namespace].unregister(this);
    },

    methods: {
      toggle() {
        this.$emit('change');
      }

    }
  });
  return R;
}
/* eslint-disable-next-line no-redeclare */

const Groupable = factory$2('itemGroup');

var Routable = Vue.extend({
  name: 'routable',
  directives: {
    Ripple
  },
  props: {
    activeClass: String,
    append: Boolean,
    disabled: Boolean,
    exact: {
      type: Boolean,
      default: undefined
    },
    exactActiveClass: String,
    link: Boolean,
    href: [String, Object],
    to: [String, Object],
    nuxt: Boolean,
    replace: Boolean,
    ripple: {
      type: [Boolean, Object],
      default: null
    },
    tag: String,
    target: String
  },
  data: () => ({
    isActive: false,
    proxyClass: ''
  }),
  computed: {
    classes() {
      const classes = {};
      if (this.to) return classes;
      if (this.activeClass) classes[this.activeClass] = this.isActive;
      if (this.proxyClass) classes[this.proxyClass] = this.isActive;
      return classes;
    },

    computedRipple() {
      return this.ripple != null ? this.ripple : !this.disabled && this.isClickable;
    },

    isClickable() {
      if (this.disabled) return false;
      return Boolean(this.isLink || this.$listeners.click || this.$listeners['!click'] || this.$attrs.tabindex);
    },

    isLink() {
      return this.to || this.href || this.link;
    },

    styles: () => ({})
  },
  watch: {
    $route: 'onRouteChange'
  },
  methods: {
    click(e) {
      this.$emit('click', e);
    },

    generateRouteLink() {
      let exact = this.exact;
      let tag;
      const data = {
        attrs: {
          tabindex: 'tabindex' in this.$attrs ? this.$attrs.tabindex : undefined
        },
        class: this.classes,
        style: this.styles,
        props: {},
        directives: [{
          name: 'ripple',
          value: this.computedRipple
        }],
        [this.to ? 'nativeOn' : 'on']: { ...this.$listeners,
          click: this.click
        },
        ref: 'link'
      };

      if (typeof this.exact === 'undefined') {
        exact = this.to === '/' || this.to === Object(this.to) && this.to.path === '/';
      }

      if (this.to) {
        // Add a special activeClass hook
        // for component level styles
        let activeClass = this.activeClass;
        let exactActiveClass = this.exactActiveClass || activeClass;

        if (this.proxyClass) {
          activeClass = `${activeClass} ${this.proxyClass}`.trim();
          exactActiveClass = `${exactActiveClass} ${this.proxyClass}`.trim();
        }

        tag = this.nuxt ? 'nuxt-link' : 'router-link';
        Object.assign(data.props, {
          to: this.to,
          exact,
          activeClass,
          exactActiveClass,
          append: this.append,
          replace: this.replace
        });
      } else {
        tag = this.href && 'a' || this.tag || 'div';
        if (tag === 'a' && this.href) data.attrs.href = this.href;
      }

      if (this.target) data.attrs.target = this.target;
      return {
        tag,
        data
      };
    },

    onRouteChange() {
      if (!this.to || !this.$refs.link || !this.$route) return;
      const activeClass = `${this.activeClass} ${this.proxyClass || ''}`.trim();
      const path = `_vnode.data.class.${activeClass}`;
      this.$nextTick(() => {
        /* istanbul ignore else */
        if (getObjectValueByPath(this.$refs.link, path)) {
          this.toggle();
        }
      });
    },

    toggle: () => {}
  }
});

// Styles
const baseMixins = mixins(VSheet, Routable, Positionable, Sizeable, factory$2('btnToggle'), factory$1('inputValue')
/* @vue/component */
);
var VBtn = baseMixins.extend().extend({
  name: 'v-btn',
  props: {
    activeClass: {
      type: String,

      default() {
        if (!this.btnToggle) return '';
        return this.btnToggle.activeClass;
      }

    },
    block: Boolean,
    depressed: Boolean,
    fab: Boolean,
    icon: Boolean,
    loading: Boolean,
    outlined: Boolean,
    retainFocusOnClick: Boolean,
    rounded: Boolean,
    tag: {
      type: String,
      default: 'button'
    },
    text: Boolean,
    type: {
      type: String,
      default: 'button'
    },
    value: null
  },
  data: () => ({
    proxyClass: 'v-btn--active'
  }),
  computed: {
    classes() {
      return {
        'v-btn': true,
        ...Routable.options.computed.classes.call(this),
        'v-btn--absolute': this.absolute,
        'v-btn--block': this.block,
        'v-btn--bottom': this.bottom,
        'v-btn--contained': this.contained,
        'v-btn--depressed': this.depressed || this.outlined,
        'v-btn--disabled': this.disabled,
        'v-btn--fab': this.fab,
        'v-btn--fixed': this.fixed,
        'v-btn--flat': this.isFlat,
        'v-btn--icon': this.icon,
        'v-btn--left': this.left,
        'v-btn--loading': this.loading,
        'v-btn--outlined': this.outlined,
        'v-btn--right': this.right,
        'v-btn--round': this.isRound,
        'v-btn--rounded': this.rounded,
        'v-btn--router': this.to,
        'v-btn--text': this.text,
        'v-btn--tile': this.tile,
        'v-btn--top': this.top,
        ...this.themeClasses,
        ...this.groupClasses,
        ...this.elevationClasses,
        ...this.sizeableClasses
      };
    },

    contained() {
      return Boolean(!this.isFlat && !this.depressed && // Contained class only adds elevation
      // is not needed if user provides value
      !this.elevation);
    },

    computedRipple() {
      const defaultRipple = this.icon || this.fab ? {
        circle: true
      } : true;
      if (this.disabled) return false;else return this.ripple != null ? this.ripple : defaultRipple;
    },

    isFlat() {
      return Boolean(this.icon || this.text || this.outlined);
    },

    isRound() {
      return Boolean(this.icon || this.fab);
    },

    styles() {
      return { ...this.measurableStyles
      };
    }

  },

  created() {
    const breakingProps = [['flat', 'text'], ['outline', 'outlined'], ['round', 'rounded']];
    /* istanbul ignore next */

    breakingProps.forEach(([original, replacement]) => {
      if (this.$attrs.hasOwnProperty(original)) breaking(original, replacement, this);
    });
  },

  methods: {
    click(e) {
      !this.retainFocusOnClick && !this.fab && e.detail && this.$el.blur();
      this.$emit('click', e);
      this.btnToggle && this.toggle();
    },

    genContent() {
      return this.$createElement('span', {
        staticClass: 'v-btn__content'
      }, this.$slots.default);
    },

    genLoader() {
      return this.$createElement('span', {
        class: 'v-btn__loader'
      }, this.$slots.loader || [this.$createElement(VProgressCircular, {
        props: {
          indeterminate: true,
          size: 23,
          width: 2
        }
      })]);
    }

  },

  render(h) {
    const children = [this.genContent(), this.loading && this.genLoader()];
    const setColor = !this.isFlat ? this.setBackgroundColor : this.setTextColor;
    const {
      tag,
      data
    } = this.generateRouteLink();

    if (tag === 'button') {
      data.attrs.type = this.type;
      data.attrs.disabled = this.disabled;
    }

    data.attrs.value = ['string', 'number'].includes(typeof this.value) ? this.value : JSON.stringify(this.value);
    return h(tag, this.disabled ? data : setColor(this.color, data), children);
  }

});

var Transitionable = Vue.extend({
  name: 'transitionable',
  props: {
    mode: String,
    origin: String,
    transition: String
  }
});

const pattern = {
  styleList: /;(?![^(]*\))/g,
  styleProp: /:(.*)/
};

function parseStyle(style) {
  const styleMap = {};

  for (const s of style.split(pattern.styleList)) {
    let [key, val] = s.split(pattern.styleProp);
    key = key.trim();

    if (!key) {
      continue;
    } // May be undefined if the `key: value` pair is incomplete.


    if (typeof val === 'string') {
      val = val.trim();
    }

    styleMap[camelize(key)] = val;
  }

  return styleMap;
}

function mergeData() {
  const mergeTarget = {};
  let i = arguments.length;
  let prop;
  let event; // Allow for variadic argument length.

  while (i--) {
    // Iterate through the data properties and execute merge strategies
    // Object.keys eliminates need for hasOwnProperty call
    for (prop of Object.keys(arguments[i])) {
      switch (prop) {
        // Array merge strategy (array concatenation)
        case 'class':
        case 'style':
        case 'directives':
          if (!arguments[i][prop]) {
            break;
          }

          if (!Array.isArray(mergeTarget[prop])) {
            mergeTarget[prop] = [];
          }

          if (prop === 'style') {
            let style;

            if (Array.isArray(arguments[i].style)) {
              style = arguments[i].style;
            } else {
              style = [arguments[i].style];
            }

            for (let j = 0; j < style.length; j++) {
              const s = style[j];

              if (typeof s === 'string') {
                style[j] = parseStyle(s);
              }
            }

            arguments[i].style = style;
          } // Repackaging in an array allows Vue runtime
          // to merge class/style bindings regardless of type.


          mergeTarget[prop] = mergeTarget[prop].concat(arguments[i][prop]);
          break;
        // Space delimited string concatenation strategy

        case 'staticClass':
          if (!arguments[i][prop]) {
            break;
          }

          if (mergeTarget[prop] === undefined) {
            mergeTarget[prop] = '';
          }

          if (mergeTarget[prop]) {
            // Not an empty string, so concatenate
            mergeTarget[prop] += ' ';
          }

          mergeTarget[prop] += arguments[i][prop].trim();
          break;
        // Object, the properties of which to merge via array merge strategy (array concatenation).
        // Callback merge strategy merges callbacks to the beginning of the array,
        // so that the last defined callback will be invoked first.
        // This is done since to mimic how Object.assign merging
        // uses the last given value to assign.

        case 'on':
        case 'nativeOn':
          if (!arguments[i][prop]) {
            break;
          }

          if (!mergeTarget[prop]) {
            mergeTarget[prop] = {};
          }

          const listeners = mergeTarget[prop];

          for (event of Object.keys(arguments[i][prop] || {})) {
            // Concat function to array of functions if callback present.
            if (listeners[event]) {
              // Insert current iteration data in beginning of merged array.
              listeners[event] = Array().concat( // eslint-disable-line
              listeners[event], arguments[i][prop][event]);
            } else {
              // Straight assign.
              listeners[event] = arguments[i][prop][event];
            }
          }

          break;
        // Object merge strategy

        case 'attrs':
        case 'props':
        case 'domProps':
        case 'scopedSlots':
        case 'staticStyle':
        case 'hook':
        case 'transition':
          if (!arguments[i][prop]) {
            break;
          }

          if (!mergeTarget[prop]) {
            mergeTarget[prop] = {};
          }

          mergeTarget[prop] = { ...arguments[i][prop],
            ...mergeTarget[prop]
          };
          break;
        // Reassignment strategy (no merge)

        case 'slot':
        case 'key':
        case 'ref':
        case 'tag':
        case 'show':
        case 'keepAlive':
        default:
          if (!mergeTarget[prop]) {
            mergeTarget[prop] = arguments[i][prop];
          }

      }
    }
  }

  return mergeTarget;
}

function mergeTransitions(dest = [], ...transitions) {
  /* eslint-disable-next-line no-array-constructor */
  return Array().concat(dest, ...transitions);
}

function createSimpleTransition(name, origin = 'top center 0', mode) {
  return {
    name,
    functional: true,
    props: {
      group: {
        type: Boolean,
        default: false
      },
      hideOnLeave: {
        type: Boolean,
        default: false
      },
      leaveAbsolute: {
        type: Boolean,
        default: false
      },
      mode: {
        type: String,
        default: mode
      },
      origin: {
        type: String,
        default: origin
      }
    },

    render(h, context) {
      const tag = `transition${context.props.group ? '-group' : ''}`;
      const data = {
        props: {
          name,
          mode: context.props.mode
        },
        on: {
          beforeEnter(el) {
            el.style.transformOrigin = context.props.origin;
            el.style.webkitTransformOrigin = context.props.origin;
          }

        }
      };

      if (context.props.leaveAbsolute) {
        data.on.leave = mergeTransitions(data.on.leave, el => el.style.position = 'absolute');
      }

      if (context.props.hideOnLeave) {
        data.on.leave = mergeTransitions(data.on.leave, el => el.style.display = 'none');
      }

      return h(tag, mergeData(context.data, data), context.children);
    }

  };
}
function createJavascriptTransition(name, functions, mode = 'in-out') {
  return {
    name,
    functional: true,
    props: {
      mode: {
        type: String,
        default: mode
      }
    },

    render(h, context) {
      return h('transition', mergeData(context.data, {
        props: {
          name
        },
        on: functions
      }), context.children);
    }

  };
}

function ExpandTransitionGenerator (expandedParentClass = '', x = false) {
  const sizeProperty = x ? 'width' : 'height';
  const offsetProperty = `offset${upperFirst(sizeProperty)}`;
  return {
    beforeEnter(el) {
      el._parent = el.parentNode;
      el._initialStyle = {
        transition: el.style.transition,
        visibility: el.style.visibility,
        overflow: el.style.overflow,
        [sizeProperty]: el.style[sizeProperty]
      };
    },

    enter(el) {
      const initialStyle = el._initialStyle;
      const offset = `${el[offsetProperty]}px`;
      el.style.setProperty('transition', 'none', 'important');
      el.style.visibility = 'hidden';
      el.style.visibility = initialStyle.visibility;
      el.style.overflow = 'hidden';
      el.style[sizeProperty] = '0';
      void el.offsetHeight; // force reflow

      el.style.transition = initialStyle.transition;

      if (expandedParentClass && el._parent) {
        el._parent.classList.add(expandedParentClass);
      }

      requestAnimationFrame(() => {
        el.style[sizeProperty] = offset;
      });
    },

    afterEnter: resetStyles,
    enterCancelled: resetStyles,

    leave(el) {
      el._initialStyle = {
        transition: '',
        visibility: '',
        overflow: el.style.overflow,
        [sizeProperty]: el.style[sizeProperty]
      };
      el.style.overflow = 'hidden';
      el.style[sizeProperty] = `${el[offsetProperty]}px`;
      void el.offsetHeight; // force reflow

      requestAnimationFrame(() => el.style[sizeProperty] = '0');
    },

    afterLeave,
    leaveCancelled: afterLeave
  };

  function afterLeave(el) {
    if (expandedParentClass && el._parent) {
      el._parent.classList.remove(expandedParentClass);
    }

    resetStyles(el);
  }

  function resetStyles(el) {
    const size = el._initialStyle[sizeProperty];
    el.style.overflow = el._initialStyle.overflow;
    if (size != null) el.style[sizeProperty] = size;
    delete el._initialStyle;
  }
}

const VFadeTransition = createSimpleTransition('fade-transition');
const VSlideXTransition = createSimpleTransition('slide-x-transition');

const VExpandTransition = createJavascriptTransition('expand-transition', ExpandTransitionGenerator());
const VExpandXTransition = createJavascriptTransition('expand-x-transition', ExpandTransitionGenerator('', true));

// Styles
/* @vue/component */

var VChip = mixins(Colorable, Sizeable, Routable, Themeable, factory$2('chipGroup'), factory$1('inputValue')).extend({
  name: 'v-chip',
  props: {
    active: {
      type: Boolean,
      default: true
    },
    activeClass: {
      type: String,

      default() {
        if (!this.chipGroup) return '';
        return this.chipGroup.activeClass;
      }

    },
    close: Boolean,
    closeIcon: {
      type: String,
      default: '$delete'
    },
    disabled: Boolean,
    draggable: Boolean,
    filter: Boolean,
    filterIcon: {
      type: String,
      default: '$complete'
    },
    label: Boolean,
    link: Boolean,
    outlined: Boolean,
    pill: Boolean,
    tag: {
      type: String,
      default: 'span'
    },
    textColor: String,
    value: null
  },
  data: () => ({
    proxyClass: 'v-chip--active'
  }),
  computed: {
    classes() {
      return {
        'v-chip': true,
        ...Routable.options.computed.classes.call(this),
        'v-chip--clickable': this.isClickable,
        'v-chip--disabled': this.disabled,
        'v-chip--draggable': this.draggable,
        'v-chip--label': this.label,
        'v-chip--link': this.isLink,
        'v-chip--no-color': !this.color,
        'v-chip--outlined': this.outlined,
        'v-chip--pill': this.pill,
        'v-chip--removable': this.hasClose,
        ...this.themeClasses,
        ...this.sizeableClasses,
        ...this.groupClasses
      };
    },

    hasClose() {
      return Boolean(this.close);
    },

    isClickable() {
      return Boolean(Routable.options.computed.isClickable.call(this) || this.chipGroup);
    }

  },

  created() {
    const breakingProps = [['outline', 'outlined'], ['selected', 'input-value'], ['value', 'active'], ['@input', '@active.sync']];
    /* istanbul ignore next */

    breakingProps.forEach(([original, replacement]) => {
      if (this.$attrs.hasOwnProperty(original)) breaking(original, replacement, this);
    });
  },

  methods: {
    click(e) {
      this.$emit('click', e);
      this.chipGroup && this.toggle();
    },

    genFilter() {
      const children = [];

      if (this.isActive) {
        children.push(this.$createElement(VIcon$1, {
          staticClass: 'v-chip__filter',
          props: {
            left: true
          }
        }, this.filterIcon));
      }

      return this.$createElement(VExpandXTransition, children);
    },

    genClose() {
      return this.$createElement(VIcon$1, {
        staticClass: 'v-chip__close',
        props: {
          right: true,
          size: 18
        },
        on: {
          click: e => {
            e.stopPropagation();
            e.preventDefault();
            this.$emit('click:close');
            this.$emit('update:active', false);
          }
        }
      }, this.closeIcon);
    },

    genContent() {
      return this.$createElement('span', {
        staticClass: 'v-chip__content'
      }, [this.filter && this.genFilter(), this.$slots.default, this.hasClose && this.genClose()]);
    }

  },

  render(h) {
    const children = [this.genContent()];
    let {
      tag,
      data
    } = this.generateRouteLink();
    data.attrs = { ...data.attrs,
      draggable: this.draggable ? 'true' : undefined,
      tabindex: this.chipGroup && !this.disabled ? 0 : data.attrs.tabindex
    };
    data.directives.push({
      name: 'show',
      value: this.active
    });
    data = this.setBackgroundColor(this.color, data);
    const color = this.textColor || this.outlined && this.color;
    return h(tag, this.setTextColor(color, data), children);
  }

});

// Mixins
/* @vue/component */

var VThemeProvider = Themeable.extend({
  name: 'v-theme-provider',
  props: {
    root: Boolean
  },
  computed: {
    isDark() {
      return this.root ? this.rootIsDark : Themeable.options.computed.isDark.call(this);
    }

  },

  render() {
    /* istanbul ignore next */
    return this.$slots.default && this.$slots.default.find(node => !node.isComment && node.text !== ' ');
  }

});

/**
 * Delayable
 *
 * @mixin
 *
 * Changes the open or close delay time for elements
 */

var Delayable = Vue.extend().extend({
  name: 'delayable',
  props: {
    openDelay: {
      type: [Number, String],
      default: 0
    },
    closeDelay: {
      type: [Number, String],
      default: 0
    }
  },
  data: () => ({
    openTimeout: undefined,
    closeTimeout: undefined
  }),
  methods: {
    /**
     * Clear any pending delay timers from executing
     */
    clearDelay() {
      clearTimeout(this.openTimeout);
      clearTimeout(this.closeTimeout);
    },

    /**
     * Runs callback after a specified delay
     */
    runDelay(type, cb) {
      this.clearDelay();
      const delay = parseInt(this[`${type}Delay`], 10);
      this[`${type}Timeout`] = setTimeout(cb || (() => {
        this.isActive = {
          open: true,
          close: false
        }[type];
      }), delay);
    }

  }
});

// Mixins
const baseMixins$1 = mixins(Delayable, Toggleable);
/* @vue/component */

var Activatable = baseMixins$1.extend({
  name: 'activatable',
  props: {
    activator: {
      default: null,
      validator: val => {
        return ['string', 'object'].includes(typeof val);
      }
    },
    disabled: Boolean,
    internalActivator: Boolean,
    openOnHover: Boolean
  },
  data: () => ({
    // Do not use this directly, call getActivator() instead
    activatorElement: null,
    activatorNode: [],
    events: ['click', 'mouseenter', 'mouseleave'],
    listeners: {}
  }),
  watch: {
    activator: 'resetActivator',
    openOnHover: 'resetActivator'
  },

  mounted() {
    const slotType = getSlotType(this, 'activator', true);

    if (slotType && ['v-slot', 'normal'].includes(slotType)) {
      consoleError(`The activator slot must be bound, try '<template v-slot:activator="{ on }"><v-btn v-on="on">'`, this);
    }

    this.addActivatorEvents();
  },

  beforeDestroy() {
    this.removeActivatorEvents();
  },

  methods: {
    addActivatorEvents() {
      if (!this.activator || this.disabled || !this.getActivator()) return;
      this.listeners = this.genActivatorListeners();
      const keys = Object.keys(this.listeners);

      for (const key of keys) {
        this.getActivator().addEventListener(key, this.listeners[key]);
      }
    },

    genActivator() {
      const node = getSlot(this, 'activator', Object.assign(this.getValueProxy(), {
        on: this.genActivatorListeners(),
        attrs: this.genActivatorAttributes()
      })) || [];
      this.activatorNode = node;
      return node;
    },

    genActivatorAttributes() {
      return {
        role: 'button',
        'aria-haspopup': true,
        'aria-expanded': String(this.isActive)
      };
    },

    genActivatorListeners() {
      if (this.disabled) return {};
      const listeners = {};

      if (this.openOnHover) {
        listeners.mouseenter = e => {
          this.getActivator(e);
          this.runDelay('open');
        };

        listeners.mouseleave = e => {
          this.getActivator(e);
          this.runDelay('close');
        };
      } else {
        listeners.click = e => {
          const activator = this.getActivator(e);
          if (activator) activator.focus();
          e.stopPropagation();
          this.isActive = !this.isActive;
        };
      }

      return listeners;
    },

    getActivator(e) {
      // If we've already fetched the activator, re-use
      if (this.activatorElement) return this.activatorElement;
      let activator = null;

      if (this.activator) {
        const target = this.internalActivator ? this.$el : document;

        if (typeof this.activator === 'string') {
          // Selector
          activator = target.querySelector(this.activator);
        } else if (this.activator.$el) {
          // Component (ref)
          activator = this.activator.$el;
        } else {
          // HTMLElement | Element
          activator = this.activator;
        }
      } else if (this.activatorNode.length === 1 || this.activatorNode.length && !e) {
        // Use the contents of the activator slot
        // There's either only one element in it or we
        // don't have a click event to use as a last resort
        const vm = this.activatorNode[0].componentInstance;

        if (vm && vm.$options.mixins && //                         Activatable is indirectly used via Menuable
        vm.$options.mixins.some(m => m.options && ['activatable', 'menuable'].includes(m.options.name))) {
          // Activator is actually another activatible component, use its activator (#8846)
          activator = vm.getActivator();
        } else {
          activator = this.activatorNode[0].elm;
        }
      } else if (e) {
        // Activated by a click event
        activator = e.currentTarget || e.target;
      }

      this.activatorElement = activator;
      return this.activatorElement;
    },

    getContentSlot() {
      return getSlot(this, 'default', this.getValueProxy(), true);
    },

    getValueProxy() {
      const self = this;
      return {
        get value() {
          return self.isActive;
        },

        set value(isActive) {
          self.isActive = isActive;
        }

      };
    },

    removeActivatorEvents() {
      if (!this.activator || !this.activatorElement) return;
      const keys = Object.keys(this.listeners);

      for (const key of keys) {
        this.activatorElement.removeEventListener(key, this.listeners[key]);
      }

      this.listeners = {};
    },

    resetActivator() {
      this.removeActivatorEvents();
      this.activatorElement = null;
      this.getActivator();
      this.addActivatorEvents();
    }

  }
});

function searchChildren(children) {
  const results = [];

  for (let index = 0; index < children.length; index++) {
    const child = children[index];

    if (child.isActive && child.isDependent) {
      results.push(child);
    } else {
      results.push(...searchChildren(child.$children));
    }
  }

  return results;
}
/* @vue/component */


var Dependent = mixins().extend({
  name: 'dependent',

  data() {
    return {
      closeDependents: true,
      isActive: false,
      isDependent: true
    };
  },

  watch: {
    isActive(val) {
      if (val) return;
      const openDependents = this.getOpenDependents();

      for (let index = 0; index < openDependents.length; index++) {
        openDependents[index].isActive = false;
      }
    }

  },
  methods: {
    getOpenDependents() {
      if (this.closeDependents) return searchChildren(this.$children);
      return [];
    },

    getOpenDependentElements() {
      const result = [];
      const openDependents = this.getOpenDependents();

      for (let index = 0; index < openDependents.length; index++) {
        result.push(...openDependents[index].getClickableDependentElements());
      }

      return result;
    },

    getClickableDependentElements() {
      const result = [this.$el];
      if (this.$refs.content) result.push(this.$refs.content);
      if (this.overlay) result.push(this.overlay.$el);
      result.push(...this.getOpenDependentElements());
      return result;
    }

  }
});

// Utilities
/**
 * Bootable
 * @mixin
 *
 * Used to add lazy content functionality to components
 * Looks for change in "isActive" to automatically boot
 * Otherwise can be set manually
 */

/* @vue/component */

var Bootable = Vue.extend().extend({
  name: 'bootable',
  props: {
    eager: Boolean
  },
  data: () => ({
    isBooted: false
  }),
  computed: {
    hasContent() {
      return this.isBooted || this.eager || this.isActive;
    }

  },
  watch: {
    isActive() {
      this.isBooted = true;
    }

  },

  created() {
    /* istanbul ignore next */
    if ('lazy' in this.$attrs) {
      removed('lazy', this);
    }
  },

  methods: {
    showLazyContent(content) {
      return this.hasContent && content ? content() : [this.$createElement()];
    }

  }
});

// Mixins

function validateAttachTarget(val) {
  const type = typeof val;
  if (type === 'boolean' || type === 'string') return true;
  return val.nodeType === Node.ELEMENT_NODE;
}
/* @vue/component */


var Detachable = mixins(Bootable).extend({
  name: 'detachable',
  props: {
    attach: {
      default: false,
      validator: validateAttachTarget
    },
    contentClass: {
      type: String,
      default: ''
    }
  },
  data: () => ({
    activatorNode: null,
    hasDetached: false
  }),
  watch: {
    attach() {
      this.hasDetached = false;
      this.initDetach();
    },

    hasContent() {
      this.$nextTick(this.initDetach);
    }

  },

  beforeMount() {
    this.$nextTick(() => {
      if (this.activatorNode) {
        const activator = Array.isArray(this.activatorNode) ? this.activatorNode : [this.activatorNode];
        activator.forEach(node => {
          if (!node.elm) return;
          if (!this.$el.parentNode) return;
          const target = this.$el === this.$el.parentNode.firstChild ? this.$el : this.$el.nextSibling;
          this.$el.parentNode.insertBefore(node.elm, target);
        });
      }
    });
  },

  mounted() {
    this.hasContent && this.initDetach();
  },

  deactivated() {
    this.isActive = false;
  },

  beforeDestroy() {
    // IE11 Fix
    try {
      if (this.$refs.content && this.$refs.content.parentNode) {
        this.$refs.content.parentNode.removeChild(this.$refs.content);
      }

      if (this.activatorNode) {
        const activator = Array.isArray(this.activatorNode) ? this.activatorNode : [this.activatorNode];
        activator.forEach(node => {
          node.elm && node.elm.parentNode && node.elm.parentNode.removeChild(node.elm);
        });
      }
    } catch (e) {
      console.log(e);
    }
  },

  methods: {
    getScopeIdAttrs() {
      const scopeId = getObjectValueByPath(this.$vnode, 'context.$options._scopeId');
      return scopeId && {
        [scopeId]: ''
      };
    },

    initDetach() {
      if (this._isDestroyed || !this.$refs.content || this.hasDetached || // Leave menu in place if attached
      // and dev has not changed target
      this.attach === '' || // If used as a boolean prop (<v-menu attach>)
      this.attach === true || // If bound to a boolean (<v-menu :attach="true">)
      this.attach === 'attach' // If bound as boolean prop in pug (v-menu(attach))
      ) return;
      let target;

      if (this.attach === false) {
        // Default, detach to app
        target = document.querySelector('[data-app]');
      } else if (typeof this.attach === 'string') {
        // CSS selector
        target = document.querySelector(this.attach);
      } else {
        // DOM Element
        target = this.attach;
      }

      if (!target) {
        consoleWarn(`Unable to locate target ${this.attach || '[data-app]'}`, this);
        return;
      }

      target.appendChild(this.$refs.content);
      this.hasDetached = true;
    }

  }
});

/* @vue/component */

var Stackable = Vue.extend().extend({
  name: 'stackable',

  data() {
    return {
      stackElement: null,
      stackExclude: null,
      stackMinZIndex: 0,
      isActive: false
    };
  },

  computed: {
    activeZIndex() {
      if (typeof window === 'undefined') return 0;
      const content = this.stackElement || this.$refs.content; // Return current zindex if not active

      const index = !this.isActive ? getZIndex(content) : this.getMaxZIndex(this.stackExclude || [content]) + 2;
      if (index == null) return index; // Return max current z-index (excluding self) + 2
      // (2 to leave room for an overlay below, if needed)

      return parseInt(index);
    }

  },
  methods: {
    getMaxZIndex(exclude = []) {
      const base = this.$el; // Start with lowest allowed z-index or z-index of
      // base component's element, whichever is greater

      const zis = [this.stackMinZIndex, getZIndex(base)]; // Convert the NodeList to an array to
      // prevent an Edge bug with Symbol.iterator
      // https://github.com/vuetifyjs/vuetify/issues/2146

      const activeElements = [...document.getElementsByClassName('v-menu__content--active'), ...document.getElementsByClassName('v-dialog__content--active')]; // Get z-index for all active dialogs

      for (let index = 0; index < activeElements.length; index++) {
        if (!exclude.includes(activeElements[index])) {
          zis.push(getZIndex(activeElements[index]));
        }
      }

      return Math.max(...zis);
    }

  }
});

// Mixins

const baseMixins$2 = mixins(Stackable, Positionable, Activatable);
/* @vue/component */

var Menuable = baseMixins$2.extend().extend({
  name: 'menuable',
  props: {
    allowOverflow: Boolean,
    light: Boolean,
    dark: Boolean,
    maxWidth: {
      type: [Number, String],
      default: 'auto'
    },
    minWidth: [Number, String],
    nudgeBottom: {
      type: [Number, String],
      default: 0
    },
    nudgeLeft: {
      type: [Number, String],
      default: 0
    },
    nudgeRight: {
      type: [Number, String],
      default: 0
    },
    nudgeTop: {
      type: [Number, String],
      default: 0
    },
    nudgeWidth: {
      type: [Number, String],
      default: 0
    },
    offsetOverflow: Boolean,
    openOnClick: Boolean,
    positionX: {
      type: Number,
      default: null
    },
    positionY: {
      type: Number,
      default: null
    },
    zIndex: {
      type: [Number, String],
      default: null
    }
  },
  data: () => ({
    absoluteX: 0,
    absoluteY: 0,
    activatedBy: null,
    activatorFixed: false,
    dimensions: {
      activator: {
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        width: 0,
        height: 0,
        offsetTop: 0,
        scrollHeight: 0,
        offsetLeft: 0
      },
      content: {
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        width: 0,
        height: 0,
        offsetTop: 0,
        scrollHeight: 0
      }
    },
    hasJustFocused: false,
    hasWindow: false,
    inputActivator: false,
    isContentActive: false,
    pageWidth: 0,
    pageYOffset: 0,
    stackClass: 'v-menu__content--active',
    stackMinZIndex: 6
  }),
  computed: {
    computedLeft() {
      const a = this.dimensions.activator;
      const c = this.dimensions.content;
      const activatorLeft = (this.attach !== false ? a.offsetLeft : a.left) || 0;
      const minWidth = Math.max(a.width, c.width);
      let left = 0;
      left += this.left ? activatorLeft - (minWidth - a.width) : activatorLeft;

      if (this.offsetX) {
        const maxWidth = isNaN(Number(this.maxWidth)) ? a.width : Math.min(a.width, Number(this.maxWidth));
        left += this.left ? -maxWidth : a.width;
      }

      if (this.nudgeLeft) left -= parseInt(this.nudgeLeft);
      if (this.nudgeRight) left += parseInt(this.nudgeRight);
      return left;
    },

    computedTop() {
      const a = this.dimensions.activator;
      const c = this.dimensions.content;
      let top = 0;
      if (this.top) top += a.height - c.height;
      if (this.attach !== false) top += a.offsetTop;else top += a.top + this.pageYOffset;
      if (this.offsetY) top += this.top ? -a.height : a.height;
      if (this.nudgeTop) top -= parseInt(this.nudgeTop);
      if (this.nudgeBottom) top += parseInt(this.nudgeBottom);
      return top;
    },

    hasActivator() {
      return !!this.$slots.activator || !!this.$scopedSlots.activator || !!this.activator || !!this.inputActivator;
    }

  },
  watch: {
    disabled(val) {
      val && this.callDeactivate();
    },

    isActive(val) {
      if (this.disabled) return;
      val ? this.callActivate() : this.callDeactivate();
    },

    positionX: 'updateDimensions',
    positionY: 'updateDimensions'
  },

  beforeMount() {
    this.hasWindow = typeof window !== 'undefined';
  },

  methods: {
    absolutePosition() {
      return {
        offsetTop: 0,
        offsetLeft: 0,
        scrollHeight: 0,
        top: this.positionY || this.absoluteY,
        bottom: this.positionY || this.absoluteY,
        left: this.positionX || this.absoluteX,
        right: this.positionX || this.absoluteX,
        height: 0,
        width: 0
      };
    },

    activate() {},

    calcLeft(menuWidth) {
      return convertToUnit(this.attach !== false ? this.computedLeft : this.calcXOverflow(this.computedLeft, menuWidth));
    },

    calcTop() {
      return convertToUnit(this.attach !== false ? this.computedTop : this.calcYOverflow(this.computedTop));
    },

    calcXOverflow(left, menuWidth) {
      const xOverflow = left + menuWidth - this.pageWidth + 12;

      if ((!this.left || this.right) && xOverflow > 0) {
        left = Math.max(left - xOverflow, 0);
      } else {
        left = Math.max(left, 12);
      }

      return left + this.getOffsetLeft();
    },

    calcYOverflow(top) {
      const documentHeight = this.getInnerHeight();
      const toTop = this.pageYOffset + documentHeight;
      const activator = this.dimensions.activator;
      const contentHeight = this.dimensions.content.height;
      const totalHeight = top + contentHeight;
      const isOverflowing = toTop < totalHeight; // If overflowing bottom and offset
      // TODO: set 'bottom' position instead of 'top'

      if (isOverflowing && this.offsetOverflow && // If we don't have enough room to offset
      // the overflow, don't offset
      activator.top > contentHeight) {
        top = this.pageYOffset + (activator.top - contentHeight); // If overflowing bottom
      } else if (isOverflowing && !this.allowOverflow) {
        top = toTop - contentHeight - 12; // If overflowing top
      } else if (top < this.pageYOffset && !this.allowOverflow) {
        top = this.pageYOffset + 12;
      }

      return top < 12 ? 12 : top;
    },

    callActivate() {
      if (!this.hasWindow) return;
      this.activate();
    },

    callDeactivate() {
      this.isContentActive = false;
      this.deactivate();
    },

    checkForPageYOffset() {
      if (this.hasWindow) {
        this.pageYOffset = this.activatorFixed ? 0 : this.getOffsetTop();
      }
    },

    checkActivatorFixed() {
      if (this.attach !== false) return;
      let el = this.getActivator();

      while (el) {
        if (window.getComputedStyle(el).position === 'fixed') {
          this.activatorFixed = true;
          return;
        }

        el = el.offsetParent;
      }

      this.activatorFixed = false;
    },

    deactivate() {},

    genActivatorListeners() {
      const listeners = Activatable.options.methods.genActivatorListeners.call(this);
      const onClick = listeners.click;

      listeners.click = e => {
        if (this.openOnClick) {
          onClick && onClick(e);
        }

        this.absoluteX = e.clientX;
        this.absoluteY = e.clientY;
      };

      return listeners;
    },

    getInnerHeight() {
      if (!this.hasWindow) return 0;
      return window.innerHeight || document.documentElement.clientHeight;
    },

    getOffsetLeft() {
      if (!this.hasWindow) return 0;
      return window.pageXOffset || document.documentElement.scrollLeft;
    },

    getOffsetTop() {
      if (!this.hasWindow) return 0;
      return window.pageYOffset || document.documentElement.scrollTop;
    },

    getRoundedBoundedClientRect(el) {
      const rect = el.getBoundingClientRect();
      return {
        top: Math.round(rect.top),
        left: Math.round(rect.left),
        bottom: Math.round(rect.bottom),
        right: Math.round(rect.right),
        width: Math.round(rect.width),
        height: Math.round(rect.height)
      };
    },

    measure(el) {
      if (!el || !this.hasWindow) return null;
      const rect = this.getRoundedBoundedClientRect(el); // Account for activator margin

      if (this.attach !== false) {
        const style = window.getComputedStyle(el);
        rect.left = parseInt(style.marginLeft);
        rect.top = parseInt(style.marginTop);
      }

      return rect;
    },

    sneakPeek(cb) {
      requestAnimationFrame(() => {
        const el = this.$refs.content;

        if (!el || el.style.display !== 'none') {
          cb();
          return;
        }

        el.style.display = 'inline-block';
        cb();
        el.style.display = 'none';
      });
    },

    startTransition() {
      return new Promise(resolve => requestAnimationFrame(() => {
        this.isContentActive = this.hasJustFocused = this.isActive;
        resolve();
      }));
    },

    updateDimensions() {
      this.hasWindow = typeof window !== 'undefined';
      this.checkActivatorFixed();
      this.checkForPageYOffset();
      this.pageWidth = document.documentElement.clientWidth;
      const dimensions = {
        activator: { ...this.dimensions.activator
        },
        content: { ...this.dimensions.content
        }
      }; // Activator should already be shown

      if (!this.hasActivator || this.absolute) {
        dimensions.activator = this.absolutePosition();
      } else {
        const activator = this.getActivator();
        if (!activator) return;
        dimensions.activator = this.measure(activator);
        dimensions.activator.offsetLeft = activator.offsetLeft;

        if (this.attach !== false) {
          // account for css padding causing things to not line up
          // this is mostly for v-autocomplete, hopefully it won't break anything
          dimensions.activator.offsetTop = activator.offsetTop;
        } else {
          dimensions.activator.offsetTop = 0;
        }
      } // Display and hide to get dimensions


      this.sneakPeek(() => {
        this.$refs.content && (dimensions.content = this.measure(this.$refs.content));
        this.dimensions = dimensions;
      });
    }

  }
});

/* @vue/component */

var Returnable = Vue.extend({
  name: 'returnable',
  props: {
    returnValue: null
  },
  data: () => ({
    isActive: false,
    originalValue: null
  }),
  watch: {
    isActive(val) {
      if (val) {
        this.originalValue = this.returnValue;
      } else {
        this.$emit('update:return-value', this.originalValue);
      }
    }

  },
  methods: {
    save(value) {
      this.originalValue = value;
      setTimeout(() => {
        this.isActive = false;
      });
    }

  }
});

// Styles
const baseMixins$3 = mixins(Dependent, Delayable, Detachable, Menuable, Returnable, Toggleable, Themeable);
/* @vue/component */

var VMenu = baseMixins$3.extend({
  name: 'v-menu',

  provide() {
    return {
      isInMenu: true,
      // Pass theme through to default slot
      theme: this.theme
    };
  },

  directives: {
    ClickOutside,
    Resize
  },
  props: {
    auto: Boolean,
    closeOnClick: {
      type: Boolean,
      default: true
    },
    closeOnContentClick: {
      type: Boolean,
      default: true
    },
    disabled: Boolean,
    disableKeys: Boolean,
    maxHeight: {
      type: [Number, String],
      default: 'auto'
    },
    offsetX: Boolean,
    offsetY: Boolean,
    openOnClick: {
      type: Boolean,
      default: true
    },
    openOnHover: Boolean,
    origin: {
      type: String,
      default: 'top left'
    },
    transition: {
      type: [Boolean, String],
      default: 'v-menu-transition'
    }
  },

  data() {
    return {
      calculatedTopAuto: 0,
      defaultOffset: 8,
      hasJustFocused: false,
      listIndex: -1,
      resizeTimeout: 0,
      selectedIndex: null,
      tiles: []
    };
  },

  computed: {
    activeTile() {
      return this.tiles[this.listIndex];
    },

    calculatedLeft() {
      const menuWidth = Math.max(this.dimensions.content.width, parseFloat(this.calculatedMinWidth));
      if (!this.auto) return this.calcLeft(menuWidth) || '0';
      return convertToUnit(this.calcXOverflow(this.calcLeftAuto(), menuWidth)) || '0';
    },

    calculatedMaxHeight() {
      const height = this.auto ? '200px' : convertToUnit(this.maxHeight);
      return height || '0';
    },

    calculatedMaxWidth() {
      return convertToUnit(this.maxWidth) || '0';
    },

    calculatedMinWidth() {
      if (this.minWidth) {
        return convertToUnit(this.minWidth) || '0';
      }

      const minWidth = Math.min(this.dimensions.activator.width + Number(this.nudgeWidth) + (this.auto ? 16 : 0), Math.max(this.pageWidth - 24, 0));
      const calculatedMaxWidth = isNaN(parseInt(this.calculatedMaxWidth)) ? minWidth : parseInt(this.calculatedMaxWidth);
      return convertToUnit(Math.min(calculatedMaxWidth, minWidth)) || '0';
    },

    calculatedTop() {
      const top = !this.auto ? this.calcTop() : convertToUnit(this.calcYOverflow(this.calculatedTopAuto));
      return top || '0';
    },

    hasClickableTiles() {
      return Boolean(this.tiles.find(tile => tile.tabIndex > -1));
    },

    styles() {
      return {
        maxHeight: this.calculatedMaxHeight,
        minWidth: this.calculatedMinWidth,
        maxWidth: this.calculatedMaxWidth,
        top: this.calculatedTop,
        left: this.calculatedLeft,
        transformOrigin: this.origin,
        zIndex: this.zIndex || this.activeZIndex
      };
    }

  },
  watch: {
    isActive(val) {
      if (!val) this.listIndex = -1;
    },

    isContentActive(val) {
      this.hasJustFocused = val;
    },

    listIndex(next, prev) {
      if (next in this.tiles) {
        const tile = this.tiles[next];
        tile.classList.add('v-list-item--highlighted');
        this.$refs.content.scrollTop = tile.offsetTop - tile.clientHeight;
      }

      prev in this.tiles && this.tiles[prev].classList.remove('v-list-item--highlighted');
    }

  },

  created() {
    /* istanbul ignore next */
    if (this.$attrs.hasOwnProperty('full-width')) {
      removed('full-width', this);
    }
  },

  mounted() {
    this.isActive && this.callActivate();
  },

  methods: {
    activate() {
      // Update coordinates and dimensions of menu
      // and its activator
      this.updateDimensions(); // Start the transition

      requestAnimationFrame(() => {
        // Once transitioning, calculate scroll and top position
        this.startTransition().then(() => {
          if (this.$refs.content) {
            this.calculatedTopAuto = this.calcTopAuto();
            this.auto && (this.$refs.content.scrollTop = this.calcScrollPosition());
          }
        });
      });
    },

    calcScrollPosition() {
      const $el = this.$refs.content;
      const activeTile = $el.querySelector('.v-list-item--active');
      const maxScrollTop = $el.scrollHeight - $el.offsetHeight;
      return activeTile ? Math.min(maxScrollTop, Math.max(0, activeTile.offsetTop - $el.offsetHeight / 2 + activeTile.offsetHeight / 2)) : $el.scrollTop;
    },

    calcLeftAuto() {
      return parseInt(this.dimensions.activator.left - this.defaultOffset * 2);
    },

    calcTopAuto() {
      const $el = this.$refs.content;
      const activeTile = $el.querySelector('.v-list-item--active');

      if (!activeTile) {
        this.selectedIndex = null;
      }

      if (this.offsetY || !activeTile) {
        return this.computedTop;
      }

      this.selectedIndex = Array.from(this.tiles).indexOf(activeTile);
      const tileDistanceFromMenuTop = activeTile.offsetTop - this.calcScrollPosition();
      const firstTileOffsetTop = $el.querySelector('.v-list-item').offsetTop;
      return this.computedTop - tileDistanceFromMenuTop - firstTileOffsetTop - 1;
    },

    changeListIndex(e) {
      // For infinite scroll and autocomplete, re-evaluate children
      this.getTiles();

      if (!this.isActive || !this.hasClickableTiles) {
        return;
      } else if (e.keyCode === keyCodes.tab) {
        this.isActive = false;
        return;
      } else if (e.keyCode === keyCodes.down) {
        this.nextTile();
      } else if (e.keyCode === keyCodes.up) {
        this.prevTile();
      } else if (e.keyCode === keyCodes.enter && this.listIndex !== -1) {
        this.tiles[this.listIndex].click();
      } else {
        return;
      } // One of the conditions was met, prevent default action (#2988)


      e.preventDefault();
    },

    closeConditional(e) {
      const target = e.target;
      return this.isActive && !this._isDestroyed && this.closeOnClick && !this.$refs.content.contains(target);
    },

    genActivatorAttributes() {
      const attributes = Activatable.options.methods.genActivatorAttributes.call(this);

      if (this.activeTile && this.activeTile.id) {
        return { ...attributes,
          'aria-activedescendant': this.activeTile.id
        };
      }

      return attributes;
    },

    genActivatorListeners() {
      const listeners = Menuable.options.methods.genActivatorListeners.call(this);

      if (!this.disableKeys) {
        listeners.keydown = this.onKeyDown;
      }

      return listeners;
    },

    genTransition() {
      const content = this.genContent();
      if (!this.transition) return content;
      return this.$createElement('transition', {
        props: {
          name: this.transition
        }
      }, [content]);
    },

    genDirectives() {
      const directives = [{
        name: 'show',
        value: this.isContentActive
      }]; // Do not add click outside for hover menu

      if (!this.openOnHover && this.closeOnClick) {
        directives.push({
          name: 'click-outside',
          value: () => {
            this.isActive = false;
          },
          args: {
            closeConditional: this.closeConditional,
            include: () => [this.$el, ...this.getOpenDependentElements()]
          }
        });
      }

      return directives;
    },

    genContent() {
      const options = {
        attrs: { ...this.getScopeIdAttrs(),
          role: 'role' in this.$attrs ? this.$attrs.role : 'menu'
        },
        staticClass: 'v-menu__content',
        class: { ...this.rootThemeClasses,
          'v-menu__content--auto': this.auto,
          'v-menu__content--fixed': this.activatorFixed,
          menuable__content__active: this.isActive,
          [this.contentClass.trim()]: true
        },
        style: this.styles,
        directives: this.genDirectives(),
        ref: 'content',
        on: {
          click: e => {
            const target = e.target;
            if (target.getAttribute('disabled')) return;
            if (this.closeOnContentClick) this.isActive = false;
          },
          keydown: this.onKeyDown
        }
      };

      if (!this.disabled && this.openOnHover) {
        options.on = options.on || {};
        options.on.mouseenter = this.mouseEnterHandler;
      }

      if (this.openOnHover) {
        options.on = options.on || {};
        options.on.mouseleave = this.mouseLeaveHandler;
      }

      return this.$createElement('div', options, this.getContentSlot());
    },

    getTiles() {
      if (!this.$refs.content) return;
      this.tiles = Array.from(this.$refs.content.querySelectorAll('.v-list-item'));
    },

    mouseEnterHandler() {
      this.runDelay('open', () => {
        if (this.hasJustFocused) return;
        this.hasJustFocused = true;
        this.isActive = true;
      });
    },

    mouseLeaveHandler(e) {
      // Prevent accidental re-activation
      this.runDelay('close', () => {
        if (this.$refs.content.contains(e.relatedTarget)) return;
        requestAnimationFrame(() => {
          this.isActive = false;
          this.callDeactivate();
        });
      });
    },

    nextTile() {
      const tile = this.tiles[this.listIndex + 1];

      if (!tile) {
        if (!this.tiles.length) return;
        this.listIndex = -1;
        this.nextTile();
        return;
      }

      this.listIndex++;
      if (tile.tabIndex === -1) this.nextTile();
    },

    prevTile() {
      const tile = this.tiles[this.listIndex - 1];

      if (!tile) {
        if (!this.tiles.length) return;
        this.listIndex = this.tiles.length;
        this.prevTile();
        return;
      }

      this.listIndex--;
      if (tile.tabIndex === -1) this.prevTile();
    },

    onKeyDown(e) {
      if (e.keyCode === keyCodes.esc) {
        // Wait for dependent elements to close first
        setTimeout(() => {
          this.isActive = false;
        });
        const activator = this.getActivator();
        this.$nextTick(() => activator && activator.focus());
      } else if (!this.isActive && [keyCodes.up, keyCodes.down].includes(e.keyCode)) {
        this.isActive = true;
      } // Allow for isActive watcher to generate tile list


      this.$nextTick(() => this.changeListIndex(e));
    },

    onResize() {
      if (!this.isActive) return; // Account for screen resize
      // and orientation change
      // eslint-disable-next-line no-unused-expressions

      this.$refs.content.offsetWidth;
      this.updateDimensions(); // When resizing to a smaller width
      // content width is evaluated before
      // the new activator width has been
      // set, causing it to not size properly
      // hacky but will revisit in the future

      clearTimeout(this.resizeTimeout);
      this.resizeTimeout = window.setTimeout(this.updateDimensions, 100);
    }

  },

  render(h) {
    const data = {
      staticClass: 'v-menu',
      class: {
        'v-menu--attached': this.attach === '' || this.attach === true || this.attach === 'attach'
      },
      directives: [{
        arg: '500',
        name: 'resize',
        value: this.onResize
      }]
    };
    return h('div', data, [!this.activator && this.genActivator(), this.showLazyContent(() => [this.$createElement(VThemeProvider, {
      props: {
        root: true,
        light: this.light,
        dark: this.dark
      }
    }, [this.genTransition()])])]);
  }

});

var VSimpleCheckbox = Vue.extend({
  name: 'v-simple-checkbox',
  functional: true,
  directives: {
    ripple: Ripple
  },
  props: { ...Colorable.options.props,
    ...Themeable.options.props,
    disabled: Boolean,
    ripple: {
      type: Boolean,
      default: true
    },
    value: Boolean,
    indeterminate: Boolean,
    indeterminateIcon: {
      type: String,
      default: '$checkboxIndeterminate'
    },
    onIcon: {
      type: String,
      default: '$checkboxOn'
    },
    offIcon: {
      type: String,
      default: '$checkboxOff'
    }
  },

  render(h, {
    props,
    data
  }) {
    const children = [];

    if (props.ripple && !props.disabled) {
      const ripple = h('div', Colorable.options.methods.setTextColor(props.color, {
        staticClass: 'v-input--selection-controls__ripple',
        directives: [{
          name: 'ripple',
          value: {
            center: true
          }
        }]
      }));
      children.push(ripple);
    }

    let icon = props.offIcon;
    if (props.indeterminate) icon = props.indeterminateIcon;else if (props.value) icon = props.onIcon;
    children.push(h(VIcon$1, Colorable.options.methods.setTextColor(props.value && props.color, {
      props: {
        disabled: props.disabled,
        dark: props.dark,
        light: props.light
      }
    }), icon));
    const classes = {
      'v-simple-checkbox': true,
      'v-simple-checkbox--disabled': props.disabled
    };
    return h('div', { ...data,
      class: classes,
      on: {
        click: e => {
          e.stopPropagation();

          if (data.on && data.on.input && !props.disabled) {
            wrapInArray(data.on.input).forEach(f => f(!props.value));
          }
        }
      }
    }, children);
  }

});

// Styles
var VDivider = Themeable.extend({
  name: 'v-divider',
  props: {
    inset: Boolean,
    vertical: Boolean
  },

  render(h) {
    // WAI-ARIA attributes
    let orientation;

    if (!this.$attrs.role || this.$attrs.role === 'separator') {
      orientation = this.vertical ? 'vertical' : 'horizontal';
    }

    return h('hr', {
      class: {
        'v-divider': true,
        'v-divider--inset': this.inset,
        'v-divider--vertical': this.vertical,
        ...this.themeClasses
      },
      attrs: {
        role: 'separator',
        'aria-orientation': orientation,
        ...this.$attrs
      },
      on: this.$listeners
    });
  }

});

// Styles
var VSubheader = mixins(Themeable
/* @vue/component */
).extend({
  name: 'v-subheader',
  props: {
    inset: Boolean
  },

  render(h) {
    return h('div', {
      staticClass: 'v-subheader',
      class: {
        'v-subheader--inset': this.inset,
        ...this.themeClasses
      },
      attrs: this.$attrs,
      on: this.$listeners
    }, this.$slots.default);
  }

});

// Styles
/* @vue/component */

var VList = VSheet.extend().extend({
  name: 'v-list',

  provide() {
    return {
      isInList: true,
      list: this
    };
  },

  inject: {
    isInMenu: {
      default: false
    },
    isInNav: {
      default: false
    }
  },
  props: {
    dense: Boolean,
    disabled: Boolean,
    expand: Boolean,
    flat: Boolean,
    nav: Boolean,
    rounded: Boolean,
    shaped: Boolean,
    subheader: Boolean,
    threeLine: Boolean,
    tile: {
      type: Boolean,
      default: true
    },
    twoLine: Boolean
  },
  data: () => ({
    groups: []
  }),
  computed: {
    classes() {
      return { ...VSheet.options.computed.classes.call(this),
        'v-list--dense': this.dense,
        'v-list--disabled': this.disabled,
        'v-list--flat': this.flat,
        'v-list--nav': this.nav,
        'v-list--rounded': this.rounded,
        'v-list--shaped': this.shaped,
        'v-list--subheader': this.subheader,
        'v-list--two-line': this.twoLine,
        'v-list--three-line': this.threeLine
      };
    }

  },
  methods: {
    register(content) {
      this.groups.push(content);
    },

    unregister(content) {
      const index = this.groups.findIndex(g => g._uid === content._uid);
      if (index > -1) this.groups.splice(index, 1);
    },

    listClick(uid) {
      if (this.expand) return;

      for (const group of this.groups) {
        group.toggle(uid);
      }
    }

  },

  render(h) {
    const data = {
      staticClass: 'v-list',
      class: this.classes,
      style: this.styles,
      attrs: {
        role: this.isInNav || this.isInMenu ? undefined : 'list',
        ...this.attrs$
      }
    };
    return h(this.tag, this.setBackgroundColor(this.color, data), [this.$slots.default]);
  }

});

// Styles
const baseMixins$4 = mixins(Colorable, Routable, Themeable, factory$2('listItemGroup'), factory$1('inputValue'));
/* @vue/component */

var VListItem = baseMixins$4.extend().extend({
  name: 'v-list-item',
  directives: {
    Ripple
  },
  inheritAttrs: false,
  inject: {
    isInGroup: {
      default: false
    },
    isInList: {
      default: false
    },
    isInMenu: {
      default: false
    },
    isInNav: {
      default: false
    }
  },
  props: {
    activeClass: {
      type: String,

      default() {
        if (!this.listItemGroup) return '';
        return this.listItemGroup.activeClass;
      }

    },
    dense: Boolean,
    inactive: Boolean,
    link: Boolean,
    selectable: {
      type: Boolean
    },
    tag: {
      type: String,
      default: 'div'
    },
    threeLine: Boolean,
    twoLine: Boolean,
    value: null
  },
  data: () => ({
    proxyClass: 'v-list-item--active'
  }),
  computed: {
    classes() {
      return {
        'v-list-item': true,
        ...Routable.options.computed.classes.call(this),
        'v-list-item--dense': this.dense,
        'v-list-item--disabled': this.disabled,
        'v-list-item--link': this.isClickable && !this.inactive,
        'v-list-item--selectable': this.selectable,
        'v-list-item--three-line': this.threeLine,
        'v-list-item--two-line': this.twoLine,
        ...this.themeClasses
      };
    },

    isClickable() {
      return Boolean(Routable.options.computed.isClickable.call(this) || this.listItemGroup);
    }

  },

  created() {
    /* istanbul ignore next */
    if (this.$attrs.hasOwnProperty('avatar')) {
      removed('avatar', this);
    }
  },

  methods: {
    click(e) {
      if (e.detail) this.$el.blur();
      this.$emit('click', e);
      this.to || this.toggle();
    },

    genAttrs() {
      const attrs = {
        'aria-disabled': this.disabled ? true : undefined,
        tabindex: this.isClickable && !this.disabled ? 0 : -1,
        ...this.$attrs
      };

      if (this.$attrs.hasOwnProperty('role')) ; else if (this.isInNav) ; else if (this.isInGroup) {
        attrs.role = 'listitem';
        attrs['aria-selected'] = String(this.isActive);
      } else if (this.isInMenu) {
        attrs.role = this.isClickable ? 'menuitem' : undefined;
        attrs.id = attrs.id || `list-item-${this._uid}`;
      } else if (this.isInList) {
        attrs.role = 'listitem';
      }

      return attrs;
    }

  },

  render(h) {
    let {
      tag,
      data
    } = this.generateRouteLink();
    data.attrs = { ...data.attrs,
      ...this.genAttrs()
    };
    data.on = { ...data.on,
      click: this.click,
      keydown: e => {
        /* istanbul ignore else */
        if (e.keyCode === keyCodes.enter) this.click(e);
        this.$emit('keydown', e);
      }
    };
    const children = this.$scopedSlots.default ? this.$scopedSlots.default({
      active: this.isActive,
      toggle: this.toggle
    }) : this.$slots.default;
    tag = this.inactive ? 'div' : tag;
    return h(tag, this.setTextColor(this.color, data), children);
  }

});

function factory$3(prop = 'value', event = 'change') {
  return Vue.extend({
    name: 'proxyable',
    model: {
      prop,
      event
    },
    props: {
      [prop]: {
        required: false
      }
    },

    data() {
      return {
        internalLazyValue: this[prop]
      };
    },

    computed: {
      internalValue: {
        get() {
          return this.internalLazyValue;
        },

        set(val) {
          if (val === this.internalLazyValue) return;
          this.internalLazyValue = val;
          this.$emit(event, val);
        }

      }
    },
    watch: {
      [prop](val) {
        this.internalLazyValue = val;
      }

    }
  });
}
/* eslint-disable-next-line no-redeclare */

const Proxyable = factory$3();

// Styles
const BaseItemGroup = mixins(Proxyable, Themeable).extend({
  name: 'base-item-group',
  props: {
    activeClass: {
      type: String,
      default: 'v-item--active'
    },
    mandatory: Boolean,
    max: {
      type: [Number, String],
      default: null
    },
    multiple: Boolean
  },

  data() {
    return {
      // As long as a value is defined, show it
      // Otherwise, check if multiple
      // to determine which default to provide
      internalLazyValue: this.value !== undefined ? this.value : this.multiple ? [] : undefined,
      items: []
    };
  },

  computed: {
    classes() {
      return {
        'v-item-group': true,
        ...this.themeClasses
      };
    },

    selectedIndex() {
      return this.selectedItem && this.items.indexOf(this.selectedItem) || -1;
    },

    selectedItem() {
      if (this.multiple) return undefined;
      return this.selectedItems[0];
    },

    selectedItems() {
      return this.items.filter((item, index) => {
        return this.toggleMethod(this.getValue(item, index));
      });
    },

    selectedValues() {
      if (this.internalValue == null) return [];
      return Array.isArray(this.internalValue) ? this.internalValue : [this.internalValue];
    },

    toggleMethod() {
      if (!this.multiple) {
        return v => this.internalValue === v;
      }

      const internalValue = this.internalValue;

      if (Array.isArray(internalValue)) {
        return v => internalValue.includes(v);
      }

      return () => false;
    }

  },
  watch: {
    internalValue: 'updateItemsState',
    items: 'updateItemsState'
  },

  created() {
    if (this.multiple && !Array.isArray(this.internalValue)) {
      consoleWarn('Model must be bound to an array if the multiple property is true.', this);
    }
  },

  methods: {
    genData() {
      return {
        class: this.classes
      };
    },

    getValue(item, i) {
      return item.value == null || item.value === '' ? i : item.value;
    },

    onClick(item) {
      this.updateInternalValue(this.getValue(item, this.items.indexOf(item)));
    },

    register(item) {
      const index = this.items.push(item) - 1;
      item.$on('change', () => this.onClick(item)); // If no value provided and mandatory,
      // assign first registered item

      if (this.mandatory && !this.selectedValues.length) {
        this.updateMandatory();
      }

      this.updateItem(item, index);
    },

    unregister(item) {
      if (this._isDestroyed) return;
      const index = this.items.indexOf(item);
      const value = this.getValue(item, index);
      this.items.splice(index, 1);
      const valueIndex = this.selectedValues.indexOf(value); // Items is not selected, do nothing

      if (valueIndex < 0) return; // If not mandatory, use regular update process

      if (!this.mandatory) {
        return this.updateInternalValue(value);
      } // Remove the value


      if (this.multiple && Array.isArray(this.internalValue)) {
        this.internalValue = this.internalValue.filter(v => v !== value);
      } else {
        this.internalValue = undefined;
      } // If mandatory and we have no selection
      // add the last item as value

      /* istanbul ignore else */


      if (!this.selectedItems.length) {
        this.updateMandatory(true);
      }
    },

    updateItem(item, index) {
      const value = this.getValue(item, index);
      item.isActive = this.toggleMethod(value);
    },

    // https://github.com/vuetifyjs/vuetify/issues/5352
    updateItemsState() {
      this.$nextTick(() => {
        if (this.mandatory && !this.selectedItems.length) {
          return this.updateMandatory();
        } // TODO: Make this smarter so it
        // doesn't have to iterate every
        // child in an update


        this.items.forEach(this.updateItem);
      });
    },

    updateInternalValue(value) {
      this.multiple ? this.updateMultiple(value) : this.updateSingle(value);
    },

    updateMandatory(last) {
      if (!this.items.length) return;
      const items = this.items.slice();
      if (last) items.reverse();
      const item = items.find(item => !item.disabled); // If no tabs are available
      // aborts mandatory value

      if (!item) return;
      const index = this.items.indexOf(item);
      this.updateInternalValue(this.getValue(item, index));
    },

    updateMultiple(value) {
      const defaultValue = Array.isArray(this.internalValue) ? this.internalValue : [];
      const internalValue = defaultValue.slice();
      const index = internalValue.findIndex(val => val === value);
      if (this.mandatory && // Item already exists
      index > -1 && // value would be reduced below min
      internalValue.length - 1 < 1) return;
      if ( // Max is set
      this.max != null && // Item doesn't exist
      index < 0 && // value would be increased above max
      internalValue.length + 1 > this.max) return;
      index > -1 ? internalValue.splice(index, 1) : internalValue.push(value);
      this.internalValue = internalValue;
    },

    updateSingle(value) {
      const isSame = value === this.internalValue;
      if (this.mandatory && isSame) return;
      this.internalValue = isSame ? undefined : value;
    }

  },

  render(h) {
    return h('div', this.genData(), this.$slots.default);
  }

});
BaseItemGroup.extend({
  name: 'v-item-group',

  provide() {
    return {
      itemGroup: this
    };
  }

});

// Types
/* @vue/component */

var VListItemAction = Vue.extend({
  name: 'v-list-item-action',
  functional: true,

  render(h, {
    data,
    children = []
  }) {
    data.staticClass = data.staticClass ? `v-list-item__action ${data.staticClass}` : 'v-list-item__action';
    const filteredChild = children.filter(VNode => {
      return VNode.isComment === false && VNode.text !== ' ';
    });
    if (filteredChild.length > 1) data.staticClass += ' v-list-item__action--stack';
    return h('div', data, children);
  }

});

var VAvatar = mixins(Colorable, Measurable
/* @vue/component */
).extend({
  name: 'v-avatar',
  props: {
    left: Boolean,
    right: Boolean,
    size: {
      type: [Number, String],
      default: 48
    },
    tile: Boolean
  },
  computed: {
    classes() {
      return {
        'v-avatar--left': this.left,
        'v-avatar--right': this.right,
        'v-avatar--tile': this.tile
      };
    },

    styles() {
      return {
        height: convertToUnit(this.size),
        minWidth: convertToUnit(this.size),
        width: convertToUnit(this.size),
        ...this.measurableStyles
      };
    }

  },

  render(h) {
    const data = {
      staticClass: 'v-avatar',
      class: this.classes,
      style: this.styles,
      on: this.$listeners
    };
    return h('div', this.setBackgroundColor(this.color, data), this.$slots.default);
  }

});

const VListItemActionText = createSimpleFunctional('v-list-item__action-text', 'span');
const VListItemContent = createSimpleFunctional('v-list-item__content', 'div');
const VListItemTitle = createSimpleFunctional('v-list-item__title', 'div');
const VListItemSubtitle = createSimpleFunctional('v-list-item__subtitle', 'div');

// Components
/* @vue/component */

var VSelectList = mixins(Colorable, Themeable).extend({
  name: 'v-select-list',
  // https://github.com/vuejs/vue/issues/6872
  directives: {
    ripple: Ripple
  },
  props: {
    action: Boolean,
    dense: Boolean,
    hideSelected: Boolean,
    items: {
      type: Array,
      default: () => []
    },
    itemDisabled: {
      type: [String, Array, Function],
      default: 'disabled'
    },
    itemText: {
      type: [String, Array, Function],
      default: 'text'
    },
    itemValue: {
      type: [String, Array, Function],
      default: 'value'
    },
    noDataText: String,
    noFilter: Boolean,
    searchInput: null,
    selectedItems: {
      type: Array,
      default: () => []
    }
  },
  computed: {
    parsedItems() {
      return this.selectedItems.map(item => this.getValue(item));
    },

    tileActiveClass() {
      return Object.keys(this.setTextColor(this.color).class || {}).join(' ');
    },

    staticNoDataTile() {
      const tile = {
        attrs: {
          role: undefined
        },
        on: {
          mousedown: e => e.preventDefault()
        }
      };
      return this.$createElement(VListItem, tile, [this.genTileContent(this.noDataText)]);
    }

  },
  methods: {
    genAction(item, inputValue) {
      return this.$createElement(VListItemAction, [this.$createElement(VSimpleCheckbox, {
        props: {
          color: this.color,
          value: inputValue
        },
        on: {
          input: () => this.$emit('select', item)
        }
      })]);
    },

    genDivider(props) {
      return this.$createElement(VDivider, {
        props
      });
    },

    genFilteredText(text) {
      text = text || '';
      if (!this.searchInput || this.noFilter) return escapeHTML(text);
      const {
        start,
        middle,
        end
      } = this.getMaskedCharacters(text);
      return `${escapeHTML(start)}${this.genHighlight(middle)}${escapeHTML(end)}`;
    },

    genHeader(props) {
      return this.$createElement(VSubheader, {
        props
      }, props.header);
    },

    genHighlight(text) {
      return `<span class="v-list-item__mask">${escapeHTML(text)}</span>`;
    },

    getMaskedCharacters(text) {
      const searchInput = (this.searchInput || '').toString().toLocaleLowerCase();
      const index = text.toLocaleLowerCase().indexOf(searchInput);
      if (index < 0) return {
        start: '',
        middle: text,
        end: ''
      };
      const start = text.slice(0, index);
      const middle = text.slice(index, index + searchInput.length);
      const end = text.slice(index + searchInput.length);
      return {
        start,
        middle,
        end
      };
    },

    genTile({
      item,
      index,
      disabled = null,
      value = false
    }) {
      if (!value) value = this.hasItem(item);

      if (item === Object(item)) {
        disabled = disabled !== null ? disabled : this.getDisabled(item);
      }

      const tile = {
        attrs: {
          // Default behavior in list does not
          // contain aria-selected by default
          'aria-selected': String(value),
          id: `list-item-${this._uid}-${index}`,
          role: 'option'
        },
        on: {
          mousedown: e => {
            // Prevent onBlur from being called
            e.preventDefault();
          },
          click: () => disabled || this.$emit('select', item)
        },
        props: {
          activeClass: this.tileActiveClass,
          disabled,
          ripple: true,
          inputValue: value
        }
      };

      if (!this.$scopedSlots.item) {
        return this.$createElement(VListItem, tile, [this.action && !this.hideSelected && this.items.length > 0 ? this.genAction(item, value) : null, this.genTileContent(item, index)]);
      }

      const parent = this;
      const scopedSlot = this.$scopedSlots.item({
        parent,
        item,
        attrs: { ...tile.attrs,
          ...tile.props
        },
        on: tile.on
      });
      return this.needsTile(scopedSlot) ? this.$createElement(VListItem, tile, scopedSlot) : scopedSlot;
    },

    genTileContent(item, index = 0) {
      const innerHTML = this.genFilteredText(this.getText(item));
      return this.$createElement(VListItemContent, [this.$createElement(VListItemTitle, {
        domProps: {
          innerHTML
        }
      })]);
    },

    hasItem(item) {
      return this.parsedItems.indexOf(this.getValue(item)) > -1;
    },

    needsTile(slot) {
      return slot.length !== 1 || slot[0].componentOptions == null || slot[0].componentOptions.Ctor.options.name !== 'v-list-item';
    },

    getDisabled(item) {
      return Boolean(getPropertyFromItem(item, this.itemDisabled, false));
    },

    getText(item) {
      return String(getPropertyFromItem(item, this.itemText, item));
    },

    getValue(item) {
      return getPropertyFromItem(item, this.itemValue, this.getText(item));
    }

  },

  render() {
    const children = [];
    const itemsLength = this.items.length;

    for (let index = 0; index < itemsLength; index++) {
      const item = this.items[index];
      if (this.hideSelected && this.hasItem(item)) continue;
      if (item == null) children.push(this.genTile({
        item,
        index
      }));else if (item.header) children.push(this.genHeader(item));else if (item.divider) children.push(this.genDivider(item));else children.push(this.genTile({
        item,
        index
      }));
    }

    children.length || children.push(this.$slots['no-data'] || this.staticNoDataTile);
    this.$slots['prepend-item'] && children.unshift(this.$slots['prepend-item']);
    this.$slots['append-item'] && children.push(this.$slots['append-item']);
    return this.$createElement(VList, {
      staticClass: 'v-select-list',
      class: this.themeClasses,
      attrs: {
        role: 'listbox',
        tabindex: -1
      },
      props: {
        dense: this.dense
      }
    }, children);
  }

});

// Styles
/* @vue/component */

var VLabel = mixins(Themeable).extend({
  name: 'v-label',
  functional: true,
  props: {
    absolute: Boolean,
    color: {
      type: String,
      default: 'primary'
    },
    disabled: Boolean,
    focused: Boolean,
    for: String,
    left: {
      type: [Number, String],
      default: 0
    },
    right: {
      type: [Number, String],
      default: 'auto'
    },
    value: Boolean
  },

  render(h, ctx) {
    const {
      children,
      listeners,
      props
    } = ctx;
    const data = {
      staticClass: 'v-label',
      class: {
        'v-label--active': props.value,
        'v-label--is-disabled': props.disabled,
        ...functionalThemeClasses(ctx)
      },
      attrs: {
        for: props.for,
        'aria-hidden': !props.for
      },
      on: listeners,
      style: {
        left: convertToUnit(props.left),
        right: convertToUnit(props.right),
        position: props.absolute ? 'absolute' : 'relative'
      },
      ref: 'label'
    };
    return h('label', Colorable.options.methods.setTextColor(props.focused && props.color, data), children);
  }

});

// Styles
/* @vue/component */

var VMessages = mixins(Colorable, Themeable).extend({
  name: 'v-messages',
  props: {
    value: {
      type: Array,
      default: () => []
    }
  },
  methods: {
    genChildren() {
      return this.$createElement('transition-group', {
        staticClass: 'v-messages__wrapper',
        attrs: {
          name: 'message-transition',
          tag: 'div'
        }
      }, this.value.map(this.genMessage));
    },

    genMessage(message, key) {
      return this.$createElement('div', {
        staticClass: 'v-messages__message',
        key
      }, getSlot(this, 'default', {
        message,
        key
      }) || [message]);
    }

  },

  render(h) {
    return h('div', this.setTextColor(this.color, {
      staticClass: 'v-messages',
      class: this.themeClasses
    }), [this.genChildren()]);
  }

});

// Mixins
/* @vue/component */

var Validatable = mixins(Colorable, inject('form'), Themeable).extend({
  name: 'validatable',
  props: {
    disabled: Boolean,
    error: Boolean,
    errorCount: {
      type: [Number, String],
      default: 1
    },
    errorMessages: {
      type: [String, Array],
      default: () => []
    },
    messages: {
      type: [String, Array],
      default: () => []
    },
    readonly: Boolean,
    rules: {
      type: Array,
      default: () => []
    },
    success: Boolean,
    successMessages: {
      type: [String, Array],
      default: () => []
    },
    validateOnBlur: Boolean,
    value: {
      required: false
    }
  },

  data() {
    return {
      errorBucket: [],
      hasColor: false,
      hasFocused: false,
      hasInput: false,
      isFocused: false,
      isResetting: false,
      lazyValue: this.value,
      valid: false
    };
  },

  computed: {
    computedColor() {
      if (this.disabled) return undefined;
      if (this.color) return this.color; // It's assumed that if the input is on a
      // dark background, the user will want to
      // have a white color. If the entire app
      // is setup to be dark, then they will
      // like want to use their primary color

      if (this.isDark && !this.appIsDark) return 'white';else return 'primary';
    },

    hasError() {
      return this.internalErrorMessages.length > 0 || this.errorBucket.length > 0 || this.error;
    },

    // TODO: Add logic that allows the user to enable based
    // upon a good validation
    hasSuccess() {
      return this.internalSuccessMessages.length > 0 || this.success;
    },

    externalError() {
      return this.internalErrorMessages.length > 0 || this.error;
    },

    hasMessages() {
      return this.validationTarget.length > 0;
    },

    hasState() {
      if (this.disabled) return false;
      return this.hasSuccess || this.shouldValidate && this.hasError;
    },

    internalErrorMessages() {
      return this.genInternalMessages(this.errorMessages);
    },

    internalMessages() {
      return this.genInternalMessages(this.messages);
    },

    internalSuccessMessages() {
      return this.genInternalMessages(this.successMessages);
    },

    internalValue: {
      get() {
        return this.lazyValue;
      },

      set(val) {
        this.lazyValue = val;
        this.$emit('input', val);
      }

    },

    shouldValidate() {
      if (this.externalError) return true;
      if (this.isResetting) return false;
      return this.validateOnBlur ? this.hasFocused && !this.isFocused : this.hasInput || this.hasFocused;
    },

    validations() {
      return this.validationTarget.slice(0, Number(this.errorCount));
    },

    validationState() {
      if (this.disabled) return undefined;
      if (this.hasError && this.shouldValidate) return 'error';
      if (this.hasSuccess) return 'success';
      if (this.hasColor) return this.computedColor;
      return undefined;
    },

    validationTarget() {
      if (this.internalErrorMessages.length > 0) {
        return this.internalErrorMessages;
      } else if (this.successMessages.length > 0) {
        return this.internalSuccessMessages;
      } else if (this.messages.length > 0) {
        return this.internalMessages;
      } else if (this.shouldValidate) {
        return this.errorBucket;
      } else return [];
    }

  },
  watch: {
    rules: {
      handler(newVal, oldVal) {
        if (deepEqual(newVal, oldVal)) return;
        this.validate();
      },

      deep: true
    },

    internalValue() {
      // If it's the first time we're setting input,
      // mark it with hasInput
      this.hasInput = true;
      this.validateOnBlur || this.$nextTick(this.validate);
    },

    isFocused(val) {
      // Should not check validation
      // if disabled
      if (!val && !this.disabled) {
        this.hasFocused = true;
        this.validateOnBlur && this.$nextTick(this.validate);
      }
    },

    isResetting() {
      setTimeout(() => {
        this.hasInput = false;
        this.hasFocused = false;
        this.isResetting = false;
        this.validate();
      }, 0);
    },

    hasError(val) {
      if (this.shouldValidate) {
        this.$emit('update:error', val);
      }
    },

    value(val) {
      this.lazyValue = val;
    }

  },

  beforeMount() {
    this.validate();
  },

  created() {
    this.form && this.form.register(this);
  },

  beforeDestroy() {
    this.form && this.form.unregister(this);
  },

  methods: {
    genInternalMessages(messages) {
      if (!messages) return [];else if (Array.isArray(messages)) return messages;else return [messages];
    },

    /** @public */
    reset() {
      this.isResetting = true;
      this.internalValue = Array.isArray(this.internalValue) ? [] : undefined;
    },

    /** @public */
    resetValidation() {
      this.isResetting = true;
    },

    /** @public */
    validate(force = false, value) {
      const errorBucket = [];
      value = value || this.internalValue;
      if (force) this.hasInput = this.hasFocused = true;

      for (let index = 0; index < this.rules.length; index++) {
        const rule = this.rules[index];
        const valid = typeof rule === 'function' ? rule(value) : rule;

        if (valid === false || typeof valid === 'string') {
          errorBucket.push(valid || '');
        } else if (typeof valid !== 'boolean') {
          consoleError(`Rules should return a string or boolean, received '${typeof valid}' instead`, this);
        }
      }

      this.errorBucket = errorBucket;
      this.valid = errorBucket.length === 0;
      return this.valid;
    }

  }
});

// Styles
const baseMixins$5 = mixins(BindsAttrs, Validatable);
/* @vue/component */

var VInput = baseMixins$5.extend().extend({
  name: 'v-input',
  inheritAttrs: false,
  props: {
    appendIcon: String,
    backgroundColor: {
      type: String,
      default: ''
    },
    dense: Boolean,
    height: [Number, String],
    hideDetails: [Boolean, String],
    hint: String,
    id: String,
    label: String,
    loading: Boolean,
    persistentHint: Boolean,
    prependIcon: String,
    value: null
  },

  data() {
    return {
      lazyValue: this.value,
      hasMouseDown: false
    };
  },

  computed: {
    classes() {
      return {
        'v-input--has-state': this.hasState,
        'v-input--hide-details': !this.showDetails,
        'v-input--is-label-active': this.isLabelActive,
        'v-input--is-dirty': this.isDirty,
        'v-input--is-disabled': this.disabled,
        'v-input--is-focused': this.isFocused,
        // <v-switch loading>.loading === '' so we can't just cast to boolean
        'v-input--is-loading': this.loading !== false && this.loading != null,
        'v-input--is-readonly': this.readonly,
        'v-input--dense': this.dense,
        ...this.themeClasses
      };
    },

    computedId() {
      return this.id || `input-${this._uid}`;
    },

    hasDetails() {
      return this.messagesToDisplay.length > 0;
    },

    hasHint() {
      return !this.hasMessages && !!this.hint && (this.persistentHint || this.isFocused);
    },

    hasLabel() {
      return !!(this.$slots.label || this.label);
    },

    // Proxy for `lazyValue`
    // This allows an input
    // to function without
    // a provided model
    internalValue: {
      get() {
        return this.lazyValue;
      },

      set(val) {
        this.lazyValue = val;
        this.$emit(this.$_modelEvent, val);
      }

    },

    isDirty() {
      return !!this.lazyValue;
    },

    isDisabled() {
      return this.disabled || this.readonly;
    },

    isLabelActive() {
      return this.isDirty;
    },

    messagesToDisplay() {
      if (this.hasHint) return [this.hint];
      if (!this.hasMessages) return [];
      return this.validations.map(validation => {
        if (typeof validation === 'string') return validation;
        const validationResult = validation(this.internalValue);
        return typeof validationResult === 'string' ? validationResult : '';
      }).filter(message => message !== '');
    },

    showDetails() {
      return this.hideDetails === false || this.hideDetails === 'auto' && this.hasDetails;
    }

  },
  watch: {
    value(val) {
      this.lazyValue = val;
    }

  },

  beforeCreate() {
    // v-radio-group needs to emit a different event
    // https://github.com/vuetifyjs/vuetify/issues/4752
    this.$_modelEvent = this.$options.model && this.$options.model.event || 'input';
  },

  methods: {
    genContent() {
      return [this.genPrependSlot(), this.genControl(), this.genAppendSlot()];
    },

    genControl() {
      return this.$createElement('div', {
        staticClass: 'v-input__control'
      }, [this.genInputSlot(), this.genMessages()]);
    },

    genDefaultSlot() {
      return [this.genLabel(), this.$slots.default];
    },

    genIcon(type, cb, extraData = {}) {
      const icon = this[`${type}Icon`];
      const eventName = `click:${kebabCase(type)}`;
      const hasListener = !!(this.listeners$[eventName] || cb);
      const data = mergeData({
        attrs: {
          'aria-label': hasListener ? kebabCase(type).split('-')[0] + ' icon' : undefined,
          color: this.validationState,
          dark: this.dark,
          disabled: this.disabled,
          light: this.light
        },
        on: !hasListener ? undefined : {
          click: e => {
            e.preventDefault();
            e.stopPropagation();
            this.$emit(eventName, e);
            cb && cb(e);
          },
          // Container has g event that will
          // trigger menu open if enclosed
          mouseup: e => {
            e.preventDefault();
            e.stopPropagation();
          }
        }
      }, extraData);
      return this.$createElement('div', {
        staticClass: `v-input__icon`,
        class: type ? `v-input__icon--${kebabCase(type)}` : undefined
      }, [this.$createElement(VIcon$1, data, icon)]);
    },

    genInputSlot() {
      return this.$createElement('div', this.setBackgroundColor(this.backgroundColor, {
        staticClass: 'v-input__slot',
        style: {
          height: convertToUnit(this.height)
        },
        on: {
          click: this.onClick,
          mousedown: this.onMouseDown,
          mouseup: this.onMouseUp
        },
        ref: 'input-slot'
      }), [this.genDefaultSlot()]);
    },

    genLabel() {
      if (!this.hasLabel) return null;
      return this.$createElement(VLabel, {
        props: {
          color: this.validationState,
          dark: this.dark,
          disabled: this.disabled,
          focused: this.hasState,
          for: this.computedId,
          light: this.light
        }
      }, this.$slots.label || this.label);
    },

    genMessages() {
      if (!this.showDetails) return null;
      return this.$createElement(VMessages, {
        props: {
          color: this.hasHint ? '' : this.validationState,
          dark: this.dark,
          light: this.light,
          value: this.messagesToDisplay
        },
        attrs: {
          role: this.hasMessages ? 'alert' : null
        },
        scopedSlots: {
          default: props => getSlot(this, 'message', props)
        }
      });
    },

    genSlot(type, location, slot) {
      if (!slot.length) return null;
      const ref = `${type}-${location}`;
      return this.$createElement('div', {
        staticClass: `v-input__${ref}`,
        ref
      }, slot);
    },

    genPrependSlot() {
      const slot = [];

      if (this.$slots.prepend) {
        slot.push(this.$slots.prepend);
      } else if (this.prependIcon) {
        slot.push(this.genIcon('prepend'));
      }

      return this.genSlot('prepend', 'outer', slot);
    },

    genAppendSlot() {
      const slot = []; // Append icon for text field was really
      // an appended inner icon, v-text-field
      // will overwrite this method in order to obtain
      // backwards compat

      if (this.$slots.append) {
        slot.push(this.$slots.append);
      } else if (this.appendIcon) {
        slot.push(this.genIcon('append'));
      }

      return this.genSlot('append', 'outer', slot);
    },

    onClick(e) {
      this.$emit('click', e);
    },

    onMouseDown(e) {
      this.hasMouseDown = true;
      this.$emit('mousedown', e);
    },

    onMouseUp(e) {
      this.hasMouseDown = false;
      this.$emit('mouseup', e);
    }

  },

  render(h) {
    return h('div', this.setTextColor(this.validationState, {
      staticClass: 'v-input',
      class: this.classes
    }), this.genContent());
  }

});

// Styles
/* @vue/component */

var VCounter = mixins(Themeable).extend({
  name: 'v-counter',
  functional: true,
  props: {
    value: {
      type: [Number, String],
      default: ''
    },
    max: [Number, String]
  },

  render(h, ctx) {
    const {
      props
    } = ctx;
    const max = parseInt(props.max, 10);
    const value = parseInt(props.value, 10);
    const content = max ? `${value} / ${max}` : String(props.value);
    const isGreater = max && value > max;
    return h('div', {
      staticClass: 'v-counter',
      class: {
        'error--text': isGreater,
        ...functionalThemeClasses(ctx)
      }
    }, content);
  }

});

// Directives
function intersectable(options) {
  if (typeof window === 'undefined' || !('IntersectionObserver' in window)) {
    // do nothing because intersection observer is not available
    return Vue.extend({
      name: 'intersectable'
    });
  }

  return Vue.extend({
    name: 'intersectable',

    mounted() {
      Intersect.inserted(this.$el, {
        name: 'intersect',
        value: {
          handler: this.onObserve
        }
      });
    },

    destroyed() {
      Intersect.unbind(this.$el);
    },

    methods: {
      onObserve(entries, observer, isIntersecting) {
        if (!isIntersecting) return;

        for (let i = 0, length = options.onVisible.length; i < length; i++) {
          const callback = this[options.onVisible[i]];

          if (typeof callback === 'function') {
            callback();
            continue;
          }

          consoleWarn(options.onVisible[i] + ' method is not available on the instance but referenced in intersectable mixin options');
        }
      }

    }
  });
}

const baseMixins$6 = mixins(Colorable, factory(['absolute', 'fixed', 'top', 'bottom']), Proxyable, Themeable);
/* @vue/component */

var VProgressLinear = baseMixins$6.extend({
  name: 'v-progress-linear',
  props: {
    active: {
      type: Boolean,
      default: true
    },
    backgroundColor: {
      type: String,
      default: null
    },
    backgroundOpacity: {
      type: [Number, String],
      default: null
    },
    bufferValue: {
      type: [Number, String],
      default: 100
    },
    color: {
      type: String,
      default: 'primary'
    },
    height: {
      type: [Number, String],
      default: 4
    },
    indeterminate: Boolean,
    query: Boolean,
    rounded: Boolean,
    stream: Boolean,
    striped: Boolean,
    value: {
      type: [Number, String],
      default: 0
    }
  },

  data() {
    return {
      internalLazyValue: this.value || 0
    };
  },

  computed: {
    __cachedBackground() {
      return this.$createElement('div', this.setBackgroundColor(this.backgroundColor || this.color, {
        staticClass: 'v-progress-linear__background',
        style: this.backgroundStyle
      }));
    },

    __cachedBar() {
      return this.$createElement(this.computedTransition, [this.__cachedBarType]);
    },

    __cachedBarType() {
      return this.indeterminate ? this.__cachedIndeterminate : this.__cachedDeterminate;
    },

    __cachedBuffer() {
      return this.$createElement('div', {
        staticClass: 'v-progress-linear__buffer',
        style: this.styles
      });
    },

    __cachedDeterminate() {
      return this.$createElement('div', this.setBackgroundColor(this.color, {
        staticClass: `v-progress-linear__determinate`,
        style: {
          width: convertToUnit(this.normalizedValue, '%')
        }
      }));
    },

    __cachedIndeterminate() {
      return this.$createElement('div', {
        staticClass: 'v-progress-linear__indeterminate',
        class: {
          'v-progress-linear__indeterminate--active': this.active
        }
      }, [this.genProgressBar('long'), this.genProgressBar('short')]);
    },

    __cachedStream() {
      if (!this.stream) return null;
      return this.$createElement('div', this.setTextColor(this.color, {
        staticClass: 'v-progress-linear__stream',
        style: {
          width: convertToUnit(100 - this.normalizedBuffer, '%')
        }
      }));
    },

    backgroundStyle() {
      const backgroundOpacity = this.backgroundOpacity == null ? this.backgroundColor ? 1 : 0.3 : parseFloat(this.backgroundOpacity);
      return {
        opacity: backgroundOpacity,
        [this.$vuetify.rtl ? 'right' : 'left']: convertToUnit(this.normalizedValue, '%'),
        width: convertToUnit(this.normalizedBuffer - this.normalizedValue, '%')
      };
    },

    classes() {
      return {
        'v-progress-linear--absolute': this.absolute,
        'v-progress-linear--fixed': this.fixed,
        'v-progress-linear--query': this.query,
        'v-progress-linear--reactive': this.reactive,
        'v-progress-linear--rounded': this.rounded,
        'v-progress-linear--striped': this.striped,
        ...this.themeClasses
      };
    },

    computedTransition() {
      return this.indeterminate ? VFadeTransition : VSlideXTransition;
    },

    normalizedBuffer() {
      return this.normalize(this.bufferValue);
    },

    normalizedValue() {
      return this.normalize(this.internalLazyValue);
    },

    reactive() {
      return Boolean(this.$listeners.change);
    },

    styles() {
      const styles = {};

      if (!this.active) {
        styles.height = 0;
      }

      if (!this.indeterminate && parseFloat(this.normalizedBuffer) !== 100) {
        styles.width = convertToUnit(this.normalizedBuffer, '%');
      }

      return styles;
    }

  },
  methods: {
    genContent() {
      const slot = getSlot(this, 'default', {
        value: this.internalLazyValue
      });
      if (!slot) return null;
      return this.$createElement('div', {
        staticClass: 'v-progress-linear__content'
      }, slot);
    },

    genListeners() {
      const listeners = this.$listeners;

      if (this.reactive) {
        listeners.click = this.onClick;
      }

      return listeners;
    },

    genProgressBar(name) {
      return this.$createElement('div', this.setBackgroundColor(this.color, {
        staticClass: 'v-progress-linear__indeterminate',
        class: {
          [name]: true
        }
      }));
    },

    onClick(e) {
      if (!this.reactive) return;
      const {
        width
      } = this.$el.getBoundingClientRect();
      this.internalValue = e.offsetX / width * 100;
    },

    normalize(value) {
      if (value < 0) return 0;
      if (value > 100) return 100;
      return parseFloat(value);
    }

  },

  render(h) {
    const data = {
      staticClass: 'v-progress-linear',
      attrs: {
        role: 'progressbar',
        'aria-valuemin': 0,
        'aria-valuemax': this.normalizedBuffer,
        'aria-valuenow': this.indeterminate ? undefined : this.normalizedValue
      },
      class: this.classes,
      style: {
        bottom: this.bottom ? 0 : undefined,
        height: this.active ? convertToUnit(this.height) : 0,
        top: this.top ? 0 : undefined
      },
      on: this.genListeners()
    };
    return h('div', data, [this.__cachedStream, this.__cachedBackground, this.__cachedBuffer, this.__cachedBar, this.genContent()]);
  }

});

/**
 * Loadable
 *
 * @mixin
 *
 * Used to add linear progress bar to components
 * Can use a default bar with a specific color
 * or designate a custom progress linear bar
 */

/* @vue/component */

var Loadable = Vue.extend().extend({
  name: 'loadable',
  props: {
    loading: {
      type: [Boolean, String],
      default: false
    },
    loaderHeight: {
      type: [Number, String],
      default: 2
    }
  },
  methods: {
    genProgress() {
      if (this.loading === false) return null;
      return this.$slots.progress || this.$createElement(VProgressLinear, {
        props: {
          absolute: true,
          color: this.loading === true || this.loading === '' ? this.color || 'primary' : this.loading,
          height: this.loaderHeight,
          indeterminate: true
        }
      });
    }

  }
});

// Styles
const baseMixins$7 = mixins(VInput, intersectable({
  onVisible: ['setLabelWidth', 'setPrefixWidth', 'setPrependWidth', 'tryAutofocus']
}), Loadable);
const dirtyTypes = ['color', 'file', 'time', 'date', 'datetime-local', 'week', 'month'];
/* @vue/component */

var VTextField = baseMixins$7.extend().extend({
  name: 'v-text-field',
  directives: {
    ripple: Ripple
  },
  inheritAttrs: false,
  props: {
    appendOuterIcon: String,
    autofocus: Boolean,
    clearable: Boolean,
    clearIcon: {
      type: String,
      default: '$clear'
    },
    counter: [Boolean, Number, String],
    counterValue: Function,
    filled: Boolean,
    flat: Boolean,
    fullWidth: Boolean,
    label: String,
    outlined: Boolean,
    placeholder: String,
    prefix: String,
    prependInnerIcon: String,
    reverse: Boolean,
    rounded: Boolean,
    shaped: Boolean,
    singleLine: Boolean,
    solo: Boolean,
    soloInverted: Boolean,
    suffix: String,
    type: {
      type: String,
      default: 'text'
    }
  },
  data: () => ({
    badInput: false,
    labelWidth: 0,
    prefixWidth: 0,
    prependWidth: 0,
    initialValue: null,
    isBooted: false,
    isClearing: false
  }),
  computed: {
    classes() {
      return { ...VInput.options.computed.classes.call(this),
        'v-text-field': true,
        'v-text-field--full-width': this.fullWidth,
        'v-text-field--prefix': this.prefix,
        'v-text-field--single-line': this.isSingle,
        'v-text-field--solo': this.isSolo,
        'v-text-field--solo-inverted': this.soloInverted,
        'v-text-field--solo-flat': this.flat,
        'v-text-field--filled': this.filled,
        'v-text-field--is-booted': this.isBooted,
        'v-text-field--enclosed': this.isEnclosed,
        'v-text-field--reverse': this.reverse,
        'v-text-field--outlined': this.outlined,
        'v-text-field--placeholder': this.placeholder,
        'v-text-field--rounded': this.rounded,
        'v-text-field--shaped': this.shaped
      };
    },

    computedColor() {
      const computedColor = Validatable.options.computed.computedColor.call(this);
      if (!this.soloInverted || !this.isFocused) return computedColor;
      return this.color || 'primary';
    },

    computedCounterValue() {
      if (typeof this.counterValue === 'function') {
        return this.counterValue(this.internalValue);
      }

      return (this.internalValue || '').toString().length;
    },

    hasCounter() {
      return this.counter !== false && this.counter != null;
    },

    hasDetails() {
      return VInput.options.computed.hasDetails.call(this) || this.hasCounter;
    },

    internalValue: {
      get() {
        return this.lazyValue;
      },

      set(val) {
        this.lazyValue = val;
        this.$emit('input', this.lazyValue);
      }

    },

    isDirty() {
      return this.lazyValue != null && this.lazyValue.toString().length > 0 || this.badInput;
    },

    isEnclosed() {
      return this.filled || this.isSolo || this.outlined;
    },

    isLabelActive() {
      return this.isDirty || dirtyTypes.includes(this.type);
    },

    isSingle() {
      return this.isSolo || this.singleLine || this.fullWidth || // https://material.io/components/text-fields/#filled-text-field
      this.filled && !this.hasLabel;
    },

    isSolo() {
      return this.solo || this.soloInverted;
    },

    labelPosition() {
      let offset = this.prefix && !this.labelValue ? this.prefixWidth : 0;
      if (this.labelValue && this.prependWidth) offset -= this.prependWidth;
      return this.$vuetify.rtl === this.reverse ? {
        left: offset,
        right: 'auto'
      } : {
        left: 'auto',
        right: offset
      };
    },

    showLabel() {
      return this.hasLabel && (!this.isSingle || !this.isLabelActive && !this.placeholder);
    },

    labelValue() {
      return !this.isSingle && Boolean(this.isFocused || this.isLabelActive || this.placeholder);
    }

  },
  watch: {
    labelValue: 'setLabelWidth',
    outlined: 'setLabelWidth',

    label() {
      this.$nextTick(this.setLabelWidth);
    },

    prefix() {
      this.$nextTick(this.setPrefixWidth);
    },

    isFocused: 'updateValue',

    value(val) {
      this.lazyValue = val;
    }

  },

  created() {
    /* istanbul ignore next */
    if (this.$attrs.hasOwnProperty('box')) {
      breaking('box', 'filled', this);
    }
    /* istanbul ignore next */


    if (this.$attrs.hasOwnProperty('browser-autocomplete')) {
      breaking('browser-autocomplete', 'autocomplete', this);
    }
    /* istanbul ignore if */


    if (this.shaped && !(this.filled || this.outlined || this.isSolo)) {
      consoleWarn('shaped should be used with either filled or outlined', this);
    }
  },

  mounted() {
    this.autofocus && this.tryAutofocus();
    this.setLabelWidth();
    this.setPrefixWidth();
    this.setPrependWidth();
    requestAnimationFrame(() => this.isBooted = true);
  },

  methods: {
    /** @public */
    focus() {
      this.onFocus();
    },

    /** @public */
    blur(e) {
      // https://github.com/vuetifyjs/vuetify/issues/5913
      // Safari tab order gets broken if called synchronous
      window.requestAnimationFrame(() => {
        this.$refs.input && this.$refs.input.blur();
      });
    },

    clearableCallback() {
      this.$refs.input && this.$refs.input.focus();
      this.$nextTick(() => this.internalValue = null);
    },

    genAppendSlot() {
      const slot = [];

      if (this.$slots['append-outer']) {
        slot.push(this.$slots['append-outer']);
      } else if (this.appendOuterIcon) {
        slot.push(this.genIcon('appendOuter'));
      }

      return this.genSlot('append', 'outer', slot);
    },

    genPrependInnerSlot() {
      const slot = [];

      if (this.$slots['prepend-inner']) {
        slot.push(this.$slots['prepend-inner']);
      } else if (this.prependInnerIcon) {
        slot.push(this.genIcon('prependInner'));
      }

      return this.genSlot('prepend', 'inner', slot);
    },

    genIconSlot() {
      const slot = [];

      if (this.$slots['append']) {
        slot.push(this.$slots['append']);
      } else if (this.appendIcon) {
        slot.push(this.genIcon('append'));
      }

      return this.genSlot('append', 'inner', slot);
    },

    genInputSlot() {
      const input = VInput.options.methods.genInputSlot.call(this);
      const prepend = this.genPrependInnerSlot();

      if (prepend) {
        input.children = input.children || [];
        input.children.unshift(prepend);
      }

      return input;
    },

    genClearIcon() {
      if (!this.clearable) return null;
      const data = this.isDirty ? undefined : {
        attrs: {
          disabled: true
        }
      };
      return this.genSlot('append', 'inner', [this.genIcon('clear', this.clearableCallback, data)]);
    },

    genCounter() {
      if (!this.hasCounter) return null;
      const max = this.counter === true ? this.attrs$.maxlength : this.counter;
      return this.$createElement(VCounter, {
        props: {
          dark: this.dark,
          light: this.light,
          max,
          value: this.computedCounterValue
        }
      });
    },

    genDefaultSlot() {
      return [this.genFieldset(), this.genTextFieldSlot(), this.genClearIcon(), this.genIconSlot(), this.genProgress()];
    },

    genFieldset() {
      if (!this.outlined) return null;
      return this.$createElement('fieldset', {
        attrs: {
          'aria-hidden': true
        }
      }, [this.genLegend()]);
    },

    genLabel() {
      if (!this.showLabel) return null;
      const data = {
        props: {
          absolute: true,
          color: this.validationState,
          dark: this.dark,
          disabled: this.disabled,
          focused: !this.isSingle && (this.isFocused || !!this.validationState),
          for: this.computedId,
          left: this.labelPosition.left,
          light: this.light,
          right: this.labelPosition.right,
          value: this.labelValue
        }
      };
      return this.$createElement(VLabel, data, this.$slots.label || this.label);
    },

    genLegend() {
      const width = !this.singleLine && (this.labelValue || this.isDirty) ? this.labelWidth : 0;
      const span = this.$createElement('span', {
        domProps: {
          innerHTML: '&#8203;'
        }
      });
      return this.$createElement('legend', {
        style: {
          width: !this.isSingle ? convertToUnit(width) : undefined
        }
      }, [span]);
    },

    genInput() {
      const listeners = Object.assign({}, this.listeners$);
      delete listeners['change']; // Change should not be bound externally

      return this.$createElement('input', {
        style: {},
        domProps: {
          value: this.lazyValue
        },
        attrs: { ...this.attrs$,
          autofocus: this.autofocus,
          disabled: this.disabled,
          id: this.computedId,
          placeholder: this.placeholder,
          readonly: this.readonly,
          type: this.type
        },
        on: Object.assign(listeners, {
          blur: this.onBlur,
          input: this.onInput,
          focus: this.onFocus,
          keydown: this.onKeyDown
        }),
        ref: 'input'
      });
    },

    genMessages() {
      if (!this.showDetails) return null;
      const messagesNode = VInput.options.methods.genMessages.call(this);
      const counterNode = this.genCounter();
      return this.$createElement('div', {
        staticClass: 'v-text-field__details'
      }, [messagesNode, counterNode]);
    },

    genTextFieldSlot() {
      return this.$createElement('div', {
        staticClass: 'v-text-field__slot'
      }, [this.genLabel(), this.prefix ? this.genAffix('prefix') : null, this.genInput(), this.suffix ? this.genAffix('suffix') : null]);
    },

    genAffix(type) {
      return this.$createElement('div', {
        class: `v-text-field__${type}`,
        ref: type
      }, this[type]);
    },

    onBlur(e) {
      this.isFocused = false;
      e && this.$nextTick(() => this.$emit('blur', e));
    },

    onClick() {
      if (this.isFocused || this.disabled || !this.$refs.input) return;
      this.$refs.input.focus();
    },

    onFocus(e) {
      if (!this.$refs.input) return;

      if (document.activeElement !== this.$refs.input) {
        return this.$refs.input.focus();
      }

      if (!this.isFocused) {
        this.isFocused = true;
        e && this.$emit('focus', e);
      }
    },

    onInput(e) {
      const target = e.target;
      this.internalValue = target.value;
      this.badInput = target.validity && target.validity.badInput;
    },

    onKeyDown(e) {
      if (e.keyCode === keyCodes.enter) this.$emit('change', this.internalValue);
      this.$emit('keydown', e);
    },

    onMouseDown(e) {
      // Prevent input from being blurred
      if (e.target !== this.$refs.input) {
        e.preventDefault();
        e.stopPropagation();
      }

      VInput.options.methods.onMouseDown.call(this, e);
    },

    onMouseUp(e) {
      if (this.hasMouseDown) this.focus();
      VInput.options.methods.onMouseUp.call(this, e);
    },

    setLabelWidth() {
      if (!this.outlined) return;
      this.labelWidth = this.$refs.label ? Math.min(this.$refs.label.scrollWidth * 0.75 + 6, this.$el.offsetWidth - 24) : 0;
    },

    setPrefixWidth() {
      if (!this.$refs.prefix) return;
      this.prefixWidth = this.$refs.prefix.offsetWidth;
    },

    setPrependWidth() {
      if (!this.outlined || !this.$refs['prepend-inner']) return;
      this.prependWidth = this.$refs['prepend-inner'].offsetWidth;
    },

    tryAutofocus() {
      if (!this.autofocus || typeof document === 'undefined' || !this.$refs.input || document.activeElement === this.$refs.input) return false;
      this.$refs.input.focus();
      return true;
    },

    updateValue(val) {
      // Sets validationState from validatable
      this.hasColor = val;

      if (val) {
        this.initialValue = this.lazyValue;
      } else if (this.initialValue !== this.lazyValue) {
        this.$emit('change', this.lazyValue);
      }
    }

  }
});

var Comparable = Vue.extend({
  name: 'comparable',
  props: {
    valueComparator: {
      type: Function,
      default: deepEqual
    }
  }
});

/* @vue/component */

var Filterable = Vue.extend({
  name: 'filterable',
  props: {
    noDataText: {
      type: String,
      default: '$vuetify.noDataText'
    }
  }
});

// Styles
const defaultMenuProps = {
  closeOnClick: false,
  closeOnContentClick: false,
  disableKeys: true,
  openOnClick: false,
  maxHeight: 304
}; // Types

const baseMixins$8 = mixins(VTextField, Comparable, Filterable);
/* @vue/component */

var VSelect = baseMixins$8.extend().extend({
  name: 'v-select',
  directives: {
    ClickOutside
  },
  props: {
    appendIcon: {
      type: String,
      default: '$dropdown'
    },
    attach: {
      type: null,
      default: false
    },
    cacheItems: Boolean,
    chips: Boolean,
    clearable: Boolean,
    deletableChips: Boolean,
    disableLookup: Boolean,
    eager: Boolean,
    hideSelected: Boolean,
    items: {
      type: Array,
      default: () => []
    },
    itemColor: {
      type: String,
      default: 'primary'
    },
    itemDisabled: {
      type: [String, Array, Function],
      default: 'disabled'
    },
    itemText: {
      type: [String, Array, Function],
      default: 'text'
    },
    itemValue: {
      type: [String, Array, Function],
      default: 'value'
    },
    menuProps: {
      type: [String, Array, Object],
      default: () => defaultMenuProps
    },
    multiple: Boolean,
    openOnClear: Boolean,
    returnObject: Boolean,
    smallChips: Boolean
  },

  data() {
    return {
      cachedItems: this.cacheItems ? this.items : [],
      menuIsBooted: false,
      isMenuActive: false,
      lastItem: 20,
      // As long as a value is defined, show it
      // Otherwise, check if multiple
      // to determine which default to provide
      lazyValue: this.value !== undefined ? this.value : this.multiple ? [] : undefined,
      selectedIndex: -1,
      selectedItems: [],
      keyboardLookupPrefix: '',
      keyboardLookupLastTime: 0
    };
  },

  computed: {
    /* All items that the select has */
    allItems() {
      return this.filterDuplicates(this.cachedItems.concat(this.items));
    },

    classes() {
      return { ...VTextField.options.computed.classes.call(this),
        'v-select': true,
        'v-select--chips': this.hasChips,
        'v-select--chips--small': this.smallChips,
        'v-select--is-menu-active': this.isMenuActive,
        'v-select--is-multi': this.multiple
      };
    },

    /* Used by other components to overwrite */
    computedItems() {
      return this.allItems;
    },

    computedOwns() {
      return `list-${this._uid}`;
    },

    computedCounterValue() {
      return this.multiple ? this.selectedItems.length : (this.getText(this.selectedItems[0]) || '').toString().length;
    },

    directives() {
      return this.isFocused ? [{
        name: 'click-outside',
        value: this.blur,
        args: {
          closeConditional: this.closeConditional
        }
      }] : undefined;
    },

    dynamicHeight() {
      return 'auto';
    },

    hasChips() {
      return this.chips || this.smallChips;
    },

    hasSlot() {
      return Boolean(this.hasChips || this.$scopedSlots.selection);
    },

    isDirty() {
      return this.selectedItems.length > 0;
    },

    listData() {
      const scopeId = this.$vnode && this.$vnode.context.$options._scopeId;
      const attrs = scopeId ? {
        [scopeId]: true
      } : {};
      return {
        attrs: { ...attrs,
          id: this.computedOwns
        },
        props: {
          action: this.multiple,
          color: this.itemColor,
          dense: this.dense,
          hideSelected: this.hideSelected,
          items: this.virtualizedItems,
          itemDisabled: this.itemDisabled,
          itemText: this.itemText,
          itemValue: this.itemValue,
          noDataText: this.$vuetify.lang.t(this.noDataText),
          selectedItems: this.selectedItems
        },
        on: {
          select: this.selectItem
        },
        scopedSlots: {
          item: this.$scopedSlots.item
        }
      };
    },

    staticList() {
      if (this.$slots['no-data'] || this.$slots['prepend-item'] || this.$slots['append-item']) {
        consoleError('assert: staticList should not be called if slots are used');
      }

      return this.$createElement(VSelectList, this.listData);
    },

    virtualizedItems() {
      return this.$_menuProps.auto ? this.computedItems : this.computedItems.slice(0, this.lastItem);
    },

    menuCanShow: () => true,

    $_menuProps() {
      let normalisedProps = typeof this.menuProps === 'string' ? this.menuProps.split(',') : this.menuProps;

      if (Array.isArray(normalisedProps)) {
        normalisedProps = normalisedProps.reduce((acc, p) => {
          acc[p.trim()] = true;
          return acc;
        }, {});
      }

      return { ...defaultMenuProps,
        eager: this.eager,
        value: this.menuCanShow && this.isMenuActive,
        nudgeBottom: normalisedProps.offsetY ? 1 : 0,
        ...normalisedProps
      };
    }

  },
  watch: {
    internalValue(val) {
      this.initialValue = val;
      this.setSelectedItems();
    },

    menuIsBooted() {
      window.setTimeout(() => {
        if (this.getContent() && this.getContent().addEventListener) {
          this.getContent().addEventListener('scroll', this.onScroll, false);
        }
      });
    },

    isMenuActive(val) {
      window.setTimeout(() => this.onMenuActiveChange(val));
      if (!val) return;
      this.menuIsBooted = true;
    },

    items: {
      immediate: true,

      handler(val) {
        if (this.cacheItems) {
          // Breaks vue-test-utils if
          // this isn't calculated
          // on the next tick
          this.$nextTick(() => {
            this.cachedItems = this.filterDuplicates(this.cachedItems.concat(val));
          });
        }

        this.setSelectedItems();
      }

    }
  },
  methods: {
    /** @public */
    blur(e) {
      VTextField.options.methods.blur.call(this, e);
      this.isMenuActive = false;
      this.isFocused = false;
      this.selectedIndex = -1;
    },

    /** @public */
    activateMenu() {
      if (this.disabled || this.readonly || this.isMenuActive) return;
      this.isMenuActive = true;
    },

    clearableCallback() {
      this.setValue(this.multiple ? [] : undefined);
      this.setMenuIndex(-1);
      this.$nextTick(() => this.$refs.input && this.$refs.input.focus());
      if (this.openOnClear) this.isMenuActive = true;
    },

    closeConditional(e) {
      if (!this.isMenuActive) return true;
      return !this._isDestroyed && ( // Click originates from outside the menu content
      // Multiple selects don't close when an item is clicked
      !this.getContent() || !this.getContent().contains(e.target)) && // Click originates from outside the element
      this.$el && !this.$el.contains(e.target) && e.target !== this.$el;
    },

    filterDuplicates(arr) {
      const uniqueValues = new Map();

      for (let index = 0; index < arr.length; ++index) {
        const item = arr[index];
        const val = this.getValue(item); // TODO: comparator

        !uniqueValues.has(val) && uniqueValues.set(val, item);
      }

      return Array.from(uniqueValues.values());
    },

    findExistingIndex(item) {
      const itemValue = this.getValue(item);
      return (this.internalValue || []).findIndex(i => this.valueComparator(this.getValue(i), itemValue));
    },

    getContent() {
      return this.$refs.menu && this.$refs.menu.$refs.content;
    },

    genChipSelection(item, index) {
      const isDisabled = this.disabled || this.readonly || this.getDisabled(item);
      return this.$createElement(VChip, {
        staticClass: 'v-chip--select',
        attrs: {
          tabindex: -1
        },
        props: {
          close: this.deletableChips && !isDisabled,
          disabled: isDisabled,
          inputValue: index === this.selectedIndex,
          small: this.smallChips
        },
        on: {
          click: e => {
            if (isDisabled) return;
            e.stopPropagation();
            this.selectedIndex = index;
          },
          'click:close': () => this.onChipInput(item)
        },
        key: JSON.stringify(this.getValue(item))
      }, this.getText(item));
    },

    genCommaSelection(item, index, last) {
      const color = index === this.selectedIndex && this.computedColor;
      const isDisabled = this.disabled || this.getDisabled(item);
      return this.$createElement('div', this.setTextColor(color, {
        staticClass: 'v-select__selection v-select__selection--comma',
        class: {
          'v-select__selection--disabled': isDisabled
        },
        key: JSON.stringify(this.getValue(item))
      }), `${this.getText(item)}${last ? '' : ', '}`);
    },

    genDefaultSlot() {
      const selections = this.genSelections();
      const input = this.genInput(); // If the return is an empty array
      // push the input

      if (Array.isArray(selections)) {
        selections.push(input); // Otherwise push it into children
      } else {
        selections.children = selections.children || [];
        selections.children.push(input);
      }

      return [this.genFieldset(), this.$createElement('div', {
        staticClass: 'v-select__slot',
        directives: this.directives
      }, [this.genLabel(), this.prefix ? this.genAffix('prefix') : null, selections, this.suffix ? this.genAffix('suffix') : null, this.genClearIcon(), this.genIconSlot(), this.genHiddenInput()]), this.genMenu(), this.genProgress()];
    },

    genIcon(type, cb, extraData) {
      const icon = VInput.options.methods.genIcon.call(this, type, cb, extraData);

      if (type === 'append') {
        // Don't allow the dropdown icon to be focused
        icon.children[0].data = mergeData(icon.children[0].data, {
          attrs: {
            tabindex: icon.children[0].componentOptions.listeners && '-1',
            'aria-hidden': 'true',
            'aria-label': undefined
          }
        });
      }

      return icon;
    },

    genInput() {
      const input = VTextField.options.methods.genInput.call(this);
      delete input.data.attrs.name;
      input.data = mergeData(input.data, {
        domProps: {
          value: null
        },
        attrs: {
          readonly: true,
          type: 'text',
          'aria-readonly': String(this.readonly),
          'aria-activedescendant': getObjectValueByPath(this.$refs.menu, 'activeTile.id'),
          autocomplete: getObjectValueByPath(input.data, 'attrs.autocomplete', 'off')
        },
        on: {
          keypress: this.onKeyPress
        }
      });
      return input;
    },

    genHiddenInput() {
      return this.$createElement('input', {
        domProps: {
          value: this.lazyValue
        },
        attrs: {
          type: 'hidden',
          name: this.attrs$.name
        }
      });
    },

    genInputSlot() {
      const render = VTextField.options.methods.genInputSlot.call(this);
      render.data.attrs = { ...render.data.attrs,
        role: 'button',
        'aria-haspopup': 'listbox',
        'aria-expanded': String(this.isMenuActive),
        'aria-owns': this.computedOwns
      };
      return render;
    },

    genList() {
      // If there's no slots, we can use a cached VNode to improve performance
      if (this.$slots['no-data'] || this.$slots['prepend-item'] || this.$slots['append-item']) {
        return this.genListWithSlot();
      } else {
        return this.staticList;
      }
    },

    genListWithSlot() {
      const slots = ['prepend-item', 'no-data', 'append-item'].filter(slotName => this.$slots[slotName]).map(slotName => this.$createElement('template', {
        slot: slotName
      }, this.$slots[slotName])); // Requires destructuring due to Vue
      // modifying the `on` property when passed
      // as a referenced object

      return this.$createElement(VSelectList, { ...this.listData
      }, slots);
    },

    genMenu() {
      const props = this.$_menuProps;
      props.activator = this.$refs['input-slot']; // Attach to root el so that
      // menu covers prepend/append icons

      if ( // TODO: make this a computed property or helper or something
      this.attach === '' || // If used as a boolean prop (<v-menu attach>)
      this.attach === true || // If bound to a boolean (<v-menu :attach="true">)
      this.attach === 'attach' // If bound as boolean prop in pug (v-menu(attach))
      ) {
          props.attach = this.$el;
        } else {
        props.attach = this.attach;
      }

      return this.$createElement(VMenu, {
        attrs: {
          role: undefined,
          offsetY: true
        },
        props,
        on: {
          input: val => {
            this.isMenuActive = val;
            this.isFocused = val;
          }
        },
        ref: 'menu'
      }, [this.genList()]);
    },

    genSelections() {
      let length = this.selectedItems.length;
      const children = new Array(length);
      let genSelection;

      if (this.$scopedSlots.selection) {
        genSelection = this.genSlotSelection;
      } else if (this.hasChips) {
        genSelection = this.genChipSelection;
      } else {
        genSelection = this.genCommaSelection;
      }

      while (length--) {
        children[length] = genSelection(this.selectedItems[length], length, length === children.length - 1);
      }

      return this.$createElement('div', {
        staticClass: 'v-select__selections'
      }, children);
    },

    genSlotSelection(item, index) {
      return this.$scopedSlots.selection({
        attrs: {
          class: 'v-chip--select'
        },
        parent: this,
        item,
        index,
        select: e => {
          e.stopPropagation();
          this.selectedIndex = index;
        },
        selected: index === this.selectedIndex,
        disabled: this.disabled || this.readonly
      });
    },

    getMenuIndex() {
      return this.$refs.menu ? this.$refs.menu.listIndex : -1;
    },

    getDisabled(item) {
      return getPropertyFromItem(item, this.itemDisabled, false);
    },

    getText(item) {
      return getPropertyFromItem(item, this.itemText, item);
    },

    getValue(item) {
      return getPropertyFromItem(item, this.itemValue, this.getText(item));
    },

    onBlur(e) {
      e && this.$emit('blur', e);
    },

    onChipInput(item) {
      if (this.multiple) this.selectItem(item);else this.setValue(null); // If all items have been deleted,
      // open `v-menu`

      if (this.selectedItems.length === 0) {
        this.isMenuActive = true;
      } else {
        this.isMenuActive = false;
      }

      this.selectedIndex = -1;
    },

    onClick(e) {
      if (this.isDisabled) return;

      if (!this.isAppendInner(e.target)) {
        this.isMenuActive = true;
      }

      if (!this.isFocused) {
        this.isFocused = true;
        this.$emit('focus');
      }

      this.$emit('click', e);
    },

    onEscDown(e) {
      e.preventDefault();

      if (this.isMenuActive) {
        e.stopPropagation();
        this.isMenuActive = false;
      }
    },

    onKeyPress(e) {
      if (this.multiple || this.readonly || this.disableLookup) return;
      const KEYBOARD_LOOKUP_THRESHOLD = 1000; // milliseconds

      const now = performance.now();

      if (now - this.keyboardLookupLastTime > KEYBOARD_LOOKUP_THRESHOLD) {
        this.keyboardLookupPrefix = '';
      }

      this.keyboardLookupPrefix += e.key.toLowerCase();
      this.keyboardLookupLastTime = now;
      const index = this.allItems.findIndex(item => {
        const text = (this.getText(item) || '').toString();
        return text.toLowerCase().startsWith(this.keyboardLookupPrefix);
      });
      const item = this.allItems[index];

      if (index !== -1) {
        this.lastItem = Math.max(this.lastItem, index + 5);
        this.setValue(this.returnObject ? item : this.getValue(item));
        this.$nextTick(() => this.$refs.menu.getTiles());
        setTimeout(() => this.setMenuIndex(index));
      }
    },

    onKeyDown(e) {
      if (this.readonly) return;
      const keyCode = e.keyCode;
      const menu = this.$refs.menu; // If enter, space, open menu

      if ([keyCodes.enter, keyCodes.space].includes(keyCode)) this.activateMenu();
      this.$emit('keydown', e);
      if (!menu) return; // If menu is active, allow default
      // listIndex change from menu

      if (this.isMenuActive && keyCode !== keyCodes.tab) {
        this.$nextTick(() => {
          menu.changeListIndex(e);
          this.$emit('update:list-index', menu.listIndex);
        });
      } // If menu is not active, up and down can do
      // one of 2 things. If multiple, opens the
      // menu, if not, will cycle through all
      // available options


      if (!this.isMenuActive && [keyCodes.up, keyCodes.down].includes(keyCode)) return this.onUpDown(e); // If escape deactivate the menu

      if (keyCode === keyCodes.esc) return this.onEscDown(e); // If tab - select item or close menu

      if (keyCode === keyCodes.tab) return this.onTabDown(e); // If space preventDefault

      if (keyCode === keyCodes.space) return this.onSpaceDown(e);
    },

    onMenuActiveChange(val) {
      // If menu is closing and mulitple
      // or menuIndex is already set
      // skip menu index recalculation
      if (this.multiple && !val || this.getMenuIndex() > -1) return;
      const menu = this.$refs.menu;
      if (!menu || !this.isDirty) return; // When menu opens, set index of first active item

      for (let i = 0; i < menu.tiles.length; i++) {
        if (menu.tiles[i].getAttribute('aria-selected') === 'true') {
          this.setMenuIndex(i);
          break;
        }
      }
    },

    onMouseUp(e) {
      if (this.hasMouseDown && e.which !== 3 && !this.isDisabled) {
        // If append inner is present
        // and the target is itself
        // or inside, toggle menu
        if (this.isAppendInner(e.target)) {
          this.$nextTick(() => this.isMenuActive = !this.isMenuActive); // If user is clicking in the container
          // and field is enclosed, activate it
        } else if (this.isEnclosed) {
          this.isMenuActive = true;
        }
      }

      VTextField.options.methods.onMouseUp.call(this, e);
    },

    onScroll() {
      if (!this.isMenuActive) {
        requestAnimationFrame(() => this.getContent().scrollTop = 0);
      } else {
        if (this.lastItem >= this.computedItems.length) return;
        const showMoreItems = this.getContent().scrollHeight - (this.getContent().scrollTop + this.getContent().clientHeight) < 200;

        if (showMoreItems) {
          this.lastItem += 20;
        }
      }
    },

    onSpaceDown(e) {
      e.preventDefault();
    },

    onTabDown(e) {
      const menu = this.$refs.menu;
      if (!menu) return;
      const activeTile = menu.activeTile; // An item that is selected by
      // menu-index should toggled

      if (!this.multiple && activeTile && this.isMenuActive) {
        e.preventDefault();
        e.stopPropagation();
        activeTile.click();
      } else {
        // If we make it here,
        // the user has no selected indexes
        // and is probably tabbing out
        this.blur(e);
      }
    },

    onUpDown(e) {
      const menu = this.$refs.menu;
      if (!menu) return;
      e.preventDefault(); // Multiple selects do not cycle their value
      // when pressing up or down, instead activate
      // the menu

      if (this.multiple) return this.activateMenu();
      const keyCode = e.keyCode; // Cycle through available values to achieve
      // select native behavior

      menu.isBooted = true;
      window.requestAnimationFrame(() => {
        menu.getTiles();
        keyCodes.up === keyCode ? menu.prevTile() : menu.nextTile();
        menu.activeTile && menu.activeTile.click();
      });
    },

    selectItem(item) {
      if (!this.multiple) {
        this.setValue(this.returnObject ? item : this.getValue(item));
        this.isMenuActive = false;
      } else {
        const internalValue = (this.internalValue || []).slice();
        const i = this.findExistingIndex(item);
        i !== -1 ? internalValue.splice(i, 1) : internalValue.push(item);
        this.setValue(internalValue.map(i => {
          return this.returnObject ? i : this.getValue(i);
        })); // When selecting multiple
        // adjust menu after each
        // selection

        this.$nextTick(() => {
          this.$refs.menu && this.$refs.menu.updateDimensions();
        }); // We only need to reset list index for multiple
        // to keep highlight when an item is toggled
        // on and off

        if (!this.multiple) return;
        const listIndex = this.getMenuIndex();
        this.setMenuIndex(-1); // There is no item to re-highlight
        // when selections are hidden

        if (this.hideSelected) return;
        this.$nextTick(() => this.setMenuIndex(listIndex));
      }
    },

    setMenuIndex(index) {
      this.$refs.menu && (this.$refs.menu.listIndex = index);
    },

    setSelectedItems() {
      const selectedItems = [];
      const values = !this.multiple || !Array.isArray(this.internalValue) ? [this.internalValue] : this.internalValue;

      for (const value of values) {
        const index = this.allItems.findIndex(v => this.valueComparator(this.getValue(v), this.getValue(value)));

        if (index > -1) {
          selectedItems.push(this.allItems[index]);
        }
      }

      this.selectedItems = selectedItems;
    },

    setValue(value) {
      const oldValue = this.internalValue;
      this.internalValue = value;
      value !== oldValue && this.$emit('change', value);
    },

    isAppendInner(target) {
      // return true if append inner is present
      // and the target is itself or inside
      const appendInner = this.$refs['append-inner'];
      return appendInner && (appendInner === target || appendInner.contains(target));
    }

  }
});

// Styles
const defaultMenuProps$1 = { ...defaultMenuProps,
  offsetY: true,
  offsetOverflow: true,
  transition: false
};
/* @vue/component */

var VAutocomplete = VSelect.extend({
  name: 'v-autocomplete',
  props: {
    allowOverflow: {
      type: Boolean,
      default: true
    },
    autoSelectFirst: {
      type: Boolean,
      default: false
    },
    filter: {
      type: Function,
      default: (item, queryText, itemText) => {
        return itemText.toLocaleLowerCase().indexOf(queryText.toLocaleLowerCase()) > -1;
      }
    },
    hideNoData: Boolean,
    menuProps: {
      type: VSelect.options.props.menuProps.type,
      default: () => defaultMenuProps$1
    },
    noFilter: Boolean,
    searchInput: {
      type: String,
      default: undefined
    }
  },

  data() {
    return {
      lazySearch: this.searchInput
    };
  },

  computed: {
    classes() {
      return { ...VSelect.options.computed.classes.call(this),
        'v-autocomplete': true,
        'v-autocomplete--is-selecting-index': this.selectedIndex > -1
      };
    },

    computedItems() {
      return this.filteredItems;
    },

    selectedValues() {
      return this.selectedItems.map(item => this.getValue(item));
    },

    hasDisplayedItems() {
      return this.hideSelected ? this.filteredItems.some(item => !this.hasItem(item)) : this.filteredItems.length > 0;
    },

    currentRange() {
      if (this.selectedItem == null) return 0;
      return String(this.getText(this.selectedItem)).length;
    },

    filteredItems() {
      if (!this.isSearching || this.noFilter || this.internalSearch == null) return this.allItems;
      return this.allItems.filter(item => this.filter(item, String(this.internalSearch), String(this.getText(item))));
    },

    internalSearch: {
      get() {
        return this.lazySearch;
      },

      set(val) {
        this.lazySearch = val;
        this.$emit('update:search-input', val);
      }

    },

    isAnyValueAllowed() {
      return false;
    },

    isDirty() {
      return this.searchIsDirty || this.selectedItems.length > 0;
    },

    isSearching() {
      return this.multiple && this.searchIsDirty || this.searchIsDirty && this.internalSearch !== this.getText(this.selectedItem);
    },

    menuCanShow() {
      if (!this.isFocused) return false;
      return this.hasDisplayedItems || !this.hideNoData;
    },

    $_menuProps() {
      const props = VSelect.options.computed.$_menuProps.call(this);
      props.contentClass = `v-autocomplete__content ${props.contentClass || ''}`.trim();
      return { ...defaultMenuProps$1,
        ...props
      };
    },

    searchIsDirty() {
      return this.internalSearch != null && this.internalSearch !== '';
    },

    selectedItem() {
      if (this.multiple) return null;
      return this.selectedItems.find(i => {
        return this.valueComparator(this.getValue(i), this.getValue(this.internalValue));
      });
    },

    listData() {
      const data = VSelect.options.computed.listData.call(this);
      data.props = { ...data.props,
        items: this.virtualizedItems,
        noFilter: this.noFilter || !this.isSearching || !this.filteredItems.length,
        searchInput: this.internalSearch
      };
      return data;
    }

  },
  watch: {
    filteredItems: 'onFilteredItemsChanged',
    internalValue: 'setSearch',

    isFocused(val) {
      if (val) {
        document.addEventListener('copy', this.onCopy);
        this.$refs.input && this.$refs.input.select();
      } else {
        document.removeEventListener('copy', this.onCopy);
        this.updateSelf();
      }
    },

    isMenuActive(val) {
      if (val || !this.hasSlot) return;
      this.lazySearch = undefined;
    },

    items(val, oldVal) {
      // If we are focused, the menu
      // is not active, hide no data is enabled,
      // and items change
      // User is probably async loading
      // items, try to activate the menu
      if (!(oldVal && oldVal.length) && this.hideNoData && this.isFocused && !this.isMenuActive && val.length) this.activateMenu();
    },

    searchInput(val) {
      this.lazySearch = val;
    },

    internalSearch: 'onInternalSearchChanged',
    itemText: 'updateSelf'
  },

  created() {
    this.setSearch();
  },

  methods: {
    onFilteredItemsChanged(val, oldVal) {
      // TODO: How is the watcher triggered
      // for duplicate items? no idea
      if (val === oldVal) return;
      this.setMenuIndex(-1);
      this.$nextTick(() => {
        if (!this.internalSearch || val.length !== 1 && !this.autoSelectFirst) return;
        this.$refs.menu.getTiles();
        this.setMenuIndex(0);
      });
    },

    onInternalSearchChanged() {
      this.updateMenuDimensions();
    },

    updateMenuDimensions() {
      // Type from menuable is not making it through
      this.isMenuActive && this.$refs.menu && this.$refs.menu.updateDimensions();
    },

    changeSelectedIndex(keyCode) {
      // Do not allow changing of selectedIndex
      // when search is dirty
      if (this.searchIsDirty) return;

      if (this.multiple && keyCode === keyCodes.left) {
        if (this.selectedIndex === -1) {
          this.selectedIndex = this.selectedItems.length - 1;
        } else {
          this.selectedIndex--;
        }
      } else if (this.multiple && keyCode === keyCodes.right) {
        if (this.selectedIndex >= this.selectedItems.length - 1) {
          this.selectedIndex = -1;
        } else {
          this.selectedIndex++;
        }
      } else if (keyCode === keyCodes.backspace || keyCode === keyCodes.delete) {
        this.deleteCurrentItem();
      }
    },

    deleteCurrentItem() {
      if (this.readonly) return;
      const index = this.selectedItems.length - 1;

      if (this.selectedIndex === -1 && index !== 0) {
        this.selectedIndex = index;
        return;
      }

      const currentItem = this.selectedItems[this.selectedIndex];
      if (this.getDisabled(currentItem)) return;
      const newIndex = this.selectedIndex === index ? this.selectedIndex - 1 : this.selectedItems[this.selectedIndex + 1] ? this.selectedIndex : -1;

      if (newIndex === -1) {
        this.setValue(this.multiple ? [] : undefined);
      } else {
        this.selectItem(currentItem);
      }

      this.selectedIndex = newIndex;
    },

    clearableCallback() {
      this.internalSearch = undefined;
      VSelect.options.methods.clearableCallback.call(this);
    },

    genInput() {
      const input = VTextField.options.methods.genInput.call(this);
      input.data = mergeData(input.data, {
        attrs: {
          'aria-activedescendant': getObjectValueByPath(this.$refs.menu, 'activeTile.id'),
          autocomplete: getObjectValueByPath(input.data, 'attrs.autocomplete', 'off')
        },
        domProps: {
          value: this.internalSearch
        }
      });
      return input;
    },

    genInputSlot() {
      const slot = VSelect.options.methods.genInputSlot.call(this);
      slot.data.attrs.role = 'combobox';
      return slot;
    },

    genSelections() {
      return this.hasSlot || this.multiple ? VSelect.options.methods.genSelections.call(this) : [];
    },

    onClick(e) {
      if (this.isDisabled) return;
      this.selectedIndex > -1 ? this.selectedIndex = -1 : this.onFocus();
      if (!this.isAppendInner(e.target)) this.activateMenu();
    },

    onInput(e) {
      if (this.selectedIndex > -1 || !e.target) return;
      const target = e.target;
      const value = target.value; // If typing and menu is not currently active

      if (target.value) this.activateMenu();
      this.internalSearch = value;
      this.badInput = target.validity && target.validity.badInput;
    },

    onKeyDown(e) {
      const keyCode = e.keyCode;
      VSelect.options.methods.onKeyDown.call(this, e); // The ordering is important here
      // allows new value to be updated
      // and then moves the index to the
      // proper location

      this.changeSelectedIndex(keyCode);
    },

    onSpaceDown(e) {},

    onTabDown(e) {
      VSelect.options.methods.onTabDown.call(this, e);
      this.updateSelf();
    },

    onUpDown(e) {
      // Prevent screen from scrolling
      e.preventDefault(); // For autocomplete / combobox, cycling
      // interfers with native up/down behavior
      // instead activate the menu

      this.activateMenu();
    },

    selectItem(item) {
      VSelect.options.methods.selectItem.call(this, item);
      this.setSearch();
    },

    setSelectedItems() {
      VSelect.options.methods.setSelectedItems.call(this); // #4273 Don't replace if searching
      // #4403 Don't replace if focused

      if (!this.isFocused) this.setSearch();
    },

    setSearch() {
      // Wait for nextTick so selectedItem
      // has had time to update
      this.$nextTick(() => {
        if (!this.multiple || !this.internalSearch || !this.isMenuActive) {
          this.internalSearch = !this.selectedItems.length || this.multiple || this.hasSlot ? null : this.getText(this.selectedItem);
        }
      });
    },

    updateSelf() {
      if (!this.searchIsDirty && !this.internalValue) return;

      if (!this.valueComparator(this.internalSearch, this.getValue(this.internalValue))) {
        this.setSearch();
      }
    },

    hasItem(item) {
      return this.selectedValues.indexOf(this.getValue(item)) > -1;
    },

    onCopy(event) {
      if (this.selectedIndex === -1) return;
      const currentItem = this.selectedItems[this.selectedIndex];
      const currentItemText = this.getText(currentItem);
      event.clipboardData.setData('text/plain', currentItemText);
      event.clipboardData.setData('text/vnd.vuetify.autocomplete.item+plain', currentItemText);
      event.preventDefault();
    }

  }
});

// Styles
var VBadge = mixins(Colorable, factory(['left', 'bottom']), Themeable, Toggleable, Transitionable).extend({
  name: 'v-badge',
  props: {
    avatar: Boolean,
    bordered: Boolean,
    color: {
      type: String,
      default: 'primary'
    },
    content: {
      required: false
    },
    dot: Boolean,
    label: {
      type: String,
      default: '$vuetify.badge'
    },
    icon: String,
    inline: Boolean,
    offsetX: [Number, String],
    offsetY: [Number, String],
    overlap: Boolean,
    tile: Boolean,
    transition: {
      type: String,
      default: 'scale-rotate-transition'
    },
    value: {
      default: true
    }
  },
  computed: {
    classes() {
      return {
        'v-badge--avatar': this.avatar,
        'v-badge--bordered': this.bordered,
        'v-badge--bottom': this.bottom,
        'v-badge--dot': this.dot,
        'v-badge--icon': this.icon != null,
        'v-badge--inline': this.inline,
        'v-badge--left': this.left,
        'v-badge--overlap': this.overlap,
        'v-badge--tile': this.tile,
        ...this.themeClasses
      };
    },

    computedBottom() {
      return this.bottom ? 'auto' : this.computedYOffset;
    },

    computedLeft() {
      if (this.isRtl) {
        return this.left ? this.computedXOffset : 'auto';
      }

      return this.left ? 'auto' : this.computedXOffset;
    },

    computedRight() {
      if (this.isRtl) {
        return this.left ? 'auto' : this.computedXOffset;
      }

      return !this.left ? 'auto' : this.computedXOffset;
    },

    computedTop() {
      return this.bottom ? this.computedYOffset : 'auto';
    },

    computedXOffset() {
      return this.calcPosition(this.offsetX);
    },

    computedYOffset() {
      return this.calcPosition(this.offsetY);
    },

    isRtl() {
      return this.$vuetify.rtl;
    },

    // Default fallback if offsetX
    // or offsetY are undefined.
    offset() {
      if (this.overlap) return this.dot ? 8 : 12;
      return this.dot ? 2 : 4;
    },

    styles() {
      if (this.inline) return {};
      return {
        bottom: this.computedBottom,
        left: this.computedLeft,
        right: this.computedRight,
        top: this.computedTop
      };
    }

  },
  methods: {
    calcPosition(offset) {
      return `calc(100% - ${convertToUnit(offset || this.offset)})`;
    },

    genBadge() {
      const lang = this.$vuetify.lang;
      const label = this.$attrs['aria-label'] || lang.t(this.label);
      const data = this.setBackgroundColor(this.color, {
        staticClass: 'v-badge__badge',
        style: this.styles,
        attrs: {
          'aria-atomic': this.$attrs['aria-atomic'] || 'true',
          'aria-label': label,
          'aria-live': this.$attrs['aria-live'] || 'polite',
          title: this.$attrs.title,
          role: this.$attrs.role || 'status'
        },
        directives: [{
          name: 'show',
          value: this.isActive
        }]
      });
      const badge = this.$createElement('span', data, [this.genBadgeContent()]);
      if (!this.transition) return badge;
      return this.$createElement('transition', {
        props: {
          name: this.transition,
          origin: this.origin,
          mode: this.mode
        }
      }, [badge]);
    },

    genBadgeContent() {
      // Dot prop shows no content
      if (this.dot) return undefined;
      const slot = getSlot(this, 'badge');
      if (slot) return slot;
      if (this.content) return String(this.content);
      if (this.icon) return this.$createElement(VIcon$1, this.icon);
      return undefined;
    },

    genBadgeWrapper() {
      return this.$createElement('span', {
        staticClass: 'v-badge__wrapper'
      }, [this.genBadge()]);
    }

  },

  render(h) {
    const badge = [this.genBadgeWrapper()];
    const children = [getSlot(this)];
    const {
      'aria-atomic': _x,
      'aria-label': _y,
      'aria-live': _z,
      role,
      title,
      ...attrs
    } = this.$attrs;
    if (this.inline && this.left) children.unshift(badge);else children.push(badge);
    return h('span', {
      staticClass: 'v-badge',
      attrs,
      class: this.classes
    }, children);
  }

});

// Extensions
/* @vue/component */

var ButtonGroup = BaseItemGroup.extend({
  name: 'button-group',

  provide() {
    return {
      btnToggle: this
    };
  },

  computed: {
    classes() {
      return BaseItemGroup.options.computed.classes.call(this);
    }

  },
  methods: {
    // Isn't being passed down through types
    genData: BaseItemGroup.options.methods.genData
  }
});

// Styles
/* @vue/component */

var VOverlay = mixins(Colorable, Themeable, Toggleable).extend({
  name: 'v-overlay',
  props: {
    absolute: Boolean,
    color: {
      type: String,
      default: '#212121'
    },
    dark: {
      type: Boolean,
      default: true
    },
    opacity: {
      type: [Number, String],
      default: 0.46
    },
    value: {
      default: true
    },
    zIndex: {
      type: [Number, String],
      default: 5
    }
  },
  computed: {
    __scrim() {
      const data = this.setBackgroundColor(this.color, {
        staticClass: 'v-overlay__scrim',
        style: {
          opacity: this.computedOpacity
        }
      });
      return this.$createElement('div', data);
    },

    classes() {
      return {
        'v-overlay--absolute': this.absolute,
        'v-overlay--active': this.isActive,
        ...this.themeClasses
      };
    },

    computedOpacity() {
      return Number(this.isActive ? this.opacity : 0);
    },

    styles() {
      return {
        zIndex: this.zIndex
      };
    }

  },
  methods: {
    genContent() {
      return this.$createElement('div', {
        staticClass: 'v-overlay__content'
      }, this.$slots.default);
    }

  },

  render(h) {
    const children = [this.__scrim];
    if (this.isActive) children.push(this.genContent());
    return h('div', {
      staticClass: 'v-overlay',
      class: this.classes,
      style: this.styles
    }, children);
  }

});

// Components
/* @vue/component */

var Overlayable = Vue.extend().extend({
  name: 'overlayable',
  props: {
    hideOverlay: Boolean,
    overlayColor: String,
    overlayOpacity: [Number, String]
  },

  data() {
    return {
      overlay: null
    };
  },

  watch: {
    hideOverlay(value) {
      if (!this.isActive) return;
      if (value) this.removeOverlay();else this.genOverlay();
    }

  },

  beforeDestroy() {
    this.removeOverlay();
  },

  methods: {
    createOverlay() {
      const overlay = new VOverlay({
        propsData: {
          absolute: this.absolute,
          value: false,
          color: this.overlayColor,
          opacity: this.overlayOpacity
        }
      });
      overlay.$mount();
      const parent = this.absolute ? this.$el.parentNode : document.querySelector('[data-app]');
      parent && parent.insertBefore(overlay.$el, parent.firstChild);
      this.overlay = overlay;
    },

    genOverlay() {
      this.hideScroll();
      if (this.hideOverlay) return;
      if (!this.overlay) this.createOverlay();
      requestAnimationFrame(() => {
        if (!this.overlay) return;

        if (this.activeZIndex !== undefined) {
          this.overlay.zIndex = String(this.activeZIndex - 1);
        } else if (this.$el) {
          this.overlay.zIndex = getZIndex(this.$el);
        }
      });

      if (this.overlay) {
        this.overlay.value = true;
      }

      return true;
    },

    /** removeOverlay(false) will not restore the scollbar afterwards */
    removeOverlay(showScroll = true) {
      if (this.overlay) {
        addOnceEventListener(this.overlay.$el, 'transitionend', () => {
          if (!this.overlay || !this.overlay.$el || !this.overlay.$el.parentNode || this.overlay.value) return;
          this.overlay.$el.parentNode.removeChild(this.overlay.$el);
          this.overlay.$destroy();
          this.overlay = null;
        });
        this.overlay.value = false;
      }

      showScroll && this.showScroll();
    },

    scrollListener(e) {
      if (e.type === 'keydown') {
        if (['INPUT', 'TEXTAREA', 'SELECT'].includes(e.target.tagName) || // https://github.com/vuetifyjs/vuetify/issues/4715
        e.target.isContentEditable) return;
        const up = [keyCodes.up, keyCodes.pageup];
        const down = [keyCodes.down, keyCodes.pagedown];

        if (up.includes(e.keyCode)) {
          e.deltaY = -1;
        } else if (down.includes(e.keyCode)) {
          e.deltaY = 1;
        } else {
          return;
        }
      }

      if (e.target === this.overlay || e.type !== 'keydown' && e.target === document.body || this.checkPath(e)) e.preventDefault();
    },

    hasScrollbar(el) {
      if (!el || el.nodeType !== Node.ELEMENT_NODE) return false;
      const style = window.getComputedStyle(el);
      return ['auto', 'scroll'].includes(style.overflowY) && el.scrollHeight > el.clientHeight;
    },

    shouldScroll(el, delta) {
      if (el.scrollTop === 0 && delta < 0) return true;
      return el.scrollTop + el.clientHeight === el.scrollHeight && delta > 0;
    },

    isInside(el, parent) {
      if (el === parent) {
        return true;
      } else if (el === null || el === document.body) {
        return false;
      } else {
        return this.isInside(el.parentNode, parent);
      }
    },

    checkPath(e) {
      const path = e.path || this.composedPath(e);
      const delta = e.deltaY;

      if (e.type === 'keydown' && path[0] === document.body) {
        const dialog = this.$refs.dialog; // getSelection returns null in firefox in some edge cases, can be ignored

        const selected = window.getSelection().anchorNode;

        if (dialog && this.hasScrollbar(dialog) && this.isInside(selected, dialog)) {
          return this.shouldScroll(dialog, delta);
        }

        return true;
      }

      for (let index = 0; index < path.length; index++) {
        const el = path[index];
        if (el === document) return true;
        if (el === document.documentElement) return true;
        if (el === this.$refs.content) return true;
        if (this.hasScrollbar(el)) return this.shouldScroll(el, delta);
      }

      return true;
    },

    /**
     * Polyfill for Event.prototype.composedPath
     */
    composedPath(e) {
      if (e.composedPath) return e.composedPath();
      const path = [];
      let el = e.target;

      while (el) {
        path.push(el);

        if (el.tagName === 'HTML') {
          path.push(document);
          path.push(window);
          return path;
        }

        el = el.parentElement;
      }

      return path;
    },

    hideScroll() {
      if (this.$vuetify.breakpoint.smAndDown) {
        document.documentElement.classList.add('overflow-y-hidden');
      } else {
        addPassiveEventListener(window, 'wheel', this.scrollListener, {
          passive: false
        });
        window.addEventListener('keydown', this.scrollListener);
      }
    },

    showScroll() {
      document.documentElement.classList.remove('overflow-y-hidden');
      window.removeEventListener('wheel', this.scrollListener);
      window.removeEventListener('keydown', this.scrollListener);
    }

  }
});

// Styles
/* @vue/component */

var VBtnToggle = mixins(ButtonGroup, Colorable).extend({
  name: 'v-btn-toggle',
  props: {
    backgroundColor: String,
    borderless: Boolean,
    dense: Boolean,
    group: Boolean,
    rounded: Boolean,
    shaped: Boolean,
    tile: Boolean
  },
  computed: {
    classes() {
      return { ...ButtonGroup.options.computed.classes.call(this),
        'v-btn-toggle': true,
        'v-btn-toggle--borderless': this.borderless,
        'v-btn-toggle--dense': this.dense,
        'v-btn-toggle--group': this.group,
        'v-btn-toggle--rounded': this.rounded,
        'v-btn-toggle--shaped': this.shaped,
        'v-btn-toggle--tile': this.tile,
        ...this.themeClasses
      };
    }

  },
  methods: {
    genData() {
      const data = this.setTextColor(this.color, { ...ButtonGroup.options.methods.genData.call(this)
      });
      if (this.group) return data;
      return this.setBackgroundColor(this.backgroundColor, data);
    }

  }
});

// Styles
/* @vue/component */

var VCard = mixins(Loadable, Routable, VSheet).extend({
  name: 'v-card',
  props: {
    flat: Boolean,
    hover: Boolean,
    img: String,
    link: Boolean,
    loaderHeight: {
      type: [Number, String],
      default: 4
    },
    outlined: Boolean,
    raised: Boolean,
    shaped: Boolean
  },
  computed: {
    classes() {
      return {
        'v-card': true,
        ...Routable.options.computed.classes.call(this),
        'v-card--flat': this.flat,
        'v-card--hover': this.hover,
        'v-card--link': this.isClickable,
        'v-card--loading': this.loading,
        'v-card--disabled': this.disabled,
        'v-card--outlined': this.outlined,
        'v-card--raised': this.raised,
        'v-card--shaped': this.shaped,
        ...VSheet.options.computed.classes.call(this)
      };
    },

    styles() {
      const style = { ...VSheet.options.computed.styles.call(this)
      };

      if (this.img) {
        style.background = `url("${this.img}") center center / cover no-repeat`;
      }

      return style;
    }

  },
  methods: {
    genProgress() {
      const render = Loadable.options.methods.genProgress.call(this);
      if (!render) return null;
      return this.$createElement('div', {
        staticClass: 'v-card__progress',
        key: 'progress'
      }, [render]);
    }

  },

  render(h) {
    const {
      tag,
      data
    } = this.generateRouteLink();
    data.style = this.styles;

    if (this.isClickable) {
      data.attrs = data.attrs || {};
      data.attrs.tabindex = 0;
    }

    return h(tag, this.setBackgroundColor(this.color, data), [this.genProgress(), this.$slots.default]);
  }

});

const VCardActions = createSimpleFunctional('v-card__actions');
const VCardSubtitle = createSimpleFunctional('v-card__subtitle');
const VCardText = createSimpleFunctional('v-card__text');
const VCardTitle = createSimpleFunctional('v-card__title');

// Directives
var Rippleable = Vue.extend({
  name: 'rippleable',
  directives: {
    ripple: Ripple
  },
  props: {
    ripple: {
      type: [Boolean, Object],
      default: true
    }
  },
  methods: {
    genRipple(data = {}) {
      if (!this.ripple) return null;
      data.staticClass = 'v-input--selection-controls__ripple';
      data.directives = data.directives || [];
      data.directives.push({
        name: 'ripple',
        value: {
          center: true
        }
      });
      data.on = Object.assign({
        click: this.onChange
      }, this.$listeners);
      return this.$createElement('div', data);
    },

    onChange() {}

  }
});

// Components
/* @vue/component */

var Selectable = mixins(VInput, Rippleable, Comparable).extend({
  name: 'selectable',
  model: {
    prop: 'inputValue',
    event: 'change'
  },
  props: {
    id: String,
    inputValue: null,
    falseValue: null,
    trueValue: null,
    multiple: {
      type: Boolean,
      default: null
    },
    label: String
  },

  data() {
    return {
      hasColor: this.inputValue,
      lazyValue: this.inputValue
    };
  },

  computed: {
    computedColor() {
      if (!this.isActive) return undefined;
      if (this.color) return this.color;
      if (this.isDark && !this.appIsDark) return 'white';
      return 'primary';
    },

    isMultiple() {
      return this.multiple === true || this.multiple === null && Array.isArray(this.internalValue);
    },

    isActive() {
      const value = this.value;
      const input = this.internalValue;

      if (this.isMultiple) {
        if (!Array.isArray(input)) return false;
        return input.some(item => this.valueComparator(item, value));
      }

      if (this.trueValue === undefined || this.falseValue === undefined) {
        return value ? this.valueComparator(value, input) : Boolean(input);
      }

      return this.valueComparator(input, this.trueValue);
    },

    isDirty() {
      return this.isActive;
    },

    rippleState() {
      return !this.disabled && !this.validationState ? undefined : this.validationState;
    }

  },
  watch: {
    inputValue(val) {
      this.lazyValue = val;
      this.hasColor = val;
    }

  },
  methods: {
    genLabel() {
      const label = VInput.options.methods.genLabel.call(this);
      if (!label) return label;
      label.data.on = {
        click: e => {
          // Prevent label from
          // causing the input
          // to focus
          e.preventDefault();
          this.onChange();
        }
      };
      return label;
    },

    genInput(type, attrs) {
      return this.$createElement('input', {
        attrs: Object.assign({
          'aria-checked': this.isActive.toString(),
          disabled: this.isDisabled,
          id: this.computedId,
          role: type,
          type
        }, attrs),
        domProps: {
          value: this.value,
          checked: this.isActive
        },
        on: {
          blur: this.onBlur,
          change: this.onChange,
          focus: this.onFocus,
          keydown: this.onKeydown
        },
        ref: 'input'
      });
    },

    onBlur() {
      this.isFocused = false;
    },

    onChange() {
      if (this.isDisabled) return;
      const value = this.value;
      let input = this.internalValue;

      if (this.isMultiple) {
        if (!Array.isArray(input)) {
          input = [];
        }

        const length = input.length;
        input = input.filter(item => !this.valueComparator(item, value));

        if (input.length === length) {
          input.push(value);
        }
      } else if (this.trueValue !== undefined && this.falseValue !== undefined) {
        input = this.valueComparator(input, this.trueValue) ? this.falseValue : this.trueValue;
      } else if (value) {
        input = this.valueComparator(input, value) ? null : value;
      } else {
        input = !input;
      }

      this.validate(true, input);
      this.internalValue = input;
      this.hasColor = input;
    },

    onFocus() {
      this.isFocused = true;
    },

    /** @abstract */
    onKeydown(e) {}

  }
});

// Styles
/* @vue/component */

var VCheckbox = Selectable.extend({
  name: 'v-checkbox',
  props: {
    indeterminate: Boolean,
    indeterminateIcon: {
      type: String,
      default: '$checkboxIndeterminate'
    },
    offIcon: {
      type: String,
      default: '$checkboxOff'
    },
    onIcon: {
      type: String,
      default: '$checkboxOn'
    }
  },

  data() {
    return {
      inputIndeterminate: this.indeterminate
    };
  },

  computed: {
    classes() {
      return { ...VInput.options.computed.classes.call(this),
        'v-input--selection-controls': true,
        'v-input--checkbox': true,
        'v-input--indeterminate': this.inputIndeterminate
      };
    },

    computedIcon() {
      if (this.inputIndeterminate) {
        return this.indeterminateIcon;
      } else if (this.isActive) {
        return this.onIcon;
      } else {
        return this.offIcon;
      }
    },

    // Do not return undefined if disabled,
    // according to spec, should still show
    // a color when disabled and active
    validationState() {
      if (this.disabled && !this.inputIndeterminate) return undefined;
      if (this.hasError && this.shouldValidate) return 'error';
      if (this.hasSuccess) return 'success';
      if (this.hasColor !== null) return this.computedColor;
      return undefined;
    }

  },
  watch: {
    indeterminate(val) {
      // https://github.com/vuetifyjs/vuetify/issues/8270
      this.$nextTick(() => this.inputIndeterminate = val);
    },

    inputIndeterminate(val) {
      this.$emit('update:indeterminate', val);
    },

    isActive() {
      if (!this.indeterminate) return;
      this.inputIndeterminate = false;
    }

  },
  methods: {
    genCheckbox() {
      return this.$createElement('div', {
        staticClass: 'v-input--selection-controls__input'
      }, [this.$createElement(VIcon$1, this.setTextColor(this.validationState, {
        props: {
          dense: this.dense,
          dark: this.dark,
          light: this.light
        }
      }), this.computedIcon), this.genInput('checkbox', { ...this.attrs$,
        'aria-checked': this.inputIndeterminate ? 'mixed' : this.isActive.toString()
      }), this.genRipple(this.setTextColor(this.rippleState))]);
    },

    genDefaultSlot() {
      return [this.genCheckbox(), this.genLabel()];
    }

  }
});

// Helpers
var VData = Vue.extend({
  name: 'v-data',
  inheritAttrs: false,
  props: {
    items: {
      type: Array,
      default: () => []
    },
    options: {
      type: Object,
      default: () => ({})
    },
    sortBy: {
      type: [String, Array],
      default: () => []
    },
    sortDesc: {
      type: [Boolean, Array],
      default: () => []
    },
    customSort: {
      type: Function,
      default: sortItems
    },
    mustSort: Boolean,
    multiSort: Boolean,
    page: {
      type: Number,
      default: 1
    },
    itemsPerPage: {
      type: Number,
      default: 10
    },
    groupBy: {
      type: [String, Array],
      default: () => []
    },
    groupDesc: {
      type: [Boolean, Array],
      default: () => []
    },
    customGroup: {
      type: Function,
      default: groupItems
    },
    locale: {
      type: String,
      default: 'en-US'
    },
    disableSort: Boolean,
    disablePagination: Boolean,
    disableFiltering: Boolean,
    search: String,
    customFilter: {
      type: Function,
      default: searchItems
    },
    serverItemsLength: {
      type: Number,
      default: -1
    }
  },

  data() {
    let internalOptions = {
      page: this.page,
      itemsPerPage: this.itemsPerPage,
      sortBy: wrapInArray(this.sortBy),
      sortDesc: wrapInArray(this.sortDesc),
      groupBy: wrapInArray(this.groupBy),
      groupDesc: wrapInArray(this.groupDesc),
      mustSort: this.mustSort,
      multiSort: this.multiSort
    };

    if (this.options) {
      internalOptions = Object.assign(internalOptions, this.options);
    }

    return {
      internalOptions
    };
  },

  computed: {
    itemsLength() {
      return this.serverItemsLength >= 0 ? this.serverItemsLength : this.filteredItems.length;
    },

    pageCount() {
      return this.internalOptions.itemsPerPage <= 0 ? 1 : Math.ceil(this.itemsLength / this.internalOptions.itemsPerPage);
    },

    pageStart() {
      if (this.internalOptions.itemsPerPage === -1 || !this.items.length) return 0;
      return (this.internalOptions.page - 1) * this.internalOptions.itemsPerPage;
    },

    pageStop() {
      if (this.internalOptions.itemsPerPage === -1) return this.itemsLength;
      if (!this.items.length) return 0;
      return Math.min(this.itemsLength, this.internalOptions.page * this.internalOptions.itemsPerPage);
    },

    isGrouped() {
      return !!this.internalOptions.groupBy.length;
    },

    pagination() {
      return {
        page: this.internalOptions.page,
        itemsPerPage: this.internalOptions.itemsPerPage,
        pageStart: this.pageStart,
        pageStop: this.pageStop,
        pageCount: this.pageCount,
        itemsLength: this.itemsLength
      };
    },

    filteredItems() {
      let items = this.items.slice();

      if (!this.disableFiltering && this.serverItemsLength <= 0) {
        items = this.customFilter(items, this.search);
      }

      return items;
    },

    computedItems() {
      let items = this.filteredItems.slice();

      if (!this.disableSort && this.serverItemsLength <= 0) {
        items = this.sortItems(items);
      }

      if (!this.disablePagination && this.serverItemsLength <= 0) {
        items = this.paginateItems(items);
      }

      return items;
    },

    groupedItems() {
      return this.isGrouped ? this.groupItems(this.computedItems) : null;
    },

    scopedProps() {
      const props = {
        sort: this.sort,
        sortArray: this.sortArray,
        group: this.group,
        items: this.computedItems,
        options: this.internalOptions,
        updateOptions: this.updateOptions,
        pagination: this.pagination,
        groupedItems: this.groupedItems,
        originalItemsLength: this.items.length
      };
      return props;
    },

    computedOptions() {
      return { ...this.options
      };
    }

  },
  watch: {
    computedOptions: {
      handler(options, old) {
        if (deepEqual(options, old)) return;
        this.updateOptions(options);
      },

      deep: true,
      immediate: true
    },
    internalOptions: {
      handler(options, old) {
        if (deepEqual(options, old)) return;
        this.$emit('update:options', options);
      },

      deep: true,
      immediate: true
    },

    page(page) {
      this.updateOptions({
        page
      });
    },

    'internalOptions.page'(page) {
      this.$emit('update:page', page);
    },

    itemsPerPage(itemsPerPage) {
      this.updateOptions({
        itemsPerPage
      });
    },

    'internalOptions.itemsPerPage'(itemsPerPage) {
      this.$emit('update:items-per-page', itemsPerPage);
    },

    sortBy(sortBy) {
      this.updateOptions({
        sortBy: wrapInArray(sortBy)
      });
    },

    'internalOptions.sortBy'(sortBy, old) {
      !deepEqual(sortBy, old) && this.$emit('update:sort-by', Array.isArray(this.sortBy) ? sortBy : sortBy[0]);
    },

    sortDesc(sortDesc) {
      this.updateOptions({
        sortDesc: wrapInArray(sortDesc)
      });
    },

    'internalOptions.sortDesc'(sortDesc, old) {
      !deepEqual(sortDesc, old) && this.$emit('update:sort-desc', Array.isArray(this.sortDesc) ? sortDesc : sortDesc[0]);
    },

    groupBy(groupBy) {
      this.updateOptions({
        groupBy: wrapInArray(groupBy)
      });
    },

    'internalOptions.groupBy'(groupBy, old) {
      !deepEqual(groupBy, old) && this.$emit('update:group-by', Array.isArray(this.groupBy) ? groupBy : groupBy[0]);
    },

    groupDesc(groupDesc) {
      this.updateOptions({
        groupDesc: wrapInArray(groupDesc)
      });
    },

    'internalOptions.groupDesc'(groupDesc, old) {
      !deepEqual(groupDesc, old) && this.$emit('update:group-desc', Array.isArray(this.groupDesc) ? groupDesc : groupDesc[0]);
    },

    multiSort(multiSort) {
      this.updateOptions({
        multiSort
      });
    },

    'internalOptions.multiSort'(multiSort) {
      this.$emit('update:multi-sort', multiSort);
    },

    mustSort(mustSort) {
      this.updateOptions({
        mustSort
      });
    },

    'internalOptions.mustSort'(mustSort) {
      this.$emit('update:must-sort', mustSort);
    },

    pageCount: {
      handler(pageCount) {
        this.$emit('page-count', pageCount);
      },

      immediate: true
    },
    computedItems: {
      handler(computedItems) {
        this.$emit('current-items', computedItems);
      },

      immediate: true
    },
    pagination: {
      handler(pagination, old) {
        if (deepEqual(pagination, old)) return;
        this.$emit('pagination', this.pagination);
      },

      immediate: true
    }
  },
  methods: {
    toggle(key, oldBy, oldDesc, page, mustSort, multiSort) {
      let by = oldBy.slice();
      let desc = oldDesc.slice();
      const byIndex = by.findIndex(k => k === key);

      if (byIndex < 0) {
        if (!multiSort) {
          by = [];
          desc = [];
        }

        by.push(key);
        desc.push(false);
      } else if (byIndex >= 0 && !desc[byIndex]) {
        desc[byIndex] = true;
      } else if (!mustSort) {
        by.splice(byIndex, 1);
        desc.splice(byIndex, 1);
      } else {
        desc[byIndex] = false;
      } // Reset page to 1 if sortBy or sortDesc have changed


      if (!deepEqual(by, oldBy) || !deepEqual(desc, oldDesc)) {
        page = 1;
      }

      return {
        by,
        desc,
        page
      };
    },

    group(key) {
      const {
        by: groupBy,
        desc: groupDesc,
        page
      } = this.toggle(key, this.internalOptions.groupBy, this.internalOptions.groupDesc, this.internalOptions.page, true, false);
      this.updateOptions({
        groupBy,
        groupDesc,
        page
      });
    },

    sort(key) {
      if (Array.isArray(key)) return this.sortArray(key);
      const {
        by: sortBy,
        desc: sortDesc,
        page
      } = this.toggle(key, this.internalOptions.sortBy, this.internalOptions.sortDesc, this.internalOptions.page, this.internalOptions.mustSort, this.internalOptions.multiSort);
      this.updateOptions({
        sortBy,
        sortDesc,
        page
      });
    },

    sortArray(sortBy) {
      const sortDesc = sortBy.map(s => {
        const i = this.internalOptions.sortBy.findIndex(k => k === s);
        return i > -1 ? this.internalOptions.sortDesc[i] : false;
      });
      this.updateOptions({
        sortBy,
        sortDesc
      });
    },

    updateOptions(options) {
      this.internalOptions = { ...this.internalOptions,
        ...options,
        page: this.serverItemsLength < 0 ? Math.max(1, Math.min(options.page || this.internalOptions.page, this.pageCount)) : options.page || this.internalOptions.page
      };
    },

    sortItems(items) {
      let sortBy = this.internalOptions.sortBy;
      let sortDesc = this.internalOptions.sortDesc;

      if (this.internalOptions.groupBy.length) {
        sortBy = [...this.internalOptions.groupBy, ...sortBy];
        sortDesc = [...this.internalOptions.groupDesc, ...sortDesc];
      }

      return this.customSort(items, sortBy, sortDesc, this.locale);
    },

    groupItems(items) {
      return this.customGroup(items, this.internalOptions.groupBy, this.internalOptions.groupDesc);
    },

    paginateItems(items) {
      // Make sure we don't try to display non-existant page if items suddenly change
      // TODO: Could possibly move this to pageStart/pageStop?
      if (this.serverItemsLength === -1 && items.length <= this.pageStart) {
        this.internalOptions.page = Math.max(1, this.internalOptions.page - 1);
      }

      return items.slice(this.pageStart, this.pageStop);
    }

  },

  render() {
    return this.$scopedSlots.default && this.$scopedSlots.default(this.scopedProps);
  }

});

var VDataFooter = Vue.extend({
  name: 'v-data-footer',
  props: {
    options: {
      type: Object,
      required: true
    },
    pagination: {
      type: Object,
      required: true
    },
    itemsPerPageOptions: {
      type: Array,
      default: () => [5, 10, 15, -1]
    },
    prevIcon: {
      type: String,
      default: '$prev'
    },
    nextIcon: {
      type: String,
      default: '$next'
    },
    firstIcon: {
      type: String,
      default: '$first'
    },
    lastIcon: {
      type: String,
      default: '$last'
    },
    itemsPerPageText: {
      type: String,
      default: '$vuetify.dataFooter.itemsPerPageText'
    },
    itemsPerPageAllText: {
      type: String,
      default: '$vuetify.dataFooter.itemsPerPageAll'
    },
    showFirstLastPage: Boolean,
    showCurrentPage: Boolean,
    disablePagination: Boolean,
    disableItemsPerPage: Boolean,
    pageText: {
      type: String,
      default: '$vuetify.dataFooter.pageText'
    }
  },
  computed: {
    disableNextPageIcon() {
      return this.options.itemsPerPage <= 0 || this.options.page * this.options.itemsPerPage >= this.pagination.itemsLength || this.pagination.pageStop < 0;
    },

    computedDataItemsPerPageOptions() {
      return this.itemsPerPageOptions.map(option => {
        if (typeof option === 'object') return option;else return this.genDataItemsPerPageOption(option);
      });
    }

  },
  methods: {
    updateOptions(obj) {
      this.$emit('update:options', Object.assign({}, this.options, obj));
    },

    onFirstPage() {
      this.updateOptions({
        page: 1
      });
    },

    onPreviousPage() {
      this.updateOptions({
        page: this.options.page - 1
      });
    },

    onNextPage() {
      this.updateOptions({
        page: this.options.page + 1
      });
    },

    onLastPage() {
      this.updateOptions({
        page: this.pagination.pageCount
      });
    },

    onChangeItemsPerPage(itemsPerPage) {
      this.updateOptions({
        itemsPerPage,
        page: 1
      });
    },

    genDataItemsPerPageOption(option) {
      return {
        text: option === -1 ? this.$vuetify.lang.t(this.itemsPerPageAllText) : String(option),
        value: option
      };
    },

    genItemsPerPageSelect() {
      let value = this.options.itemsPerPage;
      const computedIPPO = this.computedDataItemsPerPageOptions;
      if (computedIPPO.length <= 1) return null;
      if (!computedIPPO.find(ippo => ippo.value === value)) value = computedIPPO[0];
      return this.$createElement('div', {
        staticClass: 'v-data-footer__select'
      }, [this.$vuetify.lang.t(this.itemsPerPageText), this.$createElement(VSelect, {
        attrs: {
          'aria-label': this.itemsPerPageText
        },
        props: {
          disabled: this.disableItemsPerPage,
          items: computedIPPO,
          value,
          hideDetails: true,
          auto: true,
          minWidth: '75px'
        },
        on: {
          input: this.onChangeItemsPerPage
        }
      })]);
    },

    genPaginationInfo() {
      let children = ['–'];

      if (this.pagination.itemsLength && this.pagination.itemsPerPage) {
        const itemsLength = this.pagination.itemsLength;
        const pageStart = this.pagination.pageStart + 1;
        const pageStop = itemsLength < this.pagination.pageStop || this.pagination.pageStop < 0 ? itemsLength : this.pagination.pageStop;
        children = this.$scopedSlots['page-text'] ? [this.$scopedSlots['page-text']({
          pageStart,
          pageStop,
          itemsLength
        })] : [this.$vuetify.lang.t(this.pageText, pageStart, pageStop, itemsLength)];
      }

      return this.$createElement('div', {
        class: 'v-data-footer__pagination'
      }, children);
    },

    genIcon(click, disabled, label, icon) {
      return this.$createElement(VBtn, {
        props: {
          disabled: disabled || this.disablePagination,
          icon: true,
          text: true
        },
        on: {
          click
        },
        attrs: {
          'aria-label': label
        }
      }, [this.$createElement(VIcon$1, icon)]);
    },

    genIcons() {
      const before = [];
      const after = [];
      before.push(this.genIcon(this.onPreviousPage, this.options.page === 1, this.$vuetify.lang.t('$vuetify.dataFooter.prevPage'), this.$vuetify.rtl ? this.nextIcon : this.prevIcon));
      after.push(this.genIcon(this.onNextPage, this.disableNextPageIcon, this.$vuetify.lang.t('$vuetify.dataFooter.nextPage'), this.$vuetify.rtl ? this.prevIcon : this.nextIcon));

      if (this.showFirstLastPage) {
        before.unshift(this.genIcon(this.onFirstPage, this.options.page === 1, this.$vuetify.lang.t('$vuetify.dataFooter.firstPage'), this.$vuetify.rtl ? this.lastIcon : this.firstIcon));
        after.push(this.genIcon(this.onLastPage, this.options.page >= this.pagination.pageCount || this.options.itemsPerPage === -1, this.$vuetify.lang.t('$vuetify.dataFooter.lastPage'), this.$vuetify.rtl ? this.firstIcon : this.lastIcon));
      }

      return [this.$createElement('div', {
        staticClass: 'v-data-footer__icons-before'
      }, before), this.showCurrentPage && this.$createElement('span', [this.options.page.toString()]), this.$createElement('div', {
        staticClass: 'v-data-footer__icons-after'
      }, after)];
    }

  },

  render() {
    return this.$createElement('div', {
      staticClass: 'v-data-footer'
    }, [this.genItemsPerPageSelect(), this.genPaginationInfo(), this.genIcons()]);
  }

});

// Components
/* @vue/component */

var VDataIterator = Themeable.extend({
  name: 'v-data-iterator',
  props: { ...VData.options.props,
    itemKey: {
      type: String,
      default: 'id'
    },
    value: {
      type: Array,
      default: () => []
    },
    singleSelect: Boolean,
    expanded: {
      type: Array,
      default: () => []
    },
    mobileBreakpoint: {
      type: [Number, String],
      default: 600
    },
    singleExpand: Boolean,
    loading: [Boolean, String],
    noResultsText: {
      type: String,
      default: '$vuetify.dataIterator.noResultsText'
    },
    noDataText: {
      type: String,
      default: '$vuetify.noDataText'
    },
    loadingText: {
      type: String,
      default: '$vuetify.dataIterator.loadingText'
    },
    hideDefaultFooter: Boolean,
    footerProps: Object,
    selectableKey: {
      type: String,
      default: 'isSelectable'
    }
  },
  data: () => ({
    selection: {},
    expansion: {},
    internalCurrentItems: []
  }),
  computed: {
    everyItem() {
      return !!this.selectableItems.length && this.selectableItems.every(i => this.isSelected(i));
    },

    someItems() {
      return this.selectableItems.some(i => this.isSelected(i));
    },

    sanitizedFooterProps() {
      return camelizeObjectKeys(this.footerProps);
    },

    selectableItems() {
      return this.internalCurrentItems.filter(item => this.isSelectable(item));
    },

    isMobile() {
      // Guard against SSR render
      // https://github.com/vuetifyjs/vuetify/issues/7410
      if (this.$vuetify.breakpoint.width === 0) return false;
      return this.$vuetify.breakpoint.width < parseInt(this.mobileBreakpoint, 10);
    }

  },
  watch: {
    value: {
      handler(value) {
        this.selection = value.reduce((selection, item) => {
          selection[getObjectValueByPath(item, this.itemKey)] = item;
          return selection;
        }, {});
      },

      immediate: true
    },

    selection(value, old) {
      if (deepEqual(Object.keys(value), Object.keys(old))) return;
      this.$emit('input', Object.values(value));
    },

    expanded: {
      handler(value) {
        this.expansion = value.reduce((expansion, item) => {
          expansion[getObjectValueByPath(item, this.itemKey)] = true;
          return expansion;
        }, {});
      },

      immediate: true
    },

    expansion(value, old) {
      if (deepEqual(value, old)) return;
      const keys = Object.keys(value).filter(k => value[k]);
      const expanded = !keys.length ? [] : this.items.filter(i => keys.includes(String(getObjectValueByPath(i, this.itemKey))));
      this.$emit('update:expanded', expanded);
    }

  },

  created() {
    const breakingProps = [['disable-initial-sort', 'sort-by'], ['filter', 'custom-filter'], ['pagination', 'options'], ['total-items', 'server-items-length'], ['hide-actions', 'hide-default-footer'], ['rows-per-page-items', 'footer-props.items-per-page-options'], ['rows-per-page-text', 'footer-props.items-per-page-text'], ['prev-icon', 'footer-props.prev-icon'], ['next-icon', 'footer-props.next-icon']];
    /* istanbul ignore next */

    breakingProps.forEach(([original, replacement]) => {
      if (this.$attrs.hasOwnProperty(original)) breaking(original, replacement, this);
    });
    const removedProps = ['expand', 'content-class', 'content-props', 'content-tag'];
    /* istanbul ignore next */

    removedProps.forEach(prop => {
      if (this.$attrs.hasOwnProperty(prop)) removed(prop);
    });
  },

  methods: {
    toggleSelectAll(value) {
      const selection = Object.assign({}, this.selection);

      for (let i = 0; i < this.selectableItems.length; i++) {
        const item = this.selectableItems[i];
        if (!this.isSelectable(item)) continue;
        const key = getObjectValueByPath(item, this.itemKey);
        if (value) selection[key] = item;else delete selection[key];
      }

      this.selection = selection;
      this.$emit('toggle-select-all', {
        items: this.internalCurrentItems,
        value
      });
    },

    isSelectable(item) {
      return getObjectValueByPath(item, this.selectableKey) !== false;
    },

    isSelected(item) {
      return !!this.selection[getObjectValueByPath(item, this.itemKey)] || false;
    },

    select(item, value = true, emit = true) {
      if (!this.isSelectable(item)) return;
      const selection = this.singleSelect ? {} : Object.assign({}, this.selection);
      const key = getObjectValueByPath(item, this.itemKey);
      if (value) selection[key] = item;else delete selection[key];

      if (this.singleSelect && emit) {
        const keys = Object.keys(this.selection);
        const old = keys.length && getObjectValueByPath(this.selection[keys[0]], this.itemKey);
        old && old !== key && this.$emit('item-selected', {
          item: this.selection[old],
          value: false
        });
      }

      this.selection = selection;
      emit && this.$emit('item-selected', {
        item,
        value
      });
    },

    isExpanded(item) {
      return this.expansion[getObjectValueByPath(item, this.itemKey)] || false;
    },

    expand(item, value = true) {
      const expansion = this.singleExpand ? {} : Object.assign({}, this.expansion);
      const key = getObjectValueByPath(item, this.itemKey);
      if (value) expansion[key] = true;else delete expansion[key];
      this.expansion = expansion;
      this.$emit('item-expanded', {
        item,
        value
      });
    },

    createItemProps(item) {
      return {
        item,
        select: v => this.select(item, v),
        isSelected: this.isSelected(item),
        expand: v => this.expand(item, v),
        isExpanded: this.isExpanded(item),
        isMobile: this.isMobile
      };
    },

    genEmptyWrapper(content) {
      return this.$createElement('div', content);
    },

    genEmpty(originalItemsLength, filteredItemsLength) {
      if (originalItemsLength === 0 && this.loading) {
        const loading = this.$slots['loading'] || this.$vuetify.lang.t(this.loadingText);
        return this.genEmptyWrapper(loading);
      } else if (originalItemsLength === 0) {
        const noData = this.$slots['no-data'] || this.$vuetify.lang.t(this.noDataText);
        return this.genEmptyWrapper(noData);
      } else if (filteredItemsLength === 0) {
        const noResults = this.$slots['no-results'] || this.$vuetify.lang.t(this.noResultsText);
        return this.genEmptyWrapper(noResults);
      }

      return null;
    },

    genItems(props) {
      const empty = this.genEmpty(props.originalItemsLength, props.pagination.itemsLength);
      if (empty) return [empty];

      if (this.$scopedSlots.default) {
        return this.$scopedSlots.default({ ...props,
          isSelected: this.isSelected,
          select: this.select,
          isExpanded: this.isExpanded,
          expand: this.expand
        });
      }

      if (this.$scopedSlots.item) {
        return props.items.map(item => this.$scopedSlots.item(this.createItemProps(item)));
      }

      return [];
    },

    genFooter(props) {
      if (this.hideDefaultFooter) return null;
      const data = {
        props: { ...this.sanitizedFooterProps,
          options: props.options,
          pagination: props.pagination
        },
        on: {
          'update:options': value => props.updateOptions(value)
        }
      };
      const scopedSlots = getPrefixedScopedSlots('footer.', this.$scopedSlots);
      return this.$createElement(VDataFooter, {
        scopedSlots,
        ...data
      });
    },

    genDefaultScopedSlot(props) {
      const outerProps = { ...props,
        someItems: this.someItems,
        everyItem: this.everyItem,
        toggleSelectAll: this.toggleSelectAll
      };
      return this.$createElement('div', {
        staticClass: 'v-data-iterator'
      }, [getSlot(this, 'header', outerProps, true), this.genItems(props), this.genFooter(props), getSlot(this, 'footer', outerProps, true)]);
    }

  },

  render() {
    return this.$createElement(VData, {
      props: this.$props,
      on: {
        'update:options': (v, old) => !deepEqual(v, old) && this.$emit('update:options', v),
        'update:page': v => this.$emit('update:page', v),
        'update:items-per-page': v => this.$emit('update:items-per-page', v),
        'update:sort-by': v => this.$emit('update:sort-by', v),
        'update:sort-desc': v => this.$emit('update:sort-desc', v),
        'update:group-by': v => this.$emit('update:group-by', v),
        'update:group-desc': v => this.$emit('update:group-desc', v),
        pagination: (v, old) => !deepEqual(v, old) && this.$emit('pagination', v),
        'current-items': v => {
          this.internalCurrentItems = v;
          this.$emit('current-items', v);
        },
        'page-count': v => this.$emit('page-count', v)
      },
      scopedSlots: {
        default: this.genDefaultScopedSlot
      }
    });
  }

});

/**
 * Removes duplicate `@input` listeners when
 * using v-model with functional components
 *
 * @see https://github.com/vuetifyjs/vuetify/issues/4460
 */
function dedupeModelListeners(data) {
  if (data.model && data.on && data.on.input) {
    if (Array.isArray(data.on.input)) {
      const i = data.on.input.indexOf(data.model.callback);
      if (i > -1) data.on.input.splice(i, 1);
    } else {
      delete data.on.input;
    }
  }
}

function rebuildFunctionalSlots(slots, h) {
  const children = [];

  for (const slot in slots) {
    if (slots.hasOwnProperty(slot)) {
      children.push(h('template', {
        slot
      }, slots[slot]));
    }
  }

  return children;
}

var header = mixins().extend({
  // https://github.com/vuejs/vue/issues/6872
  directives: {
    ripple: Ripple
  },
  props: {
    headers: {
      type: Array,
      required: true
    },
    options: {
      type: Object,
      default: () => ({
        page: 1,
        itemsPerPage: 10,
        sortBy: [],
        sortDesc: [],
        groupBy: [],
        groupDesc: [],
        multiSort: false,
        mustSort: false
      })
    },
    sortIcon: {
      type: String,
      default: '$sort'
    },
    everyItem: Boolean,
    someItems: Boolean,
    showGroupBy: Boolean,
    singleSelect: Boolean,
    disableSort: Boolean
  },
  methods: {
    genSelectAll() {
      const data = {
        props: {
          value: this.everyItem,
          indeterminate: !this.everyItem && this.someItems
        },
        on: {
          input: v => this.$emit('toggle-select-all', v)
        }
      };

      if (this.$scopedSlots['data-table-select']) {
        return this.$scopedSlots['data-table-select'](data);
      }

      return this.$createElement(VSimpleCheckbox, {
        staticClass: 'v-data-table__checkbox',
        ...data
      });
    },

    genSortIcon() {
      return this.$createElement(VIcon$1, {
        staticClass: 'v-data-table-header__icon',
        props: {
          size: 18
        }
      }, [this.sortIcon]);
    }

  }
});

var VDataTableHeaderMobile = mixins(header).extend({
  name: 'v-data-table-header-mobile',
  props: {
    sortByText: {
      type: String,
      default: '$vuetify.dataTable.sortBy'
    }
  },
  methods: {
    genSortChip(props) {
      const children = [props.item.text];
      const sortIndex = this.options.sortBy.findIndex(k => k === props.item.value);
      const beingSorted = sortIndex >= 0;
      const isDesc = this.options.sortDesc[sortIndex];
      children.push(this.$createElement('div', {
        staticClass: 'v-chip__close',
        class: {
          sortable: true,
          active: beingSorted,
          asc: beingSorted && !isDesc,
          desc: beingSorted && isDesc
        }
      }, [this.genSortIcon()]));
      return this.$createElement(VChip, {
        staticClass: 'sortable',
        nativeOn: {
          click: e => {
            e.stopPropagation();
            this.$emit('sort', props.item.value);
          }
        }
      }, children);
    },

    genSortSelect(items) {
      return this.$createElement(VSelect, {
        props: {
          label: this.$vuetify.lang.t(this.sortByText),
          items,
          hideDetails: true,
          multiple: this.options.multiSort,
          value: this.options.multiSort ? this.options.sortBy : this.options.sortBy[0],
          menuProps: {
            closeOnContentClick: true
          }
        },
        on: {
          change: v => this.$emit('sort', v)
        },
        scopedSlots: {
          selection: props => this.genSortChip(props)
        }
      });
    }

  },

  render(h) {
    const children = [];
    const header = this.headers.find(h => h.value === 'data-table-select');

    if (header && !this.singleSelect) {
      children.push(this.$createElement('div', {
        class: ['v-data-table-header-mobile__select', ...wrapInArray(header.class)],
        attrs: {
          width: header.width
        }
      }, [this.genSelectAll()]));
    }

    const sortHeaders = this.headers.filter(h => h.sortable !== false && h.value !== 'data-table-select');

    if (!this.disableSort && sortHeaders.length) {
      children.push(this.genSortSelect(sortHeaders));
    }

    const th = h('th', [h('div', {
      staticClass: 'v-data-table-header-mobile__wrapper'
    }, children)]);
    const tr = h('tr', [th]);
    return h('thead', {
      staticClass: 'v-data-table-header v-data-table-header-mobile'
    }, [tr]);
  }

});

var VDataTableHeaderDesktop = mixins(header).extend({
  name: 'v-data-table-header-desktop',
  methods: {
    genGroupByToggle(header) {
      return this.$createElement('span', {
        on: {
          click: e => {
            e.stopPropagation();
            this.$emit('group', header.value);
          }
        }
      }, ['group']);
    },

    getAria(beingSorted, isDesc) {
      const $t = key => this.$vuetify.lang.t(`$vuetify.dataTable.ariaLabel.${key}`);

      let ariaSort = 'none';
      let ariaLabel = [$t('sortNone'), $t('activateAscending')];

      if (!beingSorted) {
        return {
          ariaSort,
          ariaLabel: ariaLabel.join(' ')
        };
      }

      if (isDesc) {
        ariaSort = 'descending';
        ariaLabel = [$t('sortDescending'), $t(this.options.mustSort ? 'activateAscending' : 'activateNone')];
      } else {
        ariaSort = 'ascending';
        ariaLabel = [$t('sortAscending'), $t('activateDescending')];
      }

      return {
        ariaSort,
        ariaLabel: ariaLabel.join(' ')
      };
    },

    genHeader(header) {
      const data = {
        attrs: {
          role: 'columnheader',
          scope: 'col',
          'aria-label': header.text || ''
        },
        style: {
          width: convertToUnit(header.width),
          minWidth: convertToUnit(header.width)
        },
        class: [`text-${header.align || 'start'}`, ...wrapInArray(header.class), header.divider && 'v-data-table__divider'],
        on: {}
      };
      const children = [];

      if (header.value === 'data-table-select' && !this.singleSelect) {
        return this.$createElement('th', data, [this.genSelectAll()]);
      }

      children.push(this.$scopedSlots[header.value] ? this.$scopedSlots[header.value]({
        header
      }) : this.$createElement('span', [header.text]));

      if (!this.disableSort && (header.sortable || !header.hasOwnProperty('sortable'))) {
        data.on['click'] = () => this.$emit('sort', header.value);

        const sortIndex = this.options.sortBy.findIndex(k => k === header.value);
        const beingSorted = sortIndex >= 0;
        const isDesc = this.options.sortDesc[sortIndex];
        data.class.push('sortable');
        const {
          ariaLabel,
          ariaSort
        } = this.getAria(beingSorted, isDesc);
        data.attrs['aria-label'] += `${header.text ? ': ' : ''}${ariaLabel}`;
        data.attrs['aria-sort'] = ariaSort;

        if (beingSorted) {
          data.class.push('active');
          data.class.push(isDesc ? 'desc' : 'asc');
        }

        if (header.align === 'end') children.unshift(this.genSortIcon());else children.push(this.genSortIcon());

        if (this.options.multiSort && beingSorted) {
          children.push(this.$createElement('span', {
            class: 'v-data-table-header__sort-badge'
          }, [String(sortIndex + 1)]));
        }
      }

      if (this.showGroupBy) children.push(this.genGroupByToggle(header));
      return this.$createElement('th', data, children);
    }

  },

  render() {
    return this.$createElement('thead', {
      staticClass: 'v-data-table-header'
    }, [this.$createElement('tr', this.headers.map(header => this.genHeader(header)))]);
  }

});

var VDataTableHeader = Vue.extend({
  name: 'v-data-table-header',
  functional: true,
  props: {
    mobile: Boolean
  },

  render(h, {
    props,
    data,
    slots
  }) {
    dedupeModelListeners(data);
    const children = rebuildFunctionalSlots(slots(), h);

    if (props.mobile) {
      return h(VDataTableHeaderMobile, data, children);
    } else {
      return h(VDataTableHeaderDesktop, data, children);
    }
  }

});

// Types
var Row = Vue.extend({
  name: 'row',
  functional: true,
  props: {
    headers: Array,
    item: Object,
    rtl: Boolean
  },

  render(h, {
    props,
    slots,
    data
  }) {
    const computedSlots = slots();
    const columns = props.headers.map(header => {
      const children = [];
      const value = getObjectValueByPath(props.item, header.value);
      const slotName = header.value;
      const scopedSlot = data.scopedSlots && data.scopedSlots[slotName];
      const regularSlot = computedSlots[slotName];

      if (scopedSlot) {
        children.push(scopedSlot({
          item: props.item,
          header,
          value
        }));
      } else if (regularSlot) {
        children.push(regularSlot);
      } else {
        children.push(value == null ? value : String(value));
      }

      const textAlign = `text-${header.align || 'start'}`;
      return h('td', {
        class: {
          [textAlign]: true,
          'v-data-table__divider': header.divider
        }
      }, children);
    });
    return h('tr', data, columns);
  }

});

var RowGroup = Vue.extend({
  name: 'row-group',
  functional: true,
  props: {
    value: {
      type: Boolean,
      default: true
    },
    headerClass: {
      type: String,
      default: 'v-row-group__header'
    },
    contentClass: String,
    summaryClass: {
      type: String,
      default: 'v-row-group__summary'
    }
  },

  render(h, {
    slots,
    props
  }) {
    const computedSlots = slots();
    const children = [];

    if (computedSlots['column.header']) {
      children.push(h('tr', {
        staticClass: props.headerClass
      }, computedSlots['column.header']));
    } else if (computedSlots['row.header']) {
      children.push(...computedSlots['row.header']);
    }

    if (computedSlots['row.content'] && props.value) children.push(...computedSlots['row.content']);

    if (computedSlots['column.summary']) {
      children.push(h('tr', {
        staticClass: props.summaryClass
      }, computedSlots['column.summary']));
    } else if (computedSlots['row.summary']) {
      children.push(...computedSlots['row.summary']);
    }

    return children;
  }

});

var VSimpleTable = mixins(Themeable).extend({
  name: 'v-simple-table',
  props: {
    dense: Boolean,
    fixedHeader: Boolean,
    height: [Number, String]
  },
  computed: {
    classes() {
      return {
        'v-data-table--dense': this.dense,
        'v-data-table--fixed-height': !!this.height && !this.fixedHeader,
        'v-data-table--fixed-header': this.fixedHeader,
        ...this.themeClasses
      };
    }

  },
  methods: {
    genWrapper() {
      return this.$slots.wrapper || this.$createElement('div', {
        staticClass: 'v-data-table__wrapper',
        style: {
          height: convertToUnit(this.height)
        }
      }, [this.$createElement('table', this.$slots.default)]);
    }

  },

  render(h) {
    return h('div', {
      staticClass: 'v-data-table',
      class: this.classes
    }, [this.$slots.top, this.genWrapper(), this.$slots.bottom]);
  }

});

var MobileRow = Vue.extend({
  name: 'row',
  functional: true,
  props: {
    headers: Array,
    item: Object,
    rtl: Boolean
  },

  render(h, {
    props,
    slots,
    data
  }) {
    const computedSlots = slots();
    const columns = props.headers.map(header => {
      const classes = {
        'v-data-table__mobile-row': true
      };
      const children = [];
      const value = getObjectValueByPath(props.item, header.value);
      const slotName = header.value;
      const scopedSlot = data.scopedSlots && data.scopedSlots[slotName];
      const regularSlot = computedSlots[slotName];

      if (scopedSlot) {
        children.push(scopedSlot({
          item: props.item,
          header,
          value
        }));
      } else if (regularSlot) {
        children.push(regularSlot);
      } else {
        children.push(value == null ? value : String(value));
      }

      const mobileRowChildren = [h('div', {
        staticClass: 'v-data-table__mobile-row__cell'
      }, children)];

      if (header.value !== 'dataTableSelect') {
        mobileRowChildren.unshift(h('div', {
          staticClass: 'v-data-table__mobile-row__header'
        }, [header.text]));
      }

      return h('td', {
        class: classes
      }, mobileRowChildren);
    });
    return h('tr', { ...data,
      staticClass: 'v-data-table__mobile-table-row'
    }, columns);
  }

});

function filterFn(item, search, filter) {
  return header => {
    const value = getObjectValueByPath(item, header.value);
    return header.filter ? header.filter(value, search, item) : filter(value, search, item);
  };
}

function searchTableItems(items, search, headersWithCustomFilters, headersWithoutCustomFilters, customFilter) {
  search = typeof search === 'string' ? search.trim() : null; // If the `search` property is empty and there are no custom filters in use, there is nothing to do.

  if (!(search && headersWithoutCustomFilters.length) && !headersWithCustomFilters.length) return items;
  return items.filter(item => {
    // Headers with custom filters are evaluated whether or not a search term has been provided.
    if (headersWithCustomFilters.length && headersWithCustomFilters.every(filterFn(item, search, defaultFilter))) {
      return true;
    } // Otherwise, the `search` property is used to filter columns without a custom filter.


    return search && headersWithoutCustomFilters.some(filterFn(item, search, customFilter));
  });
}
/* @vue/component */


var VDataTable = VDataIterator.extend({
  name: 'v-data-table',
  // https://github.com/vuejs/vue/issues/6872
  directives: {
    ripple: Ripple
  },
  props: {
    headers: {
      type: Array,
      default: () => []
    },
    showSelect: Boolean,
    showExpand: Boolean,
    showGroupBy: Boolean,
    // TODO: Fix
    // virtualRows: Boolean,
    height: [Number, String],
    hideDefaultHeader: Boolean,
    caption: String,
    dense: Boolean,
    headerProps: Object,
    calculateWidths: Boolean,
    fixedHeader: Boolean,
    headersLength: Number,
    expandIcon: {
      type: String,
      default: '$expand'
    },
    customFilter: {
      type: Function,
      default: defaultFilter
    }
  },

  data() {
    return {
      internalGroupBy: [],
      openCache: {},
      widths: []
    };
  },

  computed: {
    computedHeaders() {
      if (!this.headers) return [];
      const headers = this.headers.filter(h => h.value === undefined || !this.internalGroupBy.find(v => v === h.value));
      const defaultHeader = {
        text: '',
        sortable: false,
        width: '1px'
      };

      if (this.showSelect) {
        const index = headers.findIndex(h => h.value === 'data-table-select');
        if (index < 0) headers.unshift({ ...defaultHeader,
          value: 'data-table-select'
        });else headers.splice(index, 1, { ...defaultHeader,
          ...headers[index]
        });
      }

      if (this.showExpand) {
        const index = headers.findIndex(h => h.value === 'data-table-expand');
        if (index < 0) headers.unshift({ ...defaultHeader,
          value: 'data-table-expand'
        });else headers.splice(index, 1, { ...defaultHeader,
          ...headers[index]
        });
      }

      return headers;
    },

    colspanAttrs() {
      return this.isMobile ? undefined : {
        colspan: this.headersLength || this.computedHeaders.length
      };
    },

    columnSorters() {
      return this.computedHeaders.reduce((acc, header) => {
        if (header.sort) acc[header.value] = header.sort;
        return acc;
      }, {});
    },

    headersWithCustomFilters() {
      return this.headers.filter(header => header.filter && (!header.hasOwnProperty('filterable') || header.filterable === true));
    },

    headersWithoutCustomFilters() {
      return this.headers.filter(header => !header.filter && (!header.hasOwnProperty('filterable') || header.filterable === true));
    },

    sanitizedHeaderProps() {
      return camelizeObjectKeys(this.headerProps);
    },

    computedItemsPerPage() {
      const itemsPerPage = this.options && this.options.itemsPerPage ? this.options.itemsPerPage : this.itemsPerPage;
      const itemsPerPageOptions = this.sanitizedFooterProps.itemsPerPageOptions;

      if (itemsPerPageOptions && !itemsPerPageOptions.find(item => typeof item === 'number' ? item === itemsPerPage : item.value === itemsPerPage)) {
        const firstOption = itemsPerPageOptions[0];
        return typeof firstOption === 'object' ? firstOption.value : firstOption;
      }

      return itemsPerPage;
    }

  },

  created() {
    const breakingProps = [['sort-icon', 'header-props.sort-icon'], ['hide-headers', 'hide-default-header'], ['select-all', 'show-select']];
    /* istanbul ignore next */

    breakingProps.forEach(([original, replacement]) => {
      if (this.$attrs.hasOwnProperty(original)) breaking(original, replacement, this);
    });
  },

  mounted() {
    // if ((!this.sortBy || !this.sortBy.length) && (!this.options.sortBy || !this.options.sortBy.length)) {
    //   const firstSortable = this.headers.find(h => !('sortable' in h) || !!h.sortable)
    //   if (firstSortable) this.updateOptions({ sortBy: [firstSortable.value], sortDesc: [false] })
    // }
    if (this.calculateWidths) {
      window.addEventListener('resize', this.calcWidths);
      this.calcWidths();
    }
  },

  beforeDestroy() {
    if (this.calculateWidths) {
      window.removeEventListener('resize', this.calcWidths);
    }
  },

  methods: {
    calcWidths() {
      this.widths = Array.from(this.$el.querySelectorAll('th')).map(e => e.clientWidth);
    },

    customFilterWithColumns(items, search) {
      return searchTableItems(items, search, this.headersWithCustomFilters, this.headersWithoutCustomFilters, this.customFilter);
    },

    customSortWithHeaders(items, sortBy, sortDesc, locale) {
      return this.customSort(items, sortBy, sortDesc, locale, this.columnSorters);
    },

    createItemProps(item) {
      const props = VDataIterator.options.methods.createItemProps.call(this, item);
      return Object.assign(props, {
        headers: this.computedHeaders
      });
    },

    genCaption(props) {
      if (this.caption) return [this.$createElement('caption', [this.caption])];
      return getSlot(this, 'caption', props, true);
    },

    genColgroup(props) {
      return this.$createElement('colgroup', this.computedHeaders.map(header => {
        return this.$createElement('col', {
          class: {
            divider: header.divider
          }
        });
      }));
    },

    genLoading() {
      const progress = this.$slots['progress'] ? this.$slots.progress : this.$createElement(VProgressLinear, {
        props: {
          color: this.loading === true ? 'primary' : this.loading,
          height: 2,
          indeterminate: true
        }
      });
      const th = this.$createElement('th', {
        staticClass: 'column',
        attrs: this.colspanAttrs
      }, [progress]);
      const tr = this.$createElement('tr', {
        staticClass: 'v-data-table__progress'
      }, [th]);
      return this.$createElement('thead', [tr]);
    },

    genHeaders(props) {
      const data = {
        props: { ...this.sanitizedHeaderProps,
          headers: this.computedHeaders,
          options: props.options,
          mobile: this.isMobile,
          showGroupBy: this.showGroupBy,
          someItems: this.someItems,
          everyItem: this.everyItem,
          singleSelect: this.singleSelect,
          disableSort: this.disableSort
        },
        on: {
          sort: props.sort,
          group: props.group,
          'toggle-select-all': this.toggleSelectAll
        }
      };
      const children = [getSlot(this, 'header', data)];

      if (!this.hideDefaultHeader) {
        const scopedSlots = getPrefixedScopedSlots('header.', this.$scopedSlots);
        children.push(this.$createElement(VDataTableHeader, { ...data,
          scopedSlots
        }));
      }

      if (this.loading) children.push(this.genLoading());
      return children;
    },

    genEmptyWrapper(content) {
      return this.$createElement('tr', {
        staticClass: 'v-data-table__empty-wrapper'
      }, [this.$createElement('td', {
        attrs: this.colspanAttrs
      }, content)]);
    },

    genItems(items, props) {
      const empty = this.genEmpty(props.originalItemsLength, props.pagination.itemsLength);
      if (empty) return [empty];
      return props.groupedItems ? this.genGroupedRows(props.groupedItems, props) : this.genRows(items, props);
    },

    genGroupedRows(groupedItems, props) {
      return groupedItems.map(group => {
        if (!this.openCache.hasOwnProperty(group.name)) this.$set(this.openCache, group.name, true);

        if (this.$scopedSlots.group) {
          return this.$scopedSlots.group({
            group: group.name,
            options: props.options,
            items: group.items,
            headers: this.computedHeaders
          });
        } else {
          return this.genDefaultGroupedRow(group.name, group.items, props);
        }
      });
    },

    genDefaultGroupedRow(group, items, props) {
      const isOpen = !!this.openCache[group];
      const children = [this.$createElement('template', {
        slot: 'row.content'
      }, this.genRows(items, props))];

      const toggleFn = () => this.$set(this.openCache, group, !this.openCache[group]);

      const removeFn = () => props.updateOptions({
        groupBy: [],
        groupDesc: []
      });

      if (this.$scopedSlots['group.header']) {
        children.unshift(this.$createElement('template', {
          slot: 'column.header'
        }, [this.$scopedSlots['group.header']({
          group,
          groupBy: props.options.groupBy,
          items,
          headers: this.computedHeaders,
          isOpen,
          toggle: toggleFn,
          remove: removeFn
        })]));
      } else {
        const toggle = this.$createElement(VBtn, {
          staticClass: 'ma-0',
          props: {
            icon: true,
            small: true
          },
          on: {
            click: toggleFn
          }
        }, [this.$createElement(VIcon$1, [isOpen ? '$minus' : '$plus'])]);
        const remove = this.$createElement(VBtn, {
          staticClass: 'ma-0',
          props: {
            icon: true,
            small: true
          },
          on: {
            click: removeFn
          }
        }, [this.$createElement(VIcon$1, ['$close'])]);
        const column = this.$createElement('td', {
          staticClass: 'text-start',
          attrs: this.colspanAttrs
        }, [toggle, `${props.options.groupBy[0]}: ${group}`, remove]);
        children.unshift(this.$createElement('template', {
          slot: 'column.header'
        }, [column]));
      }

      if (this.$scopedSlots['group.summary']) {
        children.push(this.$createElement('template', {
          slot: 'column.summary'
        }, [this.$scopedSlots['group.summary']({
          group,
          groupBy: props.options.groupBy,
          items,
          headers: this.computedHeaders,
          isOpen,
          toggle: toggleFn
        })]));
      }

      return this.$createElement(RowGroup, {
        key: group,
        props: {
          value: isOpen
        }
      }, children);
    },

    genRows(items, props) {
      return this.$scopedSlots.item ? this.genScopedRows(items, props) : this.genDefaultRows(items, props);
    },

    genScopedRows(items, props) {
      const rows = [];

      for (let i = 0; i < items.length; i++) {
        const item = items[i];
        rows.push(this.$scopedSlots.item({ ...this.createItemProps(item),
          index: i
        }));

        if (this.isExpanded(item)) {
          rows.push(this.$scopedSlots['expanded-item']({
            item,
            headers: this.computedHeaders
          }));
        }
      }

      return rows;
    },

    genDefaultRows(items, props) {
      return this.$scopedSlots['expanded-item'] ? items.map(item => this.genDefaultExpandedRow(item)) : items.map(item => this.genDefaultSimpleRow(item));
    },

    genDefaultExpandedRow(item) {
      const isExpanded = this.isExpanded(item);
      const classes = {
        'v-data-table__expanded v-data-table__expanded__row': isExpanded
      };
      const headerRow = this.genDefaultSimpleRow(item, classes);
      const expandedRow = this.$createElement('tr', {
        staticClass: 'v-data-table__expanded v-data-table__expanded__content'
      }, [this.$scopedSlots['expanded-item']({
        item,
        headers: this.computedHeaders
      })]);
      return this.$createElement(RowGroup, {
        props: {
          value: isExpanded
        }
      }, [this.$createElement('template', {
        slot: 'row.header'
      }, [headerRow]), this.$createElement('template', {
        slot: 'row.content'
      }, [expandedRow])]);
    },

    genDefaultSimpleRow(item, classes = {}) {
      const scopedSlots = getPrefixedScopedSlots('item.', this.$scopedSlots);
      const data = this.createItemProps(item);

      if (this.showSelect) {
        const slot = scopedSlots['data-table-select'];
        scopedSlots['data-table-select'] = slot ? () => slot(data) : () => this.$createElement(VSimpleCheckbox, {
          staticClass: 'v-data-table__checkbox',
          props: {
            value: data.isSelected,
            disabled: !this.isSelectable(item)
          },
          on: {
            input: val => data.select(val)
          }
        });
      }

      if (this.showExpand) {
        const slot = scopedSlots['data-table-expand'];
        scopedSlots['data-table-expand'] = slot ? () => slot(data) : () => this.$createElement(VIcon$1, {
          staticClass: 'v-data-table__expand-icon',
          class: {
            'v-data-table__expand-icon--active': data.isExpanded
          },
          on: {
            click: e => {
              e.stopPropagation();
              data.expand(!data.isExpanded);
            }
          }
        }, [this.expandIcon]);
      }

      return this.$createElement(this.isMobile ? MobileRow : Row, {
        key: getObjectValueByPath(item, this.itemKey),
        class: { ...classes,
          'v-data-table__selected': data.isSelected
        },
        props: {
          headers: this.computedHeaders,
          item,
          rtl: this.$vuetify.rtl
        },
        scopedSlots,
        on: {
          // TODO: first argument should be the data object
          // but this is a breaking change so it's for v3
          click: () => this.$emit('click:row', item, data)
        }
      });
    },

    genBody(props) {
      const data = { ...props,
        expand: this.expand,
        headers: this.computedHeaders,
        isExpanded: this.isExpanded,
        isMobile: this.isMobile,
        isSelected: this.isSelected,
        select: this.select
      };

      if (this.$scopedSlots.body) {
        return this.$scopedSlots.body(data);
      }

      return this.$createElement('tbody', [getSlot(this, 'body.prepend', data, true), this.genItems(props.items, props), getSlot(this, 'body.append', data, true)]);
    },

    genFooters(props) {
      const data = {
        props: {
          options: props.options,
          pagination: props.pagination,
          itemsPerPageText: '$vuetify.dataTable.itemsPerPageText',
          ...this.sanitizedFooterProps
        },
        on: {
          'update:options': value => props.updateOptions(value)
        },
        widths: this.widths,
        headers: this.computedHeaders
      };
      const children = [getSlot(this, 'footer', data, true)];

      if (!this.hideDefaultFooter) {
        children.push(this.$createElement(VDataFooter, { ...data,
          scopedSlots: getPrefixedScopedSlots('footer.', this.$scopedSlots)
        }));
      }

      return children;
    },

    genDefaultScopedSlot(props) {
      const simpleProps = {
        height: this.height,
        fixedHeader: this.fixedHeader,
        dense: this.dense
      }; // if (this.virtualRows) {
      //   return this.$createElement(VVirtualTable, {
      //     props: Object.assign(simpleProps, {
      //       items: props.items,
      //       height: this.height,
      //       rowHeight: this.dense ? 24 : 48,
      //       headerHeight: this.dense ? 32 : 48,
      //       // TODO: expose rest of props from virtual table?
      //     }),
      //     scopedSlots: {
      //       items: ({ items }) => this.genItems(items, props) as any,
      //     },
      //   }, [
      //     this.proxySlot('body.before', [this.genCaption(props), this.genHeaders(props)]),
      //     this.proxySlot('bottom', this.genFooters(props)),
      //   ])
      // }

      return this.$createElement(VSimpleTable, {
        props: simpleProps
      }, [this.proxySlot('top', getSlot(this, 'top', props, true)), this.genCaption(props), this.genColgroup(props), this.genHeaders(props), this.genBody(props), this.proxySlot('bottom', this.genFooters(props))]);
    },

    proxySlot(slot, content) {
      return this.$createElement('template', {
        slot
      }, content);
    }

  },

  render() {
    return this.$createElement(VData, {
      props: { ...this.$props,
        customFilter: this.customFilterWithColumns,
        customSort: this.customSortWithHeaders,
        itemsPerPage: this.computedItemsPerPage
      },
      on: {
        'update:options': (v, old) => {
          this.internalGroupBy = v.groupBy || [];
          !deepEqual(v, old) && this.$emit('update:options', v);
        },
        'update:page': v => this.$emit('update:page', v),
        'update:items-per-page': v => this.$emit('update:items-per-page', v),
        'update:sort-by': v => this.$emit('update:sort-by', v),
        'update:sort-desc': v => this.$emit('update:sort-desc', v),
        'update:group-by': v => this.$emit('update:group-by', v),
        'update:group-desc': v => this.$emit('update:group-desc', v),
        pagination: (v, old) => !deepEqual(v, old) && this.$emit('pagination', v),
        'current-items': v => {
          this.internalCurrentItems = v;
          this.$emit('current-items', v);
        },
        'page-count': v => this.$emit('page-count', v)
      },
      scopedSlots: {
        default: this.genDefaultScopedSlot
      }
    });
  }

});

const breakpoints = ['sm', 'md', 'lg', 'xl'];

const breakpointProps = (() => {
  return breakpoints.reduce((props, val) => {
    props[val] = {
      type: [Boolean, String, Number],
      default: false
    };
    return props;
  }, {});
})();

const offsetProps = (() => {
  return breakpoints.reduce((props, val) => {
    props['offset' + upperFirst(val)] = {
      type: [String, Number],
      default: null
    };
    return props;
  }, {});
})();

const orderProps = (() => {
  return breakpoints.reduce((props, val) => {
    props['order' + upperFirst(val)] = {
      type: [String, Number],
      default: null
    };
    return props;
  }, {});
})();

const propMap = {
  col: Object.keys(breakpointProps),
  offset: Object.keys(offsetProps),
  order: Object.keys(orderProps)
};

function breakpointClass(type, prop, val) {
  let className = type;

  if (val == null || val === false) {
    return undefined;
  }

  if (prop) {
    const breakpoint = prop.replace(type, '');
    className += `-${breakpoint}`;
  } // Handling the boolean style prop when accepting [Boolean, String, Number]
  // means Vue will not convert <v-col sm></v-col> to sm: true for us.
  // Since the default is false, an empty string indicates the prop's presence.


  if (type === 'col' && (val === '' || val === true)) {
    // .col-md
    return className.toLowerCase();
  } // .order-md-6


  className += `-${val}`;
  return className.toLowerCase();
}

const cache = new Map();
var VCol = Vue.extend({
  name: 'v-col',
  functional: true,
  props: {
    cols: {
      type: [Boolean, String, Number],
      default: false
    },
    ...breakpointProps,
    offset: {
      type: [String, Number],
      default: null
    },
    ...offsetProps,
    order: {
      type: [String, Number],
      default: null
    },
    ...orderProps,
    alignSelf: {
      type: String,
      default: null,
      validator: str => ['auto', 'start', 'end', 'center', 'baseline', 'stretch'].includes(str)
    },
    tag: {
      type: String,
      default: 'div'
    }
  },

  render(h, {
    props,
    data,
    children,
    parent
  }) {
    // Super-fast memoization based on props, 5x faster than JSON.stringify
    let cacheKey = '';

    for (const prop in props) {
      cacheKey += String(props[prop]);
    }

    let classList = cache.get(cacheKey);

    if (!classList) {
      classList = []; // Loop through `col`, `offset`, `order` breakpoint props

      let type;

      for (type in propMap) {
        propMap[type].forEach(prop => {
          const value = props[prop];
          const className = breakpointClass(type, prop, value);
          if (className) classList.push(className);
        });
      }

      const hasColClasses = classList.some(className => className.startsWith('col-'));
      classList.push({
        // Default to .col if no other col-{bp}-* classes generated nor `cols` specified.
        col: !hasColClasses || !props.cols,
        [`col-${props.cols}`]: props.cols,
        [`offset-${props.offset}`]: props.offset,
        [`order-${props.order}`]: props.order,
        [`align-self-${props.alignSelf}`]: props.alignSelf
      });
      cache.set(cacheKey, classList);
    }

    return h(props.tag, mergeData(data, {
      class: classList
    }), children);
  }

});

const breakpoints$1 = ['sm', 'md', 'lg', 'xl'];
const ALIGNMENT = ['start', 'end', 'center'];

function makeProps(prefix, def) {
  return breakpoints$1.reduce((props, val) => {
    props[prefix + upperFirst(val)] = def();
    return props;
  }, {});
}

const alignValidator = str => [...ALIGNMENT, 'baseline', 'stretch'].includes(str);

const alignProps = makeProps('align', () => ({
  type: String,
  default: null,
  validator: alignValidator
}));

const justifyValidator = str => [...ALIGNMENT, 'space-between', 'space-around'].includes(str);

const justifyProps = makeProps('justify', () => ({
  type: String,
  default: null,
  validator: justifyValidator
}));

const alignContentValidator = str => [...ALIGNMENT, 'space-between', 'space-around', 'stretch'].includes(str);

const alignContentProps = makeProps('alignContent', () => ({
  type: String,
  default: null,
  validator: alignContentValidator
}));
const propMap$1 = {
  align: Object.keys(alignProps),
  justify: Object.keys(justifyProps),
  alignContent: Object.keys(alignContentProps)
};
const classMap = {
  align: 'align',
  justify: 'justify',
  alignContent: 'align-content'
};

function breakpointClass$1(type, prop, val) {
  let className = classMap[type];

  if (val == null) {
    return undefined;
  }

  if (prop) {
    // alignSm -> Sm
    const breakpoint = prop.replace(type, '');
    className += `-${breakpoint}`;
  } // .align-items-sm-center


  className += `-${val}`;
  return className.toLowerCase();
}

const cache$1 = new Map();
var VRow = Vue.extend({
  name: 'v-row',
  functional: true,
  props: {
    tag: {
      type: String,
      default: 'div'
    },
    dense: Boolean,
    noGutters: Boolean,
    align: {
      type: String,
      default: null,
      validator: alignValidator
    },
    ...alignProps,
    justify: {
      type: String,
      default: null,
      validator: justifyValidator
    },
    ...justifyProps,
    alignContent: {
      type: String,
      default: null,
      validator: alignContentValidator
    },
    ...alignContentProps
  },

  render(h, {
    props,
    data,
    children
  }) {
    // Super-fast memoization based on props, 5x faster than JSON.stringify
    let cacheKey = '';

    for (const prop in props) {
      cacheKey += String(props[prop]);
    }

    let classList = cache$1.get(cacheKey);

    if (!classList) {
      classList = []; // Loop through `align`, `justify`, `alignContent` breakpoint props

      let type;

      for (type in propMap$1) {
        propMap$1[type].forEach(prop => {
          const value = props[prop];
          const className = breakpointClass$1(type, prop, value);
          if (className) classList.push(className);
        });
      }

      classList.push({
        'no-gutters': props.noGutters,
        'row--dense': props.dense,
        [`align-${props.align}`]: props.align,
        [`justify-${props.justify}`]: props.justify,
        [`align-content-${props.alignContent}`]: props.alignContent
      });
      cache$1.set(cacheKey, classList);
    }

    return h(props.tag, mergeData(data, {
      staticClass: 'row',
      class: classList
    }), children);
  }

});

var VSpacer = createSimpleFunctional('spacer', 'div', 'v-spacer');

// Mixins
var VHover = mixins(Delayable, Toggleable
/* @vue/component */
).extend({
  name: 'v-hover',
  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    value: {
      type: Boolean,
      default: undefined
    }
  },
  methods: {
    onMouseEnter() {
      this.runDelay('open');
    },

    onMouseLeave() {
      this.runDelay('close');
    }

  },

  render() {
    if (!this.$scopedSlots.default && this.value === undefined) {
      consoleWarn('v-hover is missing a default scopedSlot or bound value', this);
      return null;
    }

    let element;
    /* istanbul ignore else */

    if (this.$scopedSlots.default) {
      element = this.$scopedSlots.default({
        hover: this.isActive
      });
    }

    if (Array.isArray(element) && element.length === 1) {
      element = element[0];
    }

    if (!element || Array.isArray(element) || !element.tag) {
      consoleWarn('v-hover should only contain a single element', this);
      return element;
    }

    if (!this.disabled) {
      element.data = element.data || {};

      this._g(element.data, {
        mouseenter: this.onMouseEnter,
        mouseleave: this.onMouseLeave
      });
    }

    return element;
  }

});

// Styles
const baseMixins$9 = mixins(applicationable('left', ['isActive', 'isMobile', 'miniVariant', 'expandOnHover', 'permanent', 'right', 'temporary', 'width']), Colorable, Dependent, Overlayable, SSRBootable, Themeable);
/* @vue/component */

var VNavigationDrawer = baseMixins$9.extend({
  name: 'v-navigation-drawer',

  provide() {
    return {
      isInNav: this.tag === 'nav'
    };
  },

  directives: {
    ClickOutside,
    Resize,
    Touch
  },
  props: {
    bottom: Boolean,
    clipped: Boolean,
    disableResizeWatcher: Boolean,
    disableRouteWatcher: Boolean,
    expandOnHover: Boolean,
    floating: Boolean,
    height: {
      type: [Number, String],

      default() {
        return this.app ? '100vh' : '100%';
      }

    },
    miniVariant: Boolean,
    miniVariantWidth: {
      type: [Number, String],
      default: 56
    },
    mobileBreakPoint: {
      type: [Number, String],
      default: 1264
    },
    permanent: Boolean,
    right: Boolean,
    src: {
      type: [String, Object],
      default: ''
    },
    stateless: Boolean,
    tag: {
      type: String,

      default() {
        return this.app ? 'nav' : 'aside';
      }

    },
    temporary: Boolean,
    touchless: Boolean,
    width: {
      type: [Number, String],
      default: 256
    },
    value: null
  },
  data: () => ({
    isMouseover: false,
    touchArea: {
      left: 0,
      right: 0
    },
    stackMinZIndex: 6
  }),
  computed: {
    /**
     * Used for setting an app value from a dynamic
     * property. Called from applicationable.js
     */
    applicationProperty() {
      return this.right ? 'right' : 'left';
    },

    classes() {
      return {
        'v-navigation-drawer': true,
        'v-navigation-drawer--absolute': this.absolute,
        'v-navigation-drawer--bottom': this.bottom,
        'v-navigation-drawer--clipped': this.clipped,
        'v-navigation-drawer--close': !this.isActive,
        'v-navigation-drawer--fixed': !this.absolute && (this.app || this.fixed),
        'v-navigation-drawer--floating': this.floating,
        'v-navigation-drawer--is-mobile': this.isMobile,
        'v-navigation-drawer--is-mouseover': this.isMouseover,
        'v-navigation-drawer--mini-variant': this.isMiniVariant,
        'v-navigation-drawer--custom-mini-variant': Number(this.miniVariantWidth) !== 56,
        'v-navigation-drawer--open': this.isActive,
        'v-navigation-drawer--open-on-hover': this.expandOnHover,
        'v-navigation-drawer--right': this.right,
        'v-navigation-drawer--temporary': this.temporary,
        ...this.themeClasses
      };
    },

    computedMaxHeight() {
      if (!this.hasApp) return null;
      const computedMaxHeight = this.$vuetify.application.bottom + this.$vuetify.application.footer + this.$vuetify.application.bar;
      if (!this.clipped) return computedMaxHeight;
      return computedMaxHeight + this.$vuetify.application.top;
    },

    computedTop() {
      if (!this.hasApp) return 0;
      let computedTop = this.$vuetify.application.bar;
      computedTop += this.clipped ? this.$vuetify.application.top : 0;
      return computedTop;
    },

    computedTransform() {
      if (this.isActive) return 0;
      if (this.isBottom) return 100;
      return this.right ? 100 : -100;
    },

    computedWidth() {
      return this.isMiniVariant ? this.miniVariantWidth : this.width;
    },

    hasApp() {
      return this.app && !this.isMobile && !this.temporary;
    },

    isBottom() {
      return this.bottom && this.isMobile;
    },

    isMiniVariant() {
      return !this.expandOnHover && this.miniVariant || this.expandOnHover && !this.isMouseover;
    },

    isMobile() {
      return !this.stateless && !this.permanent && this.$vuetify.breakpoint.width < parseInt(this.mobileBreakPoint, 10);
    },

    reactsToClick() {
      return !this.stateless && !this.permanent && (this.isMobile || this.temporary);
    },

    reactsToMobile() {
      return this.app && !this.disableResizeWatcher && !this.permanent && !this.stateless && !this.temporary;
    },

    reactsToResize() {
      return !this.disableResizeWatcher && !this.stateless;
    },

    reactsToRoute() {
      return !this.disableRouteWatcher && !this.stateless && (this.temporary || this.isMobile);
    },

    showOverlay() {
      return !this.hideOverlay && this.isActive && (this.isMobile || this.temporary);
    },

    styles() {
      const translate = this.isBottom ? 'translateY' : 'translateX';
      const styles = {
        height: convertToUnit(this.height),
        top: !this.isBottom ? convertToUnit(this.computedTop) : 'auto',
        maxHeight: this.computedMaxHeight != null ? `calc(100% - ${convertToUnit(this.computedMaxHeight)})` : undefined,
        transform: `${translate}(${convertToUnit(this.computedTransform, '%')})`,
        width: convertToUnit(this.computedWidth)
      };
      return styles;
    }

  },
  watch: {
    $route: 'onRouteChange',

    isActive(val) {
      this.$emit('input', val);
    },

    /**
     * When mobile changes, adjust the active state
     * only when there has been a previous value
     */
    isMobile(val, prev) {
      !val && this.isActive && !this.temporary && this.removeOverlay();
      if (prev == null || !this.reactsToResize || !this.reactsToMobile) return;
      this.isActive = !val;
    },

    permanent(val) {
      // If enabling prop enable the drawer
      if (val) this.isActive = true;
    },

    showOverlay(val) {
      if (val) this.genOverlay();else this.removeOverlay();
    },

    value(val) {
      if (this.permanent) return;

      if (val == null) {
        this.init();
        return;
      }

      if (val !== this.isActive) this.isActive = val;
    },

    expandOnHover: 'updateMiniVariant',

    isMouseover(val) {
      this.updateMiniVariant(!val);
    }

  },

  beforeMount() {
    this.init();
  },

  methods: {
    calculateTouchArea() {
      const parent = this.$el.parentNode;
      if (!parent) return;
      const parentRect = parent.getBoundingClientRect();
      this.touchArea = {
        left: parentRect.left + 50,
        right: parentRect.right - 50
      };
    },

    closeConditional() {
      return this.isActive && !this._isDestroyed && this.reactsToClick;
    },

    genAppend() {
      return this.genPosition('append');
    },

    genBackground() {
      const props = {
        height: '100%',
        width: '100%',
        src: this.src
      };
      const image = this.$scopedSlots.img ? this.$scopedSlots.img(props) : this.$createElement(VImg, {
        props
      });
      return this.$createElement('div', {
        staticClass: 'v-navigation-drawer__image'
      }, [image]);
    },

    genDirectives() {
      const directives = [{
        name: 'click-outside',
        value: () => this.isActive = false,
        args: {
          closeConditional: this.closeConditional,
          include: this.getOpenDependentElements
        }
      }];

      if (!this.touchless && !this.stateless) {
        directives.push({
          name: 'touch',
          value: {
            parent: true,
            left: this.swipeLeft,
            right: this.swipeRight
          }
        });
      }

      return directives;
    },

    genListeners() {
      const on = {
        transitionend: e => {
          if (e.target !== e.currentTarget) return;
          this.$emit('transitionend', e); // IE11 does not support new Event('resize')

          const resizeEvent = document.createEvent('UIEvents');
          resizeEvent.initUIEvent('resize', true, false, window, 0);
          window.dispatchEvent(resizeEvent);
        }
      };

      if (this.miniVariant) {
        on.click = () => this.$emit('update:mini-variant', false);
      }

      if (this.expandOnHover) {
        on.mouseenter = () => this.isMouseover = true;

        on.mouseleave = () => this.isMouseover = false;
      }

      return on;
    },

    genPosition(name) {
      const slot = getSlot(this, name);
      if (!slot) return slot;
      return this.$createElement('div', {
        staticClass: `v-navigation-drawer__${name}`
      }, slot);
    },

    genPrepend() {
      return this.genPosition('prepend');
    },

    genContent() {
      return this.$createElement('div', {
        staticClass: 'v-navigation-drawer__content'
      }, this.$slots.default);
    },

    genBorder() {
      return this.$createElement('div', {
        staticClass: 'v-navigation-drawer__border'
      });
    },

    init() {
      if (this.permanent) {
        this.isActive = true;
      } else if (this.stateless || this.value != null) {
        this.isActive = this.value;
      } else if (!this.temporary) {
        this.isActive = !this.isMobile;
      }
    },

    onRouteChange() {
      if (this.reactsToRoute && this.closeConditional()) {
        this.isActive = false;
      }
    },

    swipeLeft(e) {
      if (this.isActive && this.right) return;
      this.calculateTouchArea();
      if (Math.abs(e.touchendX - e.touchstartX) < 100) return;
      if (this.right && e.touchstartX >= this.touchArea.right) this.isActive = true;else if (!this.right && this.isActive) this.isActive = false;
    },

    swipeRight(e) {
      if (this.isActive && !this.right) return;
      this.calculateTouchArea();
      if (Math.abs(e.touchendX - e.touchstartX) < 100) return;
      if (!this.right && e.touchstartX <= this.touchArea.left) this.isActive = true;else if (this.right && this.isActive) this.isActive = false;
    },

    /**
     * Update the application layout
     */
    updateApplication() {
      if (!this.isActive || this.isMobile || this.temporary || !this.$el) return 0;
      const width = Number(this.computedWidth);
      return isNaN(width) ? this.$el.clientWidth : width;
    },

    updateMiniVariant(val) {
      if (this.miniVariant !== val) this.$emit('update:mini-variant', val);
    }

  },

  render(h) {
    const children = [this.genPrepend(), this.genContent(), this.genAppend(), this.genBorder()];
    if (this.src || getSlot(this, 'img')) children.unshift(this.genBackground());
    return h(this.tag, this.setBackgroundColor(this.color, {
      class: this.classes,
      style: this.styles,
      directives: this.genDirectives(),
      on: this.genListeners()
    }), children);
  }

});

/* @vue/component */

var VTooltip = mixins(Colorable, Delayable, Dependent, Detachable, Menuable, Toggleable).extend({
  name: 'v-tooltip',
  props: {
    closeDelay: {
      type: [Number, String],
      default: 0
    },
    disabled: Boolean,
    fixed: {
      type: Boolean,
      default: true
    },
    openDelay: {
      type: [Number, String],
      default: 0
    },
    openOnHover: {
      type: Boolean,
      default: true
    },
    tag: {
      type: String,
      default: 'span'
    },
    transition: String,
    zIndex: {
      default: null
    }
  },
  data: () => ({
    calculatedMinWidth: 0,
    closeDependents: false
  }),
  computed: {
    calculatedLeft() {
      const {
        activator,
        content
      } = this.dimensions;
      const unknown = !this.bottom && !this.left && !this.top && !this.right;
      const activatorLeft = this.attach !== false ? activator.offsetLeft : activator.left;
      let left = 0;

      if (this.top || this.bottom || unknown) {
        left = activatorLeft + activator.width / 2 - content.width / 2;
      } else if (this.left || this.right) {
        left = activatorLeft + (this.right ? activator.width : -content.width) + (this.right ? 10 : -10);
      }

      if (this.nudgeLeft) left -= parseInt(this.nudgeLeft);
      if (this.nudgeRight) left += parseInt(this.nudgeRight);
      return `${this.calcXOverflow(left, this.dimensions.content.width)}px`;
    },

    calculatedTop() {
      const {
        activator,
        content
      } = this.dimensions;
      const activatorTop = this.attach !== false ? activator.offsetTop : activator.top;
      let top = 0;

      if (this.top || this.bottom) {
        top = activatorTop + (this.bottom ? activator.height : -content.height) + (this.bottom ? 10 : -10);
      } else if (this.left || this.right) {
        top = activatorTop + activator.height / 2 - content.height / 2;
      }

      if (this.nudgeTop) top -= parseInt(this.nudgeTop);
      if (this.nudgeBottom) top += parseInt(this.nudgeBottom);
      return `${this.calcYOverflow(top + this.pageYOffset)}px`;
    },

    classes() {
      return {
        'v-tooltip--top': this.top,
        'v-tooltip--right': this.right,
        'v-tooltip--bottom': this.bottom,
        'v-tooltip--left': this.left,
        'v-tooltip--attached': this.attach === '' || this.attach === true || this.attach === 'attach'
      };
    },

    computedTransition() {
      if (this.transition) return this.transition;
      return this.isActive ? 'scale-transition' : 'fade-transition';
    },

    offsetY() {
      return this.top || this.bottom;
    },

    offsetX() {
      return this.left || this.right;
    },

    styles() {
      return {
        left: this.calculatedLeft,
        maxWidth: convertToUnit(this.maxWidth),
        minWidth: convertToUnit(this.minWidth),
        opacity: this.isActive ? 0.9 : 0,
        top: this.calculatedTop,
        zIndex: this.zIndex || this.activeZIndex
      };
    }

  },

  beforeMount() {
    this.$nextTick(() => {
      this.value && this.callActivate();
    });
  },

  mounted() {
    if (getSlotType(this, 'activator', true) === 'v-slot') {
      consoleError(`v-tooltip's activator slot must be bound, try '<template #activator="data"><v-btn v-on="data.on>'`, this);
    }
  },

  methods: {
    activate() {
      // Update coordinates and dimensions of menu
      // and its activator
      this.updateDimensions(); // Start the transition

      requestAnimationFrame(this.startTransition);
    },

    deactivate() {
      this.runDelay('close');
    },

    genActivatorListeners() {
      const listeners = Activatable.options.methods.genActivatorListeners.call(this);

      listeners.focus = e => {
        this.getActivator(e);
        this.runDelay('open');
      };

      listeners.blur = e => {
        this.getActivator(e);
        this.runDelay('close');
      };

      listeners.keydown = e => {
        if (e.keyCode === keyCodes.esc) {
          this.getActivator(e);
          this.runDelay('close');
        }
      };

      return listeners;
    },

    genTransition() {
      const content = this.genContent();
      if (!this.computedTransition) return content;
      return this.$createElement('transition', {
        props: {
          name: this.computedTransition
        }
      }, [content]);
    },

    genContent() {
      return this.$createElement('div', this.setBackgroundColor(this.color, {
        staticClass: 'v-tooltip__content',
        class: {
          [this.contentClass]: true,
          menuable__content__active: this.isActive,
          'v-tooltip__content--fixed': this.activatorFixed
        },
        style: this.styles,
        attrs: this.getScopeIdAttrs(),
        directives: [{
          name: 'show',
          value: this.isContentActive
        }],
        ref: 'content'
      }), this.getContentSlot());
    }

  },

  render(h) {
    return h(this.tag, {
      staticClass: 'v-tooltip',
      class: this.classes
    }, [this.showLazyContent(() => [this.genTransition()]), this.genActivator()]);
  }

});

function unwrapExports (x) {
	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
}

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var types = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
});

unwrapExports(types);

var configs = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.cnfLogicConfig = {
    and: {
        display: '∧'
    },
    or: {
        display: '∨'
    }
};
exports.cnfFunctionConfig = {
    identity: {
        display: 'x → x',
        function: (x) => x,
        types: ['number', 'string', 'array:number', 'undefined']
    },
    abs: {
        display: 'abs',
        function: (x) => Math.abs(x),
        types: ['number']
    },
    mean: {
        display: 'mean',
        function: (nums) => (nums.reduce((add, val) => add + val) / nums.length),
        types: ['array:number']
    },
    max: {
        display: 'max',
        function: (nums) => Math.max(...nums),
        types: ['array:number']
    },
    min: {
        display: 'min',
        function: (nums) => Math.min(...nums),
        types: ['array:number']
    },
    length: {
        display: 'len',
        function: (arr) => arr.length,
        types: ['array', 'array:number', 'array:string', 'array:object']
    }
};
exports.cnfConditionalConfig = {
    eq: {
        display: '=',
        conditional: (a, b) => {
            // intentional == rather than === to allow for 1 == '1' to be true
            return a == b;
        },
        types: [
            'array',
            'array:string',
            'array:object',
            'array:number',
            'number',
            'string',
            'undefined'
        ]
    },
    neq: {
        display: '≠',
        types: [
            'array',
            'array:string',
            'array:object',
            'array:number',
            'number',
            'string',
            'undefined'
        ],
        conditional: (a, b) => a != b,
    },
    lt: {
        display: '<',
        types: ['number'],
        conditional: (a, b) => a < b,
    },
    gt: {
        display: '>',
        types: ['number'],
        conditional: (a, b) => a > b,
    },
    lte: {
        display: '≤',
        types: ['number'],
        conditional: (a, b) => a <= b,
    },
    gte: {
        display: '≥',
        types: ['number'],
        conditional: (a, b) => a >= b,
    },
    ss: {
        display: '⊂',
        types: ['array', 'array:string', 'array:number', 'string'],
        conditional: (a, b) => {
            let includes = false;
            if (Array.isArray(a)) {
                for (let i = 0; i < a.length; i++) {
                    if (a[i] == b)
                        includes = true;
                    if (includes)
                        return includes;
                }
            }
            else if (typeof a === 'string') {
                return a.includes(b);
            }
            else {
                return includes;
            }
            return includes;
        }
    },
    nss: {
        display: '⟈',
        types: ['array', 'array:string', 'array:number', 'string'],
        conditional: (a, b) => {
            let includes = true;
            if (Array.isArray(a)) {
                return a.every(e => e != b);
            }
            else if (typeof a === 'string') {
                return !a.includes(b);
            }
            return includes;
        }
    }
};
exports.tknFunctionMap = {
    max: 'max',
    min: 'min',
    maximum: 'max',
    minimum: 'min',
    length: 'length',
    len: 'length',
    mean: 'mean',
    median: 'median',
    average: 'mean',
    ave: 'mean',
    abs: 'abs',
    absoulte: 'abs',
    module: 'abs',
    identity: 'identity'
};
exports.tknConditionalMap = {
    eq: 'eq',
    is: 'eq',
    equal: 'eq',
    '=': 'eq',
    '!=': 'neq',
    '≠': 'neq',
    gt: 'gt',
    'greater than': 'gt',
    '>': 'gt',
    '≥': 'gte',
    '>=': 'gte',
    'gte': 'gte',
    'greater than or equal to': 'gte',
    'less than': 'lt',
    lt: 'lt',
    '<': 'lt',
    '≤': 'lte',
    '<=': 'lte',
    'lte': 'lte',
    'less than or equal to': 'lte',
    'is not': 'neq',
    neq: 'neq',
    'not equal to': 'neq',
    'member of': 'ss',
    substring: 'ss',
    contains: 'ss',
    includes: 'ss',
    has: 'ss',
    'does not contain': 'nss',
    'does not include': 'nss',
    'does not have': 'nss',
    'not contain': 'nss',
    'not include': 'nss',
    'not have': 'nss',
    '!contains': 'nss',
    '!includes': 'nss',
    '!has': 'nss',
};
exports.fieldRenderFunctions = {};
exports.fieldFilterFunctions = {};
exports.fieldSortByFunctions = {};
});

unwrapExports(configs);
var configs_1 = configs.cnfLogicConfig;
var configs_2 = configs.cnfFunctionConfig;
var configs_3 = configs.cnfConditionalConfig;
var configs_4 = configs.tknFunctionMap;
var configs_5 = configs.tknConditionalMap;
var configs_6 = configs.fieldRenderFunctions;
var configs_7 = configs.fieldFilterFunctions;
var configs_8 = configs.fieldSortByFunctions;

var defaults = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.PunctuationPattern = /(\w+(?:[".,-]\w+)*)|[.,()&$#![\]{}']+/g;

exports.defaultCNFLogicConfig = configs.cnfLogicConfig;
exports.defaultCNFFunctionConfig = configs.cnfFunctionConfig;
exports.defaultCNFConditionalConfig = configs.cnfConditionalConfig;
exports.defaultCNFFunctionMap = configs.tknFunctionMap;
exports.defaultCNFConditionalMap = configs.tknConditionalMap;
exports.defaultFieldRenderFunctions = configs.fieldRenderFunctions;
exports.defaultFieldFilterFunctions = configs.fieldFilterFunctions;
exports.defaultFieldSortByFunctions = configs.fieldSortByFunctions;
exports.default = {
    PunctuationPattern: exports.PunctuationPattern,
    defaultCNFLogicConfig: configs.cnfLogicConfig,
    defaultCNFFunctionConfig: configs.cnfFunctionConfig,
    defaultCNFConditionalConfig: configs.cnfConditionalConfig,
    defaultCNFFunctionMap: configs.tknFunctionMap,
    defaultCNFConditionalMap: configs.tknConditionalMap,
    defaultFieldRenderFunctions: configs.fieldRenderFunctions,
    defaultFieldFilterFunctions: configs.fieldFilterFunctions,
    defaultFieldSortByFunctions: configs.fieldSortByFunctions,
};
});

unwrapExports(defaults);
var defaults_1 = defaults.PunctuationPattern;
var defaults_2 = defaults.defaultCNFLogicConfig;
var defaults_3 = defaults.defaultCNFFunctionConfig;
var defaults_4 = defaults.defaultCNFConditionalConfig;
var defaults_5 = defaults.defaultCNFFunctionMap;
var defaults_6 = defaults.defaultCNFConditionalMap;
var defaults_7 = defaults.defaultFieldRenderFunctions;
var defaults_8 = defaults.defaultFieldFilterFunctions;
var defaults_9 = defaults.defaultFieldSortByFunctions;

var jsonsql = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @function jsonFields
 * Gives all fields for all records in SQL-like object
 * @param {JSONSQL} json - an object of {id: record} pairs
 * @returns {Fields} all fields inside any record of the provided json data.
 */
exports.jsonFields = function (json) {
    let all = [].concat(...Object.keys(json)
        .map((key) => Object.keys(json[key])));
    let unique = [...new Set(all)].sort();
    return unique;
};
/**
 * @function jsonFieldSpy
 * Looks at the first value of the field and attempts to determine the type
 * @param {JSONSQL} json - an object of {id: record} pairs
 * @param {Field} field - a field which could be found in record, e.g. record[field]
 * @returns {string} the extended type of the field (e.g. 'array:number')
 */
exports.jsonFieldSpy = function (json, field) {
    let first = Object.keys(json)[0];
    let value = json[first][field];
    return exports.extendedType(value);
};
/**
 * @function jsonFieldTypes
 * Given an SQL-like object and a list of fields, returns the types for the values of those fields
 * @param {JSONSQL} json - an object of {id: record} pairs
 * @param {Fields} fields - an array of fields which can be found in any given record, e.g. record[fields[i]]
 * @returns {object} a mapping of the {fields: extendedType}
 */
exports.jsonFieldTypes = function (json, fields) {
    let types = {};
    fields.forEach(field => {
        types[field] = { type: exports.jsonFieldSpy(json, field) };
    });
    return types;
};
/**
 * @function reduceJSON
 * Returns only the provided keys from the json
 * @param {JSONSQL} json - an object of {id: record} pairs
 * @param {string[]} keys - an array of keys of which to keep
 * @returns {object} the reduced json containing only the provided keys
 */
exports.reduceJSON = function (json, keys) {
    return Object.keys(json)
        .filter(key => keys.includes(key))
        .reduce((obj, key) => {
        obj[key] = json[key];
        return obj;
    }, {});
};
/**
 * @function extendedType
 * A slight extension of the vanilla JS types
 * @param variable - any js variable
 * @returns {string} the extended type (e.g. 'array:number')
 */
exports.extendedType = function (variable) {
    // start with JS basics
    let type = typeof variable;
    /*
    number and string are sufficient to know which functions and conditionals
    can be applied to them.
    */
    if (["number", "string"].includes(type)) {
        return type;
    }
    /*
    if not a number or string, it might be an array.
    What the elements are of that array might modify what functions
    can be applied. E.g. an array of numbers allows for min, max, mean,
    etc to be applied. Whereas an array of strings does not.
    */
    if (type != "object") {
        return type;
    }
    /* only dealing with object types at this point */
    if (Array.isArray(variable)) {
        type = "array";
        if (!variable.length) {
            return type;
        }
        let subtypes = variable.map((e) => typeof e);
        let subtype = subtypes[0];
        let allSameQ = subtypes.every(e => e === subtype);
        if (allSameQ) {
            type = `${type}:${subtype};`;
        }
        return type;
    }
    return type;
};
exports.default = {
    jsonFields: exports.jsonFields,
    jsonFieldSpy: exports.jsonFieldSpy,
    jsonFieldTypes: exports.jsonFieldTypes,
    reduceJSON: exports.reduceJSON,
    extendedType: exports.extendedType
};
});

unwrapExports(jsonsql);
var jsonsql_1 = jsonsql.jsonFields;
var jsonsql_2 = jsonsql.jsonFieldSpy;
var jsonsql_3 = jsonsql.jsonFieldTypes;
var jsonsql_4 = jsonsql.reduceJSON;
var jsonsql_5 = jsonsql.extendedType;

var utils = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

/**
 * @function identity
 * the identity function, returns the value of the speccified field as is from
 * the record.
 * @param {RecordId} id - raw text to try and match.
 * @param {Field} field - SQL-esque JSON data.
 * @param {Record} record - a subset of record ids from the provided json data
 * @returns {any} value - record[field]
 */
exports.identity = function (id, field, record) {
    return record[field];
};
/**
 * @function lookup
 * Attemps to extract object[key], however if that is undefined returns fallback
 * @param {object} object - object to search
 * @param {string} key - key to extract
 * @param {any} fallbak - default value to use if object[key] is undefined
 * @returns {any} value - record[field]
 */
exports.lookup = function (object, key, fallback) {
    return (key in object)
        ? object[key]
        : fallback;
};
/**
 * @function hasSelectedValidOption
 * Attemps to determine if the provided object is of the known options
 * @param {string} selected - option to search for
 * @param {string[]|object} options - known options
 * @returns {boolean} whether or not select is in options
 */
exports.hasSelectedValidOption = function (selected, options) {
    return Array.isArray(options)
        ? options.indexOf(selected) > -1
        : selected in options;
};
exports.defaultFieldRenderFunction = exports.identity;
exports.defaultFieldSortByFunction = exports.identity;
exports.defaultFieldFilterFunction = exports.identity;
/**
 * @function isObjectEmpty
 * Whether or not the object has any keys
 * @param {object} obj - the objec to test
 * @returns {boolean} whether or not obj is empty
 */
function isObjectEmpty(obj) {
    return (Object.entries(obj).length === 0 &&
        obj.constructor === Object);
}
exports.isObjectEmpty = isObjectEmpty;
function sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}
exports.sleep = sleep;
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
exports.uuidv4 = uuidv4;
/**
 * @function makeDefaultTknFieldMap
 * Uses all fields from the json data to make a default token map
 * @param {JSONSQL} json - SQL-esque JSON data
 * @returns {TokenMapConfig} the token mapping for fields ({x:x})
 */
exports.makeDefaultTknFieldMap = function (json) {
    let map = {};
    let fields = jsonsql.jsonFields(json);
    fields.forEach(field => {
        map[field] = field;
    });
    return map;
};
function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            timeout = null;
            if (!immediate)
                func.apply(context, args);
        }, wait);
        if (immediate && !timeout)
            func.apply(context, args);
    };
}
exports.debounce = debounce;
/**
 * @function isLogic
 * Determines which if provided argument is known logic
 * @param {x} any
 * @returns {boolean}
 */
function isLogic(x) {
    return ['and', 'or'].indexOf(x) > -1;
}
exports.isLogic = isLogic;
/**
 * @function isFilter
 * Determines which if provided argument has the shape of a filter
 * @param {x} any
 * @returns {boolean}
 */
exports.isFilter = function (x) {
    if (typeof x !== 'object')
        return false;
    return [
        isLogic(x.logic),
        typeof x.function === 'string',
        typeof x.field === 'string',
        typeof x.conditional === 'string',
        typeof x.input === 'string'
    ].every(x => x);
};
/**
 * @function isValidFilter
 * Determines which if given filter is valid
 * @param {JSONSQL} json - an SQL-like object
 * @param {Fields} validFieldTokens - known fields
 * @param {CNFLogicConfig} configLogic - conjunctive normal form logic
 * configuration.
 * @param {CNFFunctionConfig} configFunctions - conjunctive normal form function
 * configuration, i.e. what functions are avaliable to be applied.
 * @param {CNFConditionalConfig} configConditionals - conjunctive normal form
 * conditional configuration, i.e. what conditionals are avaliable to be applied.
 * @returns {boolean}
 */
exports.isValidFilter = function (filter, validFieldTokens, cnfLogicConfig, cnfFunctionsConfig, cnfConditionalConfig) {
    if (!exports.isFilter(filter))
        return false;
    return [
        exports.hasSelectedValidOption(filter.logic, cnfLogicConfig),
        exports.hasSelectedValidOption(filter.field, validFieldTokens),
        exports.hasSelectedValidOption(filter.function, cnfFunctionsConfig),
        exports.hasSelectedValidOption(filter.conditional, cnfConditionalConfig),
        filter.input !== ''
    ].every(x => x === true);
};
/**
 * @function isRecordSortable
 * Determines which if given record can be sorted on specified fields
 * @param {JSONSQL} json - an SQL-like object
 * @param {RecordId} id - the record id under consideration
 * @param {array} fields - the fields on which to sort
 * @param {ExtractorConfig} fieldSortByFunctions - an object of {field: function} pairs which modified the value to use when sorting
 * functions of in transform should be defined to take the arguments (id, field, record)
 * @returns {boolean}
 */
exports.isRecordSortable = function (json, id, fields, fieldSortByFunctions = {}) {
    let anyMissing = fields.some(field => {
        let extractor = (field in fieldSortByFunctions) ? fieldSortByFunctions[field] : exports.identity;
        let value = extractor(id, field, json[id]);
        return (typeof value === 'undefined' || value == null);
    });
    return !anyMissing;
};
/**
 * @function reverseLookup
 * Checks to values of map for the provided value whereby the key is also
 * not the provide value. If not found will return value by default.
 * This is useful when trying to find a synonym for a field, that is not the field.
 * @param {TokenMapConfig} map - an SQL-like object
 * @param {Field} value - the record id under consideration
 * @returns {Field}
 */
exports.reverseLookup = function (map, value) {
    for (let key in map) {
        let val = map[key];
        if (val === value) {
            if (key !== value) {
                return key;
            }
        }
    }
    return value;
};
exports.default = {
    identity: exports.identity,
    lookup: exports.lookup,
    hasSelectedValidOption: exports.hasSelectedValidOption,
    isObjectEmpty,
    sleep,
    uuidv4,
    makeDefaultTknFieldMap: exports.makeDefaultTknFieldMap,
    debounce,
    isLogic,
    isFilter: exports.isFilter,
    isValidFilter: exports.isValidFilter,
    isRecordSortable: exports.isRecordSortable,
    reverseLookup: exports.reverseLookup
};
});

unwrapExports(utils);
var utils_1 = utils.identity;
var utils_2 = utils.lookup;
var utils_3 = utils.hasSelectedValidOption;
var utils_4 = utils.defaultFieldRenderFunction;
var utils_5 = utils.defaultFieldSortByFunction;
var utils_6 = utils.defaultFieldFilterFunction;
var utils_7 = utils.isObjectEmpty;
var utils_8 = utils.sleep;
var utils_9 = utils.uuidv4;
var utils_10 = utils.makeDefaultTknFieldMap;
var utils_11 = utils.debounce;
var utils_12 = utils.isLogic;
var utils_13 = utils.isFilter;
var utils_14 = utils.isValidFilter;
var utils_15 = utils.isRecordSortable;
var utils_16 = utils.reverseLookup;

var filter = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });


/**
 * @function groupByConjunctiveNormalForm
 * Group an array of filters such that each sub-array starts with a logical "and"
 * and all other filters are logical "or".
 * @param {Filters} filters - a list of filter objects
 * @returns {Filters[]} - lists of lists with each object in the inner
 * list being a filter.
 */
exports.groupByConjunctiveNormalForm = function (filters) {
    let grouped = [];
    // for each filter
    for (let i = 0; i < filters.length; i++) {
        let logic = filters[i].logic;
        // logical "and" marks the beginning of a new sublist
        if (logic === "and") {
            grouped.push([filters[i]]);
            continue;
        }
        else {
            // dealing with a logical "or", will be added to latest grouping
            let list = grouped[grouped.length - 1];
            // somehow, no logical "and" anywhere
            if (typeof list === "undefined") {
                // coerce logical "or" to logical "and"
                filters[i].logic = "and";
                grouped.push([filters[i]]);
            }
            else {
                grouped[grouped.length - 1].push(filters[i]);
            }
        }
    }
    return grouped;
};
/**
 * @function getFilterAttributeFromConfig
 * given a Filter looks up the corresponding function as specificed
 * by prop in config
 * @param {Filter} filter - a list of filter objects
 * @param {string} prop - a list of filter objects
 * @param {CNFFunctionConfig|CNFConditionalConfig} config
 * @returns {Function}
 */
exports.getFilterAttributeFromConfig = function (filter, prop, config) {
    // lookup default way of transforming funtions / conditionals as specified in
    // the passed `config`.
    let attr = config[filter[prop]];
    let func = attr[prop];
    if (attr.exceptions && filter.field in attr.exceptions) {
        return attr.exceptions[filter.field];
    }
    return func;
};
/**
 * @function getExtractorFromConfig
 * retrieves the extractor function of the provided field from the provided
 * extractor config
 * @param {Field} field - field for which the extractor is requested
 * @param {ExtractorConfig} config - the config of extractor functions
 * @returns {ExtractorFunction}
 */
exports.getExtractorFromConfig = function (field, config) {
    if (field in config) {
        return config[field];
    }
    return utils.identity;
};
/**
 * @function conjunctiveNormalFormFilter
 * Applies a list of filters to the provided data.
 * @param {JSONSQL} json - SQL-esque JSON data
 * @param {Filters} filters - list of filters to be applied
 * @param {CNFLogicConfig} configLogic - conjunctive normal form logic
 * configuration
 * @param {CNFFunctionConfig} configFunctions - conjunctive normal form function
 * configuration, i.e. what functions are avaliable to be applied.
 * @param {CNFConditionalConfig} configConditionals - conjunctive normal form
 * conditional configuration, i.e. what conditionals are avaliable to be applied.
 * @param {ExtractorConfig} configExtractors - the config of extractor functions
 * for the provided json data.
 * @returns {RecordIds} filtered - the ids of the records in the provided json
 * data that passes the fitlers.
 */
exports.conjunctiveNormalFormFilter = function (json, filters = [], configLogic = {}, configFunctions = {}, configConditionals = {}, configExtractors = {}) {
    let conjunctiveNormalForm = exports.groupByConjunctiveNormalForm(filters);
    let ids = Object.keys(json);
    let filtered = ids.filter(id => {
        // extract current record
        let record = json[id];
        // calculate the truthiness of all logical "and" statements
        let and = conjunctiveNormalForm.map(groupedOrs => {
            // calculate the truthiness of all logical "or" statements
            let or = groupedOrs.some(filter => {
                // extract the function to apply
                let transform = exports.getFilterAttributeFromConfig(filter, 'function', configFunctions);
                // extract the conditional to test
                let compare = exports.getFilterAttributeFromConfig(filter, 'conditional', configConditionals);
                // get the current record's field to test against
                let extractor = exports.getExtractorFromConfig(filter.field, configExtractors);
                let prop = extractor(id, filter.field, record);
                // apply the transform to the field
                let value = transform(prop);
                // get the truthiness of comparison
                let result = compare(value, filter.input);
                return result;
            });
            return or;
        });
        return and.every(e => e === true);
    });
    return filtered;
};
/**
 * @function giRegexOnFields
 * global search across all fields for raw text
 * @param {string} text - raw text to try and match.
 * @param {JSONSQL} json - SQL-esque JSON data.
 * @param {RecordIds} ids - a subset of record ids from the provided json data
 * to search.
 * @param {Fields} fields - a subset of fields which occur in any given record
 * in which to search.
 * @param {ExtractorConfig} configExtractors - the config of extractor functions
 * for the provided json data. These values will be used when matching.
 * @returns {RecordIds} filtered - the ids of the records in the provided json
 * data that match the provided text when the extractor functions are applied.
 */
exports.giRegexOnFields = function (text, json, ids = undefined, fields = undefined, configExtractors = {}) {
    let reg = new RegExp(text, "gi");
    let matches = [];
    let idsToUse = (ids === undefined) ? Object.keys(json) : ids;
    let fieldsToUse = (fields === undefined) ? jsonsql.jsonFields(json) : fields;
    // nothing will match everything
    if (text === '')
        return idsToUse;
    idsToUse.map(id => {
        let record = json[id];
        let fieldJoin = fieldsToUse.map(field => {
            let extractor = exports.getExtractorFromConfig(field, configExtractors);
            return extractor(id, field, record);
        }).join('');
        let match = fieldJoin.match(reg);
        if (!(match == null || match.join('') == '')) {
            matches.push(id);
        }
    });
    return matches;
};
exports.default = {
    groupByConjunctiveNormalForm: exports.groupByConjunctiveNormalForm,
    getFilterAttributeFromConfig: exports.getFilterAttributeFromConfig,
    getExtractorFromConfig: exports.getExtractorFromConfig,
    conjunctiveNormalFormFilter: exports.conjunctiveNormalFormFilter,
    giRegexOnFields: exports.giRegexOnFields
};
});

unwrapExports(filter);
var filter_1 = filter.groupByConjunctiveNormalForm;
var filter_2 = filter.getFilterAttributeFromConfig;
var filter_3 = filter.getExtractorFromConfig;
var filter_4 = filter.conjunctiveNormalFormFilter;
var filter_5 = filter.giRegexOnFields;

var freetext = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });


/**
 * @function scrubPunctuation
 * removes invalid characters from a string
 * @param {string} text - the text to process.
 * @param {string|RegExp} pattern - a regex pattern of characters to remove.
 * @returns {string} - the text with matches to the provided pattern removed.
 */
exports.scrubPunctuation = function (text, pattern = defaults.PunctuationPattern) {
    return text.replace(pattern, "$1");
};
/**
 * @function tokensFromMap
 * returns the tokens from map
 * @param {TokenMapConfig} map - an object of {token: lookup} pairs
 * @returns {string[]} a list of unique tokens
 */
exports.tokensFromMap = function (map = {}) {
    let keys = Object.keys(map);
    let values = Object.values(map);
    return [...new Set([...keys, ...values])];
};
/**
 * @function sortTokens
 * Sorts tokens in the order in which to apply them
 * @param {string[]} tokens - a list of tokens to try and match against.
 * @returns {string[]} tokens as sorted
 */
exports.sortTokens = function (tokens = []) {
    // copy array with .concat() to not affect order of passed item
    let tkns = tokens.concat();
    // sort tokens in descending length
    tkns.sort(function (a, b) {
        return b.length - a.length;
    });
    return tkns;
};
/**
 * @function extractTokensFromMap
 * Extracts the tokens from the provided mapping, sorted
 * @param {TokenMapConfig[]} map - the mapping of tokens.
 * @returns {string[]} tokens as sorted
 */
exports.extractTokensFromMap = function (map = {}) {
    return exports.sortTokens(exports.tokensFromMap(map));
};
/**
 * @function doTokensSetMatch
 * given two token sets, returns whether or not they are identical
 * @param {string[]} tokenSet1 - a list of token parts
 * @param {string[]} tokenSet2 - a list of token parts
 * @returns {boolean} whether or not they match
 */
exports.doTokensSetMatch = function (tokenSet1, tokenSet2) {
    if (tokenSet1.length !== tokenSet2.length)
        return false;
    for (let i = tokenSet1.length; i--;) {
        if (tokenSet1[i] !== tokenSet2[i])
            return false;
    }
    return true;
};
/**
 * @function searchTokensForValues
 * searchs tokens for matching values
 * @description notably this function handles multi-token matching
 * @param {string[]} tokens - an array of input tokens of which to search for
 * tokens matching in values
 * @param {string[]} values - an array of value tokens from which to search
 * if an input token matches
 * @returns {[boolean, string, number, number]} a list of useful indicators
 * about the provided query:
 * [0]: {boolean}, whether or not a token in tokens matched one of values
 * [1]: {string}, the value that matched
 * [2]: {number}, the index of the value that matched
 * [3]: {number}, the index of the token that matched
 */
exports.searchTokensForValues = function (tokens, values) {
    let valueIndex = 0;
    let tokenIndex = 0;
    let currentValue;
    let tokenizedValue;
    let tokensAreEqual = false;
    let consecutiveTokens;
    // for each value
    for (valueIndex = 0; valueIndex < values.length; valueIndex++) {
        // current value to match in tokens
        currentValue = values[valueIndex].toLowerCase();
        // "tokenize" current, in case of multiples word are in the value
        tokenizedValue = currentValue.split(' ');
        // look at n consecutive tokens where n is the number of tokenized values
        if (tokenizedValue.length > tokens.length)
            continue; // too long
        for (tokenIndex = 0; tokenIndex < tokens.length; tokenIndex++) {
            consecutiveTokens = tokens.slice(tokenIndex, tokenIndex + tokenizedValue.length);
            tokensAreEqual = exports.doTokensSetMatch(tokenizedValue, consecutiveTokens);
            if (tokensAreEqual)
                break; // early exit
        }
        if (tokensAreEqual)
            break; // early exit
    }
    return [tokensAreEqual, values[valueIndex], valueIndex, tokenIndex];
};
/**
 * @function
 * given an input string splits it by spaces, skipping regions with double quotes
 * @param {string} text - an input string
 * @returns {string[]} - A list of strings, where each string is a word
 *
 * From: https://stackoverflow.com/a/46946420
 *
 * Example: splitTextIntoWords('This is a list of words "that should\\" not be escaped"')
 * Returns: ['This', 'is', 'a', 'list', 'of', 'words', 'that should" not be escaped']
 */
const splitTextIntoWords = function (text) {
    var out = text.match(/\\?.|^$/g).reduce((p, c) => {
        if (c === '"') {
            p.quote ^= 1;
        }
        else if (!p.quote && c === ' ') {
            p.a.push('');
        }
        else {
            p.a[p.a.length - 1] += c.replace(/\\(.)/, "$1");
        }
        return p;
    }, { a: [''], quote: 0 }).a;
    return out;
};
/**
 * @function findLogicalStatements
 * given an input string attempts to parse it into logical statements (e.g.
 * and / or)
 * @param {string} text - an input string
 * @returns {number[]} - lists indices when the text is split by spaces of where
 * the logical statements begins
 */
exports.findLogicalStatements = function (text) {
    let words = splitTextIntoWords(text);
    let statementIndicies = [];
    for (let i = 0; i < words.length; i++) {
        let word = words[i];
        let isAnd = word.localeCompare('and', undefined, { sensitivity: 'base' });
        let isOr = word.localeCompare('or', undefined, { sensitivity: 'base' });
        // handel things like "greater than or equal to"
        let isEqual = 0;
        if (i < words.length - 1) {
            isEqual = words[i + 1].localeCompare('equal', undefined, { sensitivity: 'base' });
        }
        if (isAnd === 0 || (isOr === 0 && isEqual !== 0)) {
            statementIndicies.push(i);
        }
    }
    return statementIndicies;
};
/**
 * @function parseTextIntoStatements
 * splits the provided text into substrings each of which contains a logical
 * filter.
 * @param {string} text - an input string
 * @returns {string[]} - substrings each of which contains a logical filter
 */
exports.parseTextIntoStatements = function (text) {
    // where each logical statement starts
    let statementStarts = exports.findLogicalStatements(text);
    // no statements found, assume full text is statement with implied "and"
    if (statementStarts.length === 0)
        return [text];
    let statements = [];
    let words = splitTextIntoWords(text);
    // slice text into statements
    statements.push(words.slice(0, statementStarts[0]).join(' '));
    for (let i = 0; i < statementStarts.length; i++) {
        let j = statementStarts[i];
        if (i === statementStarts.length - 1) {
            statements.push(words.slice(j).join(' '));
        }
        else {
            statements.push(words.slice(j, statementStarts[i + 1]).join(' '));
        }
    }
    if (statements[0] === '')
        return statements.slice(1);
    return statements;
};
/**
 * @function extractFilterFromText
 * attempts to extract a logical filter from the provided text and configs.
 * @param {Object} args - how to configure the function
 * @param {string} args.input - an input string from which to try and find a logical
 * filter.
 * @param {TokenMapConfig} args.tknFieldMap - the token mapping for fields
 * @param {TokenMapConfig} args.tknFunctionMap - the token mapping for functions
 * @param {TokenMapConfig} args.tknConditionalMap - the token mapping for conditionals
 * @param {string[]} args.fieldTokens - the field tokens in order of which to search
 * @param {string[]} args.functionTokens - the function tokens in order of which to search
 * @param {string[]} args.conditionalTokens - the conditional tokens in order of which to search
 * @param {string|RegExp} args.scrubInputPattern - a regex pattern of characters to remove.
 * @param {boolean} args.debug - whether or not to debug to console.
 * @returns {Filter} - the extracted filter. if field and / or input is undefined
 * the extraction failed. By default logic, function, and conditional have the following
 * default values (and, identity, eq)
 */
exports.extractFilterFromText = function ({ 
/** @param {string} args.input - an input string from which to try and find a logical filter. */
input, 
/** @param {TokenMapConfig} args.tknFieldMap - the token mapping for fields */
tknFieldMap = {}, 
/** @param {TokenMapConfig} args.tknFunctionMap - the token mapping for functions */
tknFunctionMap = {}, 
/** @param {TokenMapConfig} args.tknConditionalMap - the token mapping for conditionals */
tknConditionalMap = {}, 
/** @param {string[]} args.fieldTokens - the field tokens in order of which to search */
fieldTokens = undefined, 
/** @param {string[]} args.functionTokens - the function tokens in order of which to search */
functionTokens = undefined, 
/** @param {string[]} args.conditionalTokens - the conditional tokens in order of which to search */
conditionalTokens = undefined, 
/** @param {string|RegExp} args.scrubInputPattern - a regex pattern of characters to remove. */
scrubInputPattern = defaults.PunctuationPattern, 
/** @param {boolean} args.debug - whether or not to debug to console. */
debug = false }) {
    // STEP 0.1: initalize filter object to be returned
    let filter = {
        logic: 'and',
        function: 'identity',
        field: undefined,
        conditional: 'eq',
        input: undefined
    };
    // STEP 0.2: get tokens to match and ensure they are in descending order
    if (fieldTokens === undefined)
        fieldTokens = exports.extractTokensFromMap(tknFieldMap);
    else
        fieldTokens = exports.sortTokens(fieldTokens);
    if (functionTokens === undefined)
        functionTokens = exports.extractTokensFromMap(tknFunctionMap);
    else
        functionTokens = exports.sortTokens(functionTokens);
    if (conditionalTokens === undefined)
        conditionalTokens = exports.extractTokensFromMap(tknConditionalMap);
    else
        conditionalTokens = exports.sortTokens(conditionalTokens);
    if (debug)
        console.debug(fieldTokens, functionTokens, conditionalTokens);
    // STEP 1: preprocess text for later token matching
    // remove leading and trailing whitespace
    let text = `${input}`.trim();
    // remove all "invalid" punctuation
    text = exports.scrubPunctuation(text, scrubInputPattern);
    if (debug)
        console.debug(`input post scrub:\t${text}`);
    // "tokenize"
    let textTokensUntouched = text.split(' ');
    // make lowercase to standardize comparisons
    let textTokens = textTokensUntouched.map(x => x.toLowerCase());
    // STEP 2: extract logic from input
    // ASSUMPTION: if logic is specified, it will be the first word.
    let firstWord = textTokens[0].toLowerCase();
    switch (utils.isLogic(firstWord)) {
        case true:
            filter.logic = firstWord;
            textTokens.splice(0, 1);
            textTokensUntouched.splice(0, 1);
    }
    // STEP 3: extract function from input
    // ASSUMPTION: default function is the identity function
    let [tokensAreEqual, // did we find a token
    currentValue, // what token did we find
    valueIndex, // index of token from passed values (arg2)
    tokenIndex // index of token from passed tokens (arg1)
    ] = exports.searchTokensForValues(textTokens, functionTokens);
    if (tokensAreEqual) {
        filter.function = tknFunctionMap[currentValue];
    }
    // STEP 4: extract conditional from input
    [
        tokensAreEqual,
        currentValue,
        valueIndex,
        tokenIndex
    ] = exports.searchTokensForValues(textTokens, conditionalTokens);
    if (tokensAreEqual) {
        filter.conditional = tknConditionalMap[currentValue];
    }
    // everything after the conditional is "input", so need to keep track of index
    let conditionalIndex = tokenIndex;
    let conditionalValue = ((currentValue === undefined) ? '' : currentValue);
    // STEP 5: extract field on which the filter should be applied
    [
        tokensAreEqual,
        currentValue,
        valueIndex,
        tokenIndex
    ] = exports.searchTokensForValues(textTokens, fieldTokens);
    if (tokensAreEqual) {
        filter.field = tknFieldMap[currentValue];
    }
    // STEP 6: return everything after the conditional
    // everything after the conditional is "input"
    filter.input = textTokensUntouched.slice(conditionalIndex + conditionalValue.split(' ').length, textTokens.length).join(' ');
    return filter;
};
/**
 * @function extractFilterFromText
 * attempts to extract multiple logical filters from the provided text and configs.
 * @param {Object} args - how to configure the function
 * @param {string} args.input - an input string from which to try and find a logical
 * filter.
 * @param {TokenMapConfig} args.tknFieldMap - the token mapping for fields
 * @param {TokenMapConfig} args.tknFunctionMap - the token mapping for functions
 * @param {TokenMapConfig} args.tknConditionalMap - the token mapping for conditionals
 * @param {string[]} args.fieldTokens - the field tokens in order of which to search
 * @param {string[]} args.functionTokens - the function tokens in order of which to search
 * @param {string[]} args.conditionalTokens - the conditional tokens in order of which to search
 * @param {string|RegExp} args.scrubInputPattern - a regex pattern of characters to remove.
 * @param {CNFLogicConfig} args.configLogic - conjunctive normal form logic
 * configuration.
 * @param {CNFFunctionConfig} args.configFunctions - conjunctive normal form function
 * configuration, i.e. what functions are avaliable to be applied.
 * @param {CNFConditionalConfig} args.configConditionals - conjunctive normal form
 * conditional configuration, i.e. what conditionals are avaliable to be applied.
 * @param {boolean} args.debug - whether or not to debug to console.
 * @returns {ExtractedFilters[]} - the valid filters ([0]) which were extracted and
 * the invalid filters ([1]) which were extracted.
 * @returns {Filter} - ExtractedFilters[].filter - the filter object
 * @returns {string} - ExtractedFilters[].text - the text which was processed to produce
 * the filter
 */
exports.extractFiltersFromText = function ({ 
/** @param {string} args.input - an input string from which to try and find a logical filter.*/
input, 
/** @param {TokenMapConfig} args.tknFieldMap - the token mapping for fields*/
tknFieldMap = {}, 
/** @param {TokenMapConfig} args.tknFunctionMap - the token mapping for functions*/
tknFunctionMap = {}, 
/** @param {TokenMapConfig} args.tknConditionalMap - the token mapping for conditionals*/
tknConditionalMap = {}, 
/** @param {string[]} args.fieldTokens - the field tokens in order of which to search*/
fieldTokens = undefined, 
/** @param {string[]} args.functionTokens - the function tokens in order of which to search*/
functionTokens = undefined, 
/** @param {string[]} args.conditionalTokens - the conditional tokens in order of which to search*/
conditionalTokens = undefined, 
/** @param {string|RegExp} args.scrubInputPattern - a regex pattern of characters to remove.*/
scrubInputPattern = defaults.PunctuationPattern, 
/** @param {CNFLogicConfig} args.configLogic - conjunctive normal form logic configuration.*/
cnfLogicConfig = {}, 
/** @param {CNFFunctionConfig} args.configFunctions - conjunctive normal form function configuration, i.e. what functions are avaliable to be applied.*/
cnfFunctionsConfig = {}, 
/** @param {CNFConditionalConfig} args.configConditionals - conjunctive normal form conditional configuration, i.e. what conditionals are avaliable to be applied.*/
cnfConditionalConfig = {}, 
/** @param {boolean} args.debug - whether or not to debug to console.*/
debug = false, }) {
    if (fieldTokens === undefined)
        fieldTokens = exports.extractTokensFromMap(tknFieldMap);
    else
        fieldTokens = exports.sortTokens(fieldTokens);
    if (functionTokens === undefined)
        functionTokens = exports.extractTokensFromMap(tknFunctionMap);
    else
        functionTokens = exports.sortTokens(functionTokens);
    if (conditionalTokens === undefined)
        conditionalTokens = exports.extractTokensFromMap(tknConditionalMap);
    else
        conditionalTokens = exports.sortTokens(conditionalTokens);
    // break text into chunks of logical statments
    let statements = exports.parseTextIntoStatements(input);
    let isValid = [], isError = [];
    statements.forEach((statement) => {
        let filter = exports.extractFilterFromText({
            input: statement,
            tknFieldMap,
            tknFunctionMap,
            tknConditionalMap,
            fieldTokens,
            functionTokens,
            conditionalTokens,
            scrubInputPattern,
            debug
        });
        let hasProvidedValidFilter = utils.isValidFilter(filter, Object.values(tknFieldMap), cnfLogicConfig, cnfFunctionsConfig, cnfConditionalConfig);
        if (hasProvidedValidFilter) {
            isValid.push({ filter, text: statement });
        }
        else {
            isError.push({ filter, text: statement });
        }
    });
    return [isValid, isError];
};
exports.default = {
    scrubPunctuation: exports.scrubPunctuation,
    tokensFromMap: exports.tokensFromMap,
    sortTokens: exports.sortTokens,
    extractTokensFromMap: exports.extractTokensFromMap,
    doTokensSetMatch: exports.doTokensSetMatch,
    searchTokensForValues: exports.searchTokensForValues,
    findLogicalStatements: exports.findLogicalStatements,
    parseTextIntoStatements: exports.parseTextIntoStatements,
    extractFilterFromText: exports.extractFilterFromText,
    extractFiltersFromText: exports.extractFiltersFromText,
};
});

unwrapExports(freetext);
var freetext_1 = freetext.scrubPunctuation;
var freetext_2 = freetext.tokensFromMap;
var freetext_3 = freetext.sortTokens;
var freetext_4 = freetext.extractTokensFromMap;
var freetext_5 = freetext.doTokensSetMatch;
var freetext_6 = freetext.searchTokensForValues;
var freetext_7 = freetext.findLogicalStatements;
var freetext_8 = freetext.parseTextIntoStatements;
var freetext_9 = freetext.extractFilterFromText;
var freetext_10 = freetext.extractFiltersFromText;

var sort = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

/**
 * @function sortNumbers
 * Comparison function for numerical values.
 * By default JavaScript does _not_ sort numbers numerically.
 * @param a - first value
 * @param b - second value
 * @param {boolean} isAscending - whether or not to sort ascending or descending
 */
exports.sortNumbers = function (a, b, isAscending) {
    return isAscending
        ? a - b
        : b - a;
};
/**
 * @function sortStrings
 * Comparison function for string values.
 * Use of the method localeCompare with the following options specified:
 * - lang='en'
 * - sensitivity='base'
 * - ignorePunctuation=true
 * @param a - first value
 * @param b - second value
 * @param {boolean} isAscending - whether or not to sort ascending or descending
 */
exports.sortStrings = function (a, b, isAscending) {
    const lang = 'en';
    const opts = { sensitivity: 'base', ignorePunctuation: true };
    return isAscending
        ? a.localeCompare(b, lang, opts)
        : b.localeCompare(a, lang, opts);
};
/**
 * @function sortAtomic
 * A simple, dynamic sorting function. If both values are are string, compares
 * strings; if both numeric, compares magnitude, else returns none.
 * @param a - first value
 * @param b - second value
 * @param {boolean} isAscending - whether or not to sort ascending or descending
 */
exports.sortAtomic = function (a, b, isAscending) {
    if (typeof a == 'string' && typeof b == 'string') {
        return exports.sortStrings(a, b, isAscending);
    }
    else if (typeof a == 'number' && typeof b == 'number') {
        return exports.sortNumbers(a, b, isAscending);
    }
    else {
        // return numberSort(a, b, isAscending)
        return; //-Infinity
    }
};
/**
 * @function sortableRecords
 * Determines which records can be sorted (has sortable values)
 * @param {JSONSQL} json - an SQL-like object
 * @param {RecordIds} ids - a subset of record ids to sort, if not specified, applied to all
 * @param {Fields} fields - a list of fields which occur in each record
 * @param {ExtractorConfig} fieldSortByFunctions - an object of {field: function} pairs which modified the value to use when sorting
 * functions of in transform should be defined to take the arguments (id, record, field)
 * @returns {[RecordIds, RecordIds]} the ids of the sortable records and those which
 * can not be sorted
 */
function sortableRecords(json, ids, fields, fieldSortByFunctions = {}) {
    let sortable = [], ignorable = [];
    ids.forEach(id => {
        let isSortable = utils.isRecordSortable(json, id, fields, fieldSortByFunctions);
        if (isSortable) {
            // if all fields have a defined value, this record is sortable
            sortable.push(id);
        }
        else {
            // at least one field is missing a value, not sortable
            ignorable.push(id);
        }
    });
    return [sortable, ignorable];
}
exports.sortableRecords = sortableRecords;
/**
 * @function timsort
 * Peforms timsort on an SQL-like object
 * @param {JSONSQL} json - an SQL-like object
 * @param {SortSpecifications} sort - a list of sort specifications
 * @param {RecordIds} ids - a subset of record ids to sort, if not specified, applied to all
 * @param {ExtractorConfig} fieldSortByFunctions - an object of {field: function} pairs which modified the value to use when sorting
 * functions of in transform should be defined to take the arguments (id, record, field)
 * @param {SortFunctionDef} defaultSortFunction - a function which handles unspecified the sorting
 * for unspecified fields in `fieldSortByFunctions`
 * @returns {RecordIds} sorted - the ids of the provided json data sorted according
 * to the sort specification of <sort>
 */
function timsort(json, sort, ids = [], fieldSortByFunctions = {}, defaultSortFunction = exports.sortAtomic) {
    let use, sortable, ignorable;
    use = ids.length === 0 ? Object.keys(json) : [...ids];
    // no sort specified, return filtered in order that it was given
    if (sort.length === 0) {
        return use;
    }
    // determine which fields are actually sortable
    [sortable, ignorable] = sortableRecords(json, use, sort.map(s => s.field), fieldSortByFunctions);
    // setting up the comparison functions for the TimSort
    let comparisons = sort.map(({ field, isAscending }) => {
        // each comparsion function takes the two record ids, extracts their values,
        // and then passes it to the defaultSort function
        return (id1, id2) => {
            const extractor = (field in fieldSortByFunctions) ? fieldSortByFunctions[field] : utils.identity;
            let a = extractor(id1, field, json[id1]);
            let b = extractor(id2, field, json[id2]);
            return defaultSortFunction(a, b, isAscending);
        };
    });
    // the actual TimSort
    let sorted = sortable.sort((a, b) => {
        // while there is no difference, keep trying comparisons
        let difference;
        for (let i = 0; i < comparisons.length; i++) {
            difference = comparisons[i](a, b);
            if (!difference)
                continue;
            else
                break;
        }
        return difference;
    });
    // return sorted and tack on the unsortable
    return sorted.concat(ignorable);
}
exports.timsort = timsort;
exports.sortIndexFromSpecification = function (sortSpecifications, field) {
    for (let i = 0; i < sortSpecifications.length; i++) {
        if (sortSpecifications[i].field === field) {
            return i;
        }
    }
    return -1;
};
exports.sortDirectionFromSpecification = function (sortSpecifications, field) {
    let i = exports.sortIndexFromSpecification(sortSpecifications, field);
    if (i < 0)
        return undefined;
    return sortSpecifications[i].isAscending;
};
exports.toggleSortSpecification = function (sortSpecifications, field) {
    let sortSpecs = sortSpecifications.concat();
    let sortSpec = { field: field, isAscending: true };
    let i = exports.sortIndexFromSpecification(sortSpecifications, field);
    let found = i > -1;
    if (found) {
        let cur = sortSpecs[i];
        if (cur.isAscending) {
            sortSpec.isAscending = false;
            sortSpecs[i] = sortSpec;
        }
        else {
            sortSpecs.splice(i, 1);
        }
    }
    else {
        sortSpecs.push(sortSpec);
    }
    return sortSpecs;
};
exports.default = {
    sortNumbers: exports.sortNumbers,
    sortStrings: exports.sortStrings,
    sortAtomic: exports.sortAtomic,
    sortableRecords,
    timsort,
    sortIndexFromSpecification: exports.sortIndexFromSpecification,
    sortDirectionFromSpecification: exports.sortDirectionFromSpecification,
    toggleSortSpecification: exports.toggleSortSpecification,
};
});

unwrapExports(sort);
var sort_1 = sort.sortNumbers;
var sort_2 = sort.sortStrings;
var sort_3 = sort.sortAtomic;
var sort_4 = sort.sortableRecords;
var sort_5 = sort.timsort;
var sort_6 = sort.sortIndexFromSpecification;
var sort_7 = sort.sortDirectionFromSpecification;
var sort_8 = sort.toggleSortSpecification;

var dist = createCommonjsModule(function (module, exports) {
// export * as types from './types'
// export * as configs from './defaults'
// export * from './filter'
// export * from './freetext'
// export * from './jsonsql'
// export * from './sort'
// export * from './utils'
Object.defineProperty(exports, "__esModule", { value: true });

exports.types = types;
// // configs imported and exported renamed from defaults

exports.configs = defaults;
//

exports.groupByConjunctiveNormalForm = filter.groupByConjunctiveNormalForm;
exports.getFilterAttributeFromConfig = filter.getFilterAttributeFromConfig;
exports.getExtractorFromConfig = filter.getExtractorFromConfig;
exports.conjunctiveNormalFormFilter = filter.conjunctiveNormalFormFilter;
exports.giRegexOnFields = filter.giRegexOnFields;

exports.scrubPunctuation = freetext.scrubPunctuation;
exports.tokensFromMap = freetext.tokensFromMap;
exports.sortTokens = freetext.sortTokens;
exports.extractTokensFromMap = freetext.extractTokensFromMap;
exports.doTokensSetMatch = freetext.doTokensSetMatch;
exports.searchTokensForValues = freetext.searchTokensForValues;
exports.findLogicalStatements = freetext.findLogicalStatements;
exports.parseTextIntoStatements = freetext.parseTextIntoStatements;
exports.extractFilterFromText = freetext.extractFilterFromText;
exports.extractFiltersFromText = freetext.extractFiltersFromText;

exports.jsonFields = jsonsql.jsonFields;
exports.jsonFieldSpy = jsonsql.jsonFieldSpy;
exports.jsonFieldTypes = jsonsql.jsonFieldTypes;
exports.reduceJSON = jsonsql.reduceJSON;
exports.extendedType = jsonsql.extendedType;

exports.sortNumbers = sort.sortNumbers;
exports.sortStrings = sort.sortStrings;
exports.sortAtomic = sort.sortAtomic;
exports.sortableRecords = sort.sortableRecords;
exports.timsort = sort.timsort;
exports.sortIndexFromSpecification = sort.sortIndexFromSpecification;
exports.sortDirectionFromSpecification = sort.sortDirectionFromSpecification;
exports.toggleSortSpecification = sort.toggleSortSpecification;

exports.identity = utils.identity;
exports.lookup = utils.lookup;
exports.hasSelectedValidOption = utils.hasSelectedValidOption;
exports.isObjectEmpty = utils.isObjectEmpty;
exports.sleep = utils.sleep;
exports.uuidv4 = utils.uuidv4;
exports.makeDefaultTknFieldMap = utils.makeDefaultTknFieldMap;
exports.debounce = utils.debounce;
exports.reverseLookup = utils.reverseLookup;
exports.default = {
    configs: defaults, types,
    groupByConjunctiveNormalForm: filter.groupByConjunctiveNormalForm,
    getFilterAttributeFromConfig: filter.getFilterAttributeFromConfig,
    getExtractorFromConfig: filter.getExtractorFromConfig,
    conjunctiveNormalFormFilter: filter.conjunctiveNormalFormFilter,
    giRegexOnFields: filter.giRegexOnFields,
    scrubPunctuation: freetext.scrubPunctuation,
    tokensFromMap: freetext.tokensFromMap,
    sortTokens: freetext.sortTokens,
    extractTokensFromMap: freetext.extractTokensFromMap,
    doTokensSetMatch: freetext.doTokensSetMatch,
    searchTokensForValues: freetext.searchTokensForValues,
    findLogicalStatements: freetext.findLogicalStatements,
    parseTextIntoStatements: freetext.parseTextIntoStatements,
    extractFilterFromText: freetext.extractFilterFromText,
    extractFiltersFromText: freetext.extractFiltersFromText,
    jsonFields: jsonsql.jsonFields,
    jsonFieldSpy: jsonsql.jsonFieldSpy,
    jsonFieldTypes: jsonsql.jsonFieldTypes,
    reduceJSON: jsonsql.reduceJSON,
    extendedType: jsonsql.extendedType,
    sortNumbers: sort.sortNumbers,
    sortStrings: sort.sortStrings,
    sortAtomic: sort.sortAtomic,
    sortableRecords: sort.sortableRecords,
    timsort: sort.timsort,
    sortIndexFromSpecification: sort.sortIndexFromSpecification,
    sortDirectionFromSpecification: sort.sortDirectionFromSpecification,
    toggleSortSpecification: sort.toggleSortSpecification,
    identity: utils.identity,
    lookup: utils.lookup,
    hasSelectedValidOption: utils.hasSelectedValidOption,
    isObjectEmpty: utils.isObjectEmpty,
    sleep: utils.sleep,
    uuidv4: utils.uuidv4,
    makeDefaultTknFieldMap: utils.makeDefaultTknFieldMap,
    debounce: utils.debounce,
    reverseLookup: utils.reverseLookup
};
});

unwrapExports(dist);
var dist_1 = dist.types;
var dist_2 = dist.configs;
var dist_3 = dist.groupByConjunctiveNormalForm;
var dist_4 = dist.getFilterAttributeFromConfig;
var dist_5 = dist.getExtractorFromConfig;
var dist_6 = dist.conjunctiveNormalFormFilter;
var dist_7 = dist.giRegexOnFields;
var dist_8 = dist.scrubPunctuation;
var dist_9 = dist.tokensFromMap;
var dist_10 = dist.sortTokens;
var dist_11 = dist.extractTokensFromMap;
var dist_12 = dist.doTokensSetMatch;
var dist_13 = dist.searchTokensForValues;
var dist_14 = dist.findLogicalStatements;
var dist_15 = dist.parseTextIntoStatements;
var dist_16 = dist.extractFilterFromText;
var dist_17 = dist.extractFiltersFromText;
var dist_18 = dist.jsonFields;
var dist_19 = dist.jsonFieldSpy;
var dist_20 = dist.jsonFieldTypes;
var dist_21 = dist.reduceJSON;
var dist_22 = dist.extendedType;
var dist_23 = dist.sortNumbers;
var dist_24 = dist.sortStrings;
var dist_25 = dist.sortAtomic;
var dist_26 = dist.sortableRecords;
var dist_27 = dist.timsort;
var dist_28 = dist.sortIndexFromSpecification;
var dist_29 = dist.sortDirectionFromSpecification;
var dist_30 = dist.toggleSortSpecification;
var dist_31 = dist.identity;
var dist_32 = dist.lookup;
var dist_33 = dist.hasSelectedValidOption;
var dist_34 = dist.isObjectEmpty;
var dist_35 = dist.sleep;
var dist_36 = dist.uuidv4;
var dist_37 = dist.makeDefaultTknFieldMap;
var dist_38 = dist.debounce;
var dist_39 = dist.reverseLookup;

var script = Vue.extend({
  components: {
    VChip
  },
  props: {
    logicalAndFilterGroupIndex: {
      type: Number,
      default: 0
    },
    logicalOrFilterGroupIndex: {
      type: Number,
      default: 0
    },
    filter: {
      type: Object,
      default: () => ({
        logic: 'and',
        function: 'identity',
        field: '',
        conditional: 'is',
        input: ''
      })
    },
    fieldMap: {
      type: Object,
      default: () => ({})
    },
    cnfLogicConfig: {
      type: Object,
      default: () => dist_2.defaultCNFLogicConfig
    },
    cnfFunctionsConfig: {
      type: Object,
      default: () => dist_2.defaultCNFFunctionConfig
    },
    cnfConditionalConfig: {
      type: Object,
      default: () => dist_2.defaultCNFConditionalConfig
    }
  },
  methods: {
    clickClose() {
      this.$emit('click:close', [this.logicalAndFilterGroupIndex, this.logicalOrFilterGroupIndex]);
    }

  },
  computed: {
    notIdentityFn() {
      return this.filter.function !== 'identity';
    },

    functionString() {
      let fn = this.filter.function;
      return this.cnfFunctionsConfig[fn].display;
    },

    fieldString() {
      let field = this.filter.field;
      return dist_39(this.fieldMap, field);
    },

    conditionalString() {
      let cond = this.filter.conditional;
      return this.cnfConditionalConfig[cond].display;
    }

  }
});

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}

const isOldIE = typeof navigator !== 'undefined' &&
    /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
    return (id, style) => addStyle(id, style);
}
let HEAD;
const styles = {};
function addStyle(id, css) {
    const group = isOldIE ? css.media || 'default' : id;
    const style = styles[group] || (styles[group] = { ids: new Set(), styles: [] });
    if (!style.ids.has(id)) {
        style.ids.add(id);
        let code = css.source;
        if (css.map) {
            // https://developer.chrome.com/devtools/docs/javascript-debugging
            // this makes source maps inside style tags work properly in Chrome
            code += '\n/*# sourceURL=' + css.map.sources[0] + ' */';
            // http://stackoverflow.com/a/26603875
            code +=
                '\n/*# sourceMappingURL=data:application/json;base64,' +
                    btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) +
                    ' */';
        }
        if (!style.element) {
            style.element = document.createElement('style');
            style.element.type = 'text/css';
            if (css.media)
                style.element.setAttribute('media', css.media);
            if (HEAD === undefined) {
                HEAD = document.head || document.getElementsByTagName('head')[0];
            }
            HEAD.appendChild(style.element);
        }
        if ('styleSheet' in style.element) {
            style.styles.push(code);
            style.element.styleSheet.cssText = style.styles
                .filter(Boolean)
                .join('\n');
        }
        else {
            const index = style.ids.size - 1;
            const textNode = document.createTextNode(code);
            const nodes = style.element.childNodes;
            if (nodes[index])
                style.element.removeChild(nodes[index]);
            if (nodes.length)
                style.element.insertBefore(textNode, nodes[index]);
            else
                style.element.appendChild(textNode);
        }
    }
}

/* script */
const __vue_script__ = script;
/* template */

var __vue_render__ = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('v-chip', {
    staticClass: "cnf__chip",
    attrs: {
      "small": "",
      "close": ""
    },
    on: {
      "click:close": _vm.clickClose
    }
  }, [_vm.notIdentityFn ? _c('span', {
    staticClass: "cnf__function"
  }, [_vm._v("\n    " + _vm._s(_vm.functionString) + "\n  ")]) : _vm._e(), _vm._v(" "), _c('span', {
    staticClass: "cnf__field",
    class: {
      'cnf__field--function-applied': _vm.notIdentityFn
    }
  }, [_vm._v("\n    " + _vm._s(_vm.fieldString) + "\n  ")]), _vm._v(" "), _c('span', {
    staticClass: "cnf__conditional"
  }, [_vm._v("\n     \n    " + _vm._s(_vm.conditionalString) + "\n     \n  ")]), _vm._v(" "), _c('span', {
    staticClass: "cnf__input"
  }, [_vm._v("\n    " + _vm._s(_vm.filter.input) + "\n  ")])]);
};

var __vue_staticRenderFns__ = [];
/* style */

const __vue_inject_styles__ = function (inject) {
  if (!inject) return;
  inject("data-v-e7d96aa2_0", {
    source: ".cnf__field--function-applied::before{content:'(';color:#000}.cnf__field--function-applied::after{content:')';color:#000}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


const __vue_scope_id__ = undefined;
/* module identifier */

const __vue_module_identifier__ = undefined;
/* functional template */

const __vue_is_functional_template__ = false;
/* style inject SSR */

/* style inject shadow dom */

const __vue_component__ = /*#__PURE__*/normalizeComponent({
  render: __vue_render__,
  staticRenderFns: __vue_staticRenderFns__
}, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, createInjector, undefined, undefined);

var script$1 = Vue.extend({
  components: {
    VCnfChip: __vue_component__,
    VIcon: VIcon$1
  },
  props: {
    logicalAndFilterGroupIndex: {
      type: Number,
      default: 0
    },
    filters: {
      type: Array,
      default: () => []
    },
    fieldMap: {
      type: Object,
      default: () => ({})
    },
    cnfLogicConfig: {
      type: Object,
      default: () => dist_2.defaultCNFLogicConfig
    },
    cnfFunctionsConfig: {
      type: Object,
      default: () => dist_2.defaultCNFFunctionConfig
    },
    cnfConditionalConfig: {
      type: Object,
      default: () => dist_2.defaultCNFConditionalConfig
    }
  },
  methods: {
    clickClose([i, j]) {
      this.$emit('click:close', [i, j]);
    },

    hasIcon(i, j) {
      let f = this.filters[j];
      return i !== 0 && f.logic === 'and' || j !== 0;
    },

    logicString(i) {
      let f = this.filters[i];
      let logic = f.logic;
      return this.cnfLogicConfig[logic].display;
    }

  }
});

/* script */
const __vue_script__$1 = script$1;
/* template */

var __vue_render__$1 = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', {
    staticStyle: {
      "display": "inline"
    }
  }, [_vm._l(_vm.filters, function (filter, i) {
    return [_vm.hasIcon(_vm.logicalAndFilterGroupIndex, i) ? _c('v-icon', {
      key: "filter-" + _vm.logicalAndFilterGroupIndex + "-" + i + "-logic-icon",
      class: {
        'cnf__logic--and': filter.logic === 'and',
        'cnf__logic--or': filter.logic === 'or'
      },
      attrs: {
        "small": ""
      }
    }, [[_vm._v("\n        " + _vm._s(_vm.logicString(i)) + " \n      ")]], 2) : _vm._e(), _vm._v(" "), !i ? [_vm._v(" (")] : _vm._e(), _vm._v(" "), _c('VCnfChip', {
      key: "filter-" + _vm.logicalAndFilterGroupIndex + "-" + i + "-chip",
      attrs: {
        "logicalAndFilterGroupIndex": _vm.logicalAndFilterGroupIndex,
        "logicalOrFilterGroupIndex": i,
        "filter": filter,
        "fieldMap": _vm.fieldMap,
        "cnfLogicConfig": _vm.cnfLogicConfig,
        "cnfFunctionsConfig": _vm.cnfFunctionsConfig,
        "cnfConditionalConfig": _vm.cnfConditionalConfig
      },
      on: {
        "click:close": _vm.clickClose
      }
    }), _vm._v(" "), i === _vm.filters.length - 1 ? [_vm._v(") ")] : _vm._e()];
  })], 2);
};

var __vue_staticRenderFns__$1 = [];
/* style */

const __vue_inject_styles__$1 = function (inject) {
  if (!inject) return;
  inject("data-v-0a5eaf69_0", {
    source: ".cnf__chip--first-or::before{content:'('}.cnf__chip--last-or::after{content:')'}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


const __vue_scope_id__$1 = undefined;
/* module identifier */

const __vue_module_identifier__$1 = undefined;
/* functional template */

const __vue_is_functional_template__$1 = false;
/* style inject SSR */

/* style inject shadow dom */

const __vue_component__$1 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$1,
  staticRenderFns: __vue_staticRenderFns__$1
}, __vue_inject_styles__$1, __vue_script__$1, __vue_scope_id__$1, __vue_is_functional_template__$1, __vue_module_identifier__$1, false, createInjector, undefined, undefined);

var script$2 = Vue.extend({
  components: {
    VCnfOrGroup: __vue_component__$1,
    VIcon: VIcon$1,
    VChip
  },
  props: {
    filters: {
      type: Array,
      default: () => []
    },
    fieldMap: {
      type: Object,
      default: () => ({})
    },
    cnfLogicConfig: {
      type: Object,
      default: () => dist_2.defaultCNFLogicConfig
    },
    cnfFunctionsConfig: {
      type: Object,
      default: () => dist_2.defaultCNFFunctionConfig
    },
    cnfConditionalConfig: {
      type: Object,
      default: () => dist_2.defaultCNFConditionalConfig
    },
    regex: {
      type: String,
      default: ''
    }
  },
  methods: {
    clickClose([i, j]) {
      let loc = 0;

      for (let k = 0; k < i; k++) {
        loc += this.cnfGroupedFilters[k].length;
      }

      loc += j;
      this.$emit('click:close', loc);
    },

    clickCloseRegEx() {
      this.$emit('click:close:regex', true);
    }

  },
  computed: {
    andLogicString() {
      let logic = 'and';
      return this.cnfLogicConfig[logic].display;
    },

    cnfGroupedFilters() {
      return dist_3(this.filters);
    }

  }
});

/* script */
const __vue_script__$2 = script$2;
/* template */

var __vue_render__$2 = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', [_vm._l(_vm.cnfGroupedFilters, function (orFilters, i) {
    return [_c('VCnfOrGroup', {
      key: "logical-and-group-" + i,
      attrs: {
        "logicalAndFilterGroupIndex": i,
        "filters": orFilters,
        "fieldMap": _vm.fieldMap,
        "cnfLogicConfig": _vm.cnfLogicConfig,
        "cnfFunctionsConfig": _vm.cnfFunctionsConfig,
        "cnfConditionalConfig": _vm.cnfConditionalConfig
      },
      on: {
        "click:close": _vm.clickClose
      }
    })];
  }), _vm._v(" "), _vm.regex !== '' ? [_vm.cnfGroupedFilters.length !== 0 ? _c('v-icon', {
    key: "regex-icon",
    staticClass: "cnf__logic--and",
    attrs: {
      "small": ""
    }
  }, [[_vm._v("\n        " + _vm._s(_vm.andLogicString) + "\n      ")]], 2) : _vm._e(), _vm._v(" "), _c('v-chip', {
    attrs: {
      "close": "",
      "small": ""
    },
    on: {
      "click:close": _vm.clickCloseRegEx
    }
  }, [_c('var', [_vm._v("regex:")]), _vm._v(" "), _c('kbd', [_vm._v("/" + _vm._s(_vm.regex) + "/gi")])])] : _vm._e()], 2);
};

var __vue_staticRenderFns__$2 = [];
/* style */

const __vue_inject_styles__$2 = function (inject) {
  if (!inject) return;
  inject("data-v-3c23a9ba_0", {
    source: ".cnf__logic--and{color:#0072ff!important}.cnf__logic--or{color:#00c6ff!important}.cnf__field{color:#f83600}.cnf__function{color:#000}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


const __vue_scope_id__$2 = undefined;
/* module identifier */

const __vue_module_identifier__$2 = undefined;
/* functional template */

const __vue_is_functional_template__$2 = false;
/* style inject SSR */

/* style inject shadow dom */

const __vue_component__$2 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$2,
  staticRenderFns: __vue_staticRenderFns__$2
}, __vue_inject_styles__$2, __vue_script__$2, __vue_scope_id__$2, __vue_is_functional_template__$2, __vue_module_identifier__$2, false, createInjector, undefined, undefined);

var script$3 = Vue.extend({
  components: {
    VCol,
    VRow,
    VSelect
  },
  props: {
    extractorConfig: {
      type: Object,
      default: () => ({})
    },
    functionLookup: {
      type: Object,
      default: () => ({})
    },
    stackedLayout: {
      type: Boolean,
      default: true
    }
  },

  created() {
    this.populateDefaults();
    this.change();
  },

  watch: {
    extractorConfig: function (newConfig, oldConfig) {
      this.populateDefaults();
    }
  },
  data: () => ({
    currentOption: '',
    showConfig: true,
    selections: {},
    renderFunctions: {},
    sortByFunctions: {},
    filterFunctions: {}
  }),
  methods: {
    updateOptions(innerKey) {
      this.$set(this.selections, this.currentOption, innerKey);
      let currentConfig = this.extractorConfig[this.currentOption][innerKey];
      let opts = ['render', 'filter', 'sortBy'];

      for (let i = 0; i < opts.length; i++) {
        let t = opts[i];
        let cfg = currentConfig[`${t}Config`];
        let func = this.functionLookup[cfg.functionName];
        let f;

        if (func === undefined) {
          f = dist_31;
        } else {
          f = func({ ...cfg.params
          });
        }

        this.$set(this[`${t}Functions`], this.currentOption, f);
      }

      this.change();
    },

    populateDefaults() {
      let newKeys = false;
      let selections = { ...this.selections
      };
      let functions = {
        renderFunctions: { ...this.renderFunctions
        },
        filterFunctions: { ...this.sortByFunctions
        },
        sortByFunctions: { ...this.filterFunctions
        }
      };
      let outerKeys = Object.keys(this.extractorConfig);
      let opts = ['render', 'filter', 'sortBy'];
      outerKeys.forEach((k, i) => {
        let c = this.extractorConfig[k];
        let innerKeys = Object.keys(c);
        let s = innerKeys[0];
        if (Object.keys(selections).indexOf(s) >= 0) return;
        newKeys = true;
        selections[k] = s;

        for (let i = 0; i < opts.length; i++) {
          let t = opts[i];
          let cfg = c[s][`${t}Config`];
          let func = this.functionLookup[cfg.functionName];
          let f;

          if (func === undefined) {
            f = dist_31;
          } else {
            f = func({ ...cfg.params
            });
          }

          functions[`${t}Functions`][k] = f;
        }
      });
      this.selections = Object.assign({}, selections);

      for (let i = 0; i < opts.length; i++) {
        let t = opts[i];
        this[`${t}Functions`] = Object.assign({}, functions[`${t}Functions`]);
      }

      if (newKeys) {
        this.change();
      }
    },

    change() {
      this.$emit('change', {
        renderFunctions: this.renderFunctions,
        sortByFunctions: this.sortByFunctions,
        filterFunctions: this.filterFunctions,
        selections: this.selections
      });
    }

  },
  computed: {
    outerKeys() {
      return Object.keys(this.extractorConfig);
    },

    currentInnerKeys() {
      if (this.currentOption === '') return [];
      if (!(this.currentOption in this.extractorConfig)) return [];
      return Object.keys(this.extractorConfig[this.currentOption]);
    },

    currentOptionConfigs() {
      if (this.currentOption === '') return [];
      return this.currentInnerKeys.map(k => {
        let currentConfigOptions = this.extractorConfig[this.currentOption];
        return {
          text: `${this.currentOption} by ${k}`,
          value: k
        };
      });
    },

    configurableOuterKeys() {
      return this.outerKeys.filter(k => {
        return Object.keys(this.extractorConfig[k]).length > 1;
      });
    }

  }
});

/* script */
const __vue_script__$3 = script$3;
/* template */

var __vue_render__$3 = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('v-col', {
    staticClass: "py-0"
  }, [_c('v-row', {
    staticClass: "py-0"
  }, [_c('v-col', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.showConfig,
      expression: "showConfig"
    }],
    attrs: {
      "cols": _vm.stackedLayout || _vm.currentOption === '' ? 12 : 6
    }
  }, [_c('v-select', {
    attrs: {
      "dense": "",
      "items": _vm.configurableOuterKeys,
      "outlined": "",
      "label": "Adjust configuration of"
    },
    model: {
      value: _vm.currentOption,
      callback: function ($$v) {
        _vm.currentOption = $$v;
      },
      expression: "currentOption"
    }
  })], 1), _vm._v(" "), _c('v-col', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.showConfig && _vm.currentOption !== '',
      expression: "showConfig && currentOption!==''"
    }],
    attrs: {
      "cols": _vm.stackedLayout ? 12 : 6
    }
  }, [_c('v-select', {
    attrs: {
      "dense": "",
      "outlined": "",
      "items": _vm.currentOptionConfigs,
      "value": _vm.extractorConfig[_vm.currentOption],
      "item-text": "text",
      "item-value": "value",
      "label": "to"
    },
    on: {
      "input": _vm.updateOptions
    }
  })], 1)], 1)], 1);
};

var __vue_staticRenderFns__$3 = [];
/* style */

const __vue_inject_styles__$3 = undefined;
/* scoped */

const __vue_scope_id__$3 = undefined;
/* module identifier */

const __vue_module_identifier__$3 = undefined;
/* functional template */

const __vue_is_functional_template__$3 = false;
/* style inject */

/* style inject SSR */

/* style inject shadow dom */

const __vue_component__$3 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$3,
  staticRenderFns: __vue_staticRenderFns__$3
}, __vue_inject_styles__$3, __vue_script__$3, __vue_scope_id__$3, __vue_is_functional_template__$3, __vue_module_identifier__$3, false, undefined, undefined, undefined);

var script$4 = Vue.extend({
  components: {
    VTextField
  },
  props: {
    tknFieldMap: {
      type: Object,
      default: () => ({})
    },
    tknFunctionMap: {
      type: Object,
      default: dist_2.defaultCNFFunctionMap
    },
    tknConditionalMap: {
      type: Object,
      default: dist_2.defaultCNFConditionalMap
    },
    fieldTokens: {
      type: Array,
      default: undefined
    },
    functionTokens: {
      type: Array,
      default: undefined
    },
    conditionalTokens: {
      type: Array,
      default: undefined
    },
    PunctuationPattern: {
      type: [RegExp, String],
      default: dist_2.PunctuationPattern
    },
    cnfLogicConfig: {
      type: Object,
      default: dist_2.defaultCNFLogicConfig
    },
    cnfFunctionsConfig: {
      type: Object,
      default: dist_2.defaultCNFFunctionConfig
    },
    cnfConditionalConfig: {
      type: Object,
      default: dist_2.defaultCNFConditionalConfig
    },
    debug: {
      type: Boolean,
      default: false
    }
  },
  data: () => ({
    hint: `type statements in the form of 'logic field (function) test value'.`,
    label: `Free Text`,
    persistentHint: true,
    appendIcon: `mdi-filter`,
    input: ''
  }),
  methods: {
    filtersFromText() {
      let [valid, error] = dist_17({
        input: this.input,
        tknFieldMap: this.tknFieldMap,
        tknFunctionMap: this.tknFunctionMap,
        tknConditionalMap: this.tknConditionalMap,
        fieldTokens: undefined,
        functionTokens: undefined,
        conditionalTokens: undefined,
        scrubInputPattern: this.PunctuationPattern,
        cnfLogicConfig: this.cnfLogicConfig,
        cnfFunctionsConfig: this.cnfFunctionsConfig,
        cnfConditionalConfig: this.cnfConditionalConfig,
        debug: this.debug
      });
      this.input = '';
      this.$emit('filters-from-text', {
        valid,
        error
      });
    }

  }
});

/* script */
const __vue_script__$4 = script$4;
/* template */

var __vue_render__$4 = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('v-text-field', {
    attrs: {
      "label": _vm.label,
      "append-icon": _vm.appendIcon,
      "persistent-hint": _vm.persistentHint,
      "hint": _vm.hint,
      "clearable": ""
    },
    on: {
      "keyup": function ($event) {
        if (!$event.type.indexOf('key') && _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")) {
          return null;
        }

        return _vm.filtersFromText($event);
      }
    },
    model: {
      value: _vm.input,
      callback: function ($$v) {
        _vm.input = $$v;
      },
      expression: "input"
    }
  });
};

var __vue_staticRenderFns__$4 = [];
/* style */

const __vue_inject_styles__$4 = undefined;
/* scoped */

const __vue_scope_id__$4 = "data-v-7ab3f888";
/* module identifier */

const __vue_module_identifier__$4 = undefined;
/* functional template */

const __vue_is_functional_template__$4 = false;
/* style inject */

/* style inject SSR */

/* style inject shadow dom */

const __vue_component__$4 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$4,
  staticRenderFns: __vue_staticRenderFns__$4
}, __vue_inject_styles__$4, __vue_script__$4, __vue_scope_id__$4, __vue_is_functional_template__$4, __vue_module_identifier__$4, false, undefined, undefined, undefined);

var script$5 = Vue.extend({
  components: {
    VChip,
    VAvatar,
    VIcon: VIcon$1,
    VAutocomplete
  },
  props: {
    sortSpecifications: {
      type: Array,
      default: () => ({})
    },
    fieldMap: {
      type: Object,
      default: () => ({})
    }
  },
  computed: {
    autocompleteItems() {
      let items = []; // Object.keys(this.fieldMap)

      let fields = [...new Set(Object.values(this.fieldMap))];
      fields.forEach(field => {
        // let humanField = this.fieldMap[field]
        let humanField = dist_39(this.fieldMap, field);
        let item = {
          field,
          text: humanField
        };
        items.push(item);
      });
      return items;
    }

  },
  methods: {
    fieldSortDirectionIcon(field) {
      let dir = dist_29(this.sortSpecifications, field);
      if (dir === undefined) return 'mdi-arrow-up';
      return dir ? 'mdi-arrow-up' : 'mdi-arrow-down';
    },

    autocompleteChange(fields) {
      let newFields = [];

      if (fields.length !== 0) {
        newFields = fields.filter(field => {
          let i = dist_28(this.sortSpecifications, field);
          return i > -1;
        });
      }

      this.toggle(newFields);
    },

    change(newSortSpecs) {
      this.$emit('change', newSortSpecs);
    },

    clear() {
      this.change([]);
    },

    toggle(payload) {
      let newSortSpec = this.sortSpecifications.concat();

      if (Array.isArray(payload)) {
        let fields = payload;
        let newFields = fields.filter(field => {
          let i = dist_28(newSortSpec, field);
          return i < 0;
        });
        newFields.forEach(field => {
          newSortSpec = dist_30(newSortSpec, field);
        });
      } else {
        let field = payload;
        newSortSpec = dist_30(newSortSpec, field);
      }

      this.change(newSortSpec);
    },

    remove(payload) {
      let field = payload;
      let newSortSpec = this.sortSpecifications.concat();
      let i = dist_28(newSortSpec, field);
      newSortSpec.splice(i, 1);
      this.change(newSortSpec);
    }

  }
});

/* script */
const __vue_script__$5 = script$5;
/* template */

var __vue_render__$5 = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('v-autocomplete', {
    ref: "autocomplete",
    attrs: {
      "value": _vm.sortSpecifications,
      "items": _vm.autocompleteItems,
      "label": "TimSort",
      "multiple": "",
      "item-value": "field",
      "item-text": "text",
      "clearable": "",
      "hide-selected": "",
      "append-icon": "mdi-sort"
    },
    on: {
      "click:clear": _vm.clear,
      "change": _vm.toggle
    },
    scopedSlots: _vm._u([{
      key: "selection",
      fn: function (data) {
        return [_c('v-chip', {
          staticClass: "chip--select-multi",
          attrs: {
            "input-value": data.selected,
            "close": "",
            "small": ""
          },
          on: {
            "click:close": function ($event) {
              return _vm.remove(data.item.field);
            },
            "click": function ($event) {
              $event.preventDefault();
              $event.stopPropagation();
              return _vm.toggle(data.item.field);
            }
          }
        }, [_c('v-avatar', [_c('v-icon', {
          attrs: {
            "small": "",
            "left": ""
          }
        }, [_vm._v("\n          " + _vm._s(_vm.fieldSortDirectionIcon(data.item.field)) + "\n        ")])], 1), _vm._v("\n      " + _vm._s(data.item.text) + "\n    ")], 1)];
      }
    }])
  });
};

var __vue_staticRenderFns__$5 = [];
/* style */

const __vue_inject_styles__$5 = undefined;
/* scoped */

const __vue_scope_id__$5 = undefined;
/* module identifier */

const __vue_module_identifier__$5 = undefined;
/* functional template */

const __vue_is_functional_template__$5 = false;
/* style inject */

/* style inject SSR */

/* style inject shadow dom */

const __vue_component__$5 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$5,
  staticRenderFns: __vue_staticRenderFns__$5
}, __vue_inject_styles__$5, __vue_script__$5, __vue_scope_id__$5, __vue_is_functional_template__$5, __vue_module_identifier__$5, false, undefined, undefined, undefined);

var script$6 = Vue.extend({
  components: {
    VBtnToggle,
    VTooltip,
    VBtn,
    VIcon: VIcon$1,
    VSpacer
  },
  props: {
    hasConfig: {
      type: Boolean,
      default: true
    },
    value: {
      type: Number,
      default: 0
    }
  },
  methods: {
    change(n) {
      this.$emit('change', n);
    },

    clickCog() {
      this.$emit('click:cog', true);
    }

  }
});

/* script */
const __vue_script__$6 = script$6;
/* template */

var __vue_render__$6 = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', {
    staticClass: "d-flex",
    attrs: {
      "cols": "12"
    }
  }, [_c('v-btn-toggle', {
    attrs: {
      "value": _vm.value,
      "borderless": "",
      "dense": ""
    },
    on: {
      "change": _vm.change
    }
  }, [_c('v-tooltip', {
    attrs: {
      "bottom": ""
    },
    scopedSlots: _vm._u([{
      key: "activator",
      fn: function (ref) {
        var on = ref.on;
        return [_c('v-btn', _vm._g({}, on), [_c('v-icon', [_vm._v("mdi-filter")])], 1)];
      }
    }])
  }, [_vm._v(" "), _c('span', [_vm._v("free text filter")])]), _vm._v(" "), _c('v-tooltip', {
    attrs: {
      "bottom": ""
    },
    scopedSlots: _vm._u([{
      key: "activator",
      fn: function (ref) {
        var on = ref.on;
        return [_c('v-btn', _vm._g({}, on), [_c('v-icon', [_vm._v("mdi-sort")])], 1)];
      }
    }])
  }, [_vm._v(" "), _c('span', [_vm._v("tim sort specification")])]), _vm._v(" "), _c('v-tooltip', {
    attrs: {
      "bottom": ""
    },
    scopedSlots: _vm._u([{
      key: "activator",
      fn: function (ref) {
        var on = ref.on;
        return [_c('v-btn', _vm._g({}, on), [_c('v-icon', [_vm._v("mdi-format-quote-close")])], 1)];
      }
    }])
  }, [_vm._v(" "), _c('span', [_vm._v("regex search")])])], 1), _vm._v(" "), _c('v-spacer'), _vm._v(" "), _vm.hasConfig ? _c('v-tooltip', {
    attrs: {
      "bottom": ""
    },
    scopedSlots: _vm._u([{
      key: "activator",
      fn: function (ref) {
        var on = ref.on;
        return [_c('v-btn', _vm._g({
          attrs: {
            "depressed": ""
          },
          on: {
            "click": _vm.clickCog
          }
        }, on), [_c('v-icon', [_vm._v("mdi-cog")])], 1)];
      }
    }], null, false, 1727717408)
  }, [_vm._v(" "), _c('span', [_vm._v("configure")])]) : _vm._e()], 1);
};

var __vue_staticRenderFns__$6 = [];
/* style */

const __vue_inject_styles__$6 = undefined;
/* scoped */

const __vue_scope_id__$6 = undefined;
/* module identifier */

const __vue_module_identifier__$6 = undefined;
/* functional template */

const __vue_is_functional_template__$6 = false;
/* style inject */

/* style inject SSR */

/* style inject shadow dom */

const __vue_component__$6 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$6,
  staticRenderFns: __vue_staticRenderFns__$6
}, __vue_inject_styles__$6, __vue_script__$6, __vue_scope_id__$6, __vue_is_functional_template__$6, __vue_module_identifier__$6, false, undefined, undefined, undefined);

var script$7 = Vue.extend({
  components: {
    VCol,
    VHover,
    VBtn,
    VBadge,
    VIcon: VIcon$1
  },
  props: {
    field: {
      type: String
    },
    fieldText: {
      type: String
    },
    fieldSortNumber: {
      type: Number,
      default: 0
    },
    fieldSortIcon: {
      type: String,
      default: 'mdi-arrow-up'
    }
  },
  methods: {
    click() {
      this.$emit('click', this.field);
    }

  }
});

/* script */
const __vue_script__$7 = script$7;
/* template */

var __vue_render__$7 = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('th', {
    on: {
      "click": _vm.click
    }
  }, [_c('v-col', [_c('v-hover', {
    scopedSlots: _vm._u([{
      key: "default",
      fn: function (ref) {
        var hover = ref.hover;
        return [_c('div', {
          staticClass: "d-inline-flex"
        }, [_c('span', {
          staticClass: "align-self-center"
        }, [_vm._v("\n          " + _vm._s(_vm.fieldText) + "\n        ")]), _vm._v(" "), _c('v-btn', {
          staticClass: "smooth-transition",
          attrs: {
            "icon": "",
            "text": ""
          }
        }, [_c('v-badge', {
          attrs: {
            "color": hover ? 'primary' : 'transparent'
          },
          scopedSlots: _vm._u([{
            key: "badge",
            fn: function () {
              return [_c('span', [_vm._v("\n                " + _vm._s(_vm.fieldSortNumber + 1) + "\n              ")])];
            },
            proxy: true
          }], null, true)
        }, [_vm._v(" "), _c('v-icon', {
          class: {
            'no-opacity': !(hover || _vm.fieldSortNumber !== -1)
          }
        }, [_vm._v("\n              " + _vm._s(_vm.fieldSortIcon) + "\n            ")])], 1)], 1)], 1)];
      }
    }])
  })], 1)], 1);
};

var __vue_staticRenderFns__$7 = [];
/* style */

const __vue_inject_styles__$7 = function (inject) {
  if (!inject) return;
  inject("data-v-101116d2_0", {
    source: ".smooth-transition{transition:all .4s ease-in-out}.smooth-transition *{transition:all .4s ease-in-out}.no-opacity{opacity:0}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


const __vue_scope_id__$7 = undefined;
/* module identifier */

const __vue_module_identifier__$7 = undefined;
/* functional template */

const __vue_is_functional_template__$7 = false;
/* style inject SSR */

/* style inject shadow dom */

const __vue_component__$7 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$7,
  staticRenderFns: __vue_staticRenderFns__$7
}, __vue_inject_styles__$7, __vue_script__$7, __vue_scope_id__$7, __vue_is_functional_template__$7, __vue_module_identifier__$7, false, createInjector, undefined, undefined);

var script$8 = Vue.extend({
  components: {
    VFrxtInput: __vue_component__$4,
    VCnf: __vue_component__$2,
    VTimSort: __vue_component__$5,
    VTableProfileBtns: __vue_component__$6,
    VExtractorConfig: __vue_component__$3,
    VRecordsTableHeaderCell: __vue_component__$7,
    VData,
    VDataIterator,
    VDataFooter,
    VDataTable,
    VDataTableHeader,
    VTextField,
    VProgressLinear,
    VNavigationDrawer,
    VCheckbox,
    VTooltip,
    VCard,
    VCardText,
    VDivider,
    VCol,
    VRow,
    VList,
    VListItem,
    VListItemContent,
    VListItemTitle
  },
  props: {
    json: {
      type: Object,
      default: () => ({})
    },
    fieldSynonymMap: {
      type: Object,
      default: () => ({})
    },
    fieldRenderFunctions: {
      type: Object,
      default: () => dist_2.defaultFieldRenderFunctions
    },
    fieldFilterFunctions: {
      type: Object,
      default: () => dist_2.defaultFieldFilterFunctions
    },
    fieldSortByFunctions: {
      type: Object,
      default: () => dist_2.defaultFieldSortByFunctions
    },
    PunctuationPattern: {
      type: [RegExp, String],
      default: () => dist_2.PunctuationPattern
    },
    tknFunctionMap: {
      type: Object,
      default: () => dist_2.defaultCNFFunctionMap
    },
    tknConditionalMap: {
      type: Object,
      default: () => dist_2.defaultCNFConditionalMap
    },
    cnfLogicConfig: {
      type: Object,
      default: () => dist_2.defaultCNFLogicConfig
    },
    cnfFunctionsConfig: {
      type: Object,
      default: () => dist_2.defaultCNFFunctionConfig
    },
    cnfConditionalConfig: {
      type: Object,
      default: () => dist_2.defaultCNFConditionalConfig
    },
    fieldOrder: {
      type: Array,
      default: () => []
    },
    selected: {
      type: Array,
      default: () => []
    },
    showSelect: {
      type: Boolean,
      default: false
    },
    debug: {
      type: Boolean,
      default: false
    },
    extractorConfig: {
      type: Object,
      default: () => ({})
    },
    functionLookup: {
      type: Object,
      default: () => ({})
    },
    showTable: {
      type: Boolean,
      default: true
    }
  },
  data: () => ({
    tableProfileIndex: 0,
    configDrawerToggle: false,
    page: 0,
    isPaginated: true,
    recordsPerPage: 5,
    globalRegEx: '',
    sortSpecifications: [],
    filters: [],
    failedFilters: [],
    filtered: [],
    regmatched: [],
    timsorted: [],
    isTextProcessing: false,
    isFiltering: false,
    isRegmatching: false,
    isTimSorting: false,
    status: '',
    selectedIds: [],
    selectAllModel: false,
    extractorConfigFieldRenderFunctions: {},
    extractorConfigFieldFilterFunctions: {},
    extractorConfigFieldSortByFunctions: {},
    tooltipText: null,
    tooltipStyle: {}
  }),

  created() {
    this.setInitialState();
    this.debounceRegEx = dist_38(e => {
      this.globalRegEx = e;
    }, 500);
    this.debounceMousemoveRecord = dist_38(({
      event,
      id
    }) => {
      this.$emit('mousemove:record', {
        event,
        id
      });
    }, 500);
    this.debounceMousemoveRecordField = dist_38(({
      event,
      id,
      field
    }) => {
      this.updateTooltip({
        event,
        id,
        field
      });
      this.$emit('mousemove:record:field', {
        event,
        id,
        field
      });
    }, 100);
  },

  watch: {
    json: function (newJson, oldJson) {
      this.setInitialState();
    },
    filters: function (newFilters, oldFilters) {
      this.filterJSON();
    },
    filtered: function (newFiltered, oldFiltered) {
      this.regmatchJSON();
    },
    regmatched: function (newRegmatched, oldRegmatched) {
      this.timSortJSON();
    },
    sortSpecifications: function (newSortSpecs, oldSortSpecs) {
      this.timSortJSON();
    },
    globalRegEx: function (newRegEx, oldRegEx) {
      this.regmatchJSON();
    }
  },
  computed: {
    uid() {
      return dist_36();
    },

    namespace() {
      return `v-records-table-${this.uid}`;
    },

    ids() {
      return Object.keys(this.json);
    },

    $fields() {
      return dist_18(this.json);
    },

    fields() {
      return this.fieldOrder.length === 0 ? this.$fields : this.fieldOrder;
    },

    $fieldRenderFunctions() {
      return { ...this.fieldRenderFunctions,
        ...this.extractorConfigFieldRenderFunctions
      };
    },

    $fieldFilterFunctions() {
      return { ...this.fieldFilterFunctions,
        ...this.extractorConfigFieldFilterFunctions
      };
    },

    $fieldSortByFunctions() {
      return { ...this.fieldSortByFunctions,
        ...this.extractorConfigFieldSortByFunctions
      };
    },

    tknFieldMap() {
      let map = {};
      this.fields.forEach(field => {
        map[field] = field;
      });
      Object.keys(this.fieldSynonymMap).forEach(humanField => {
        let field = this.fieldSynonymMap[humanField];
        map[humanField] = field;
      });
      return map;
    },

    fieldTokens() {
      return dist_11(this.tknFieldMap);
    },

    functionTokens() {
      return dist_11(this.tknFunctionMap);
    },

    conditionalTokens() {
      return dist_11(this.tknConditionalMap);
    },

    numberOfRecords() {
      return this.ids.length;
    },

    numberOfFilteredRecords() {
      return this.timsorted.length;
    },

    filtersGroupedByConjunctiveNormalForm() {
      return dist_3(this.filters);
    },

    hasExtractorConfig() {
      return !dist_34(this.extractorConfig);
    },

    headerFields() {
      return this.fields.map(field => {
        return dist_39(this.tknFieldMap, field); // if (field in this.tknFieldMap) return this.tknFieldMap[field]
        // return field as Field
      });
    },

    hasAtLeastOneFilter() {
      return this.filtersGroupedByConjunctiveNormalForm.length > 0 || this.globalRegEx !== '';
    }

  },
  methods: {
    updateTooltip({
      event,
      id,
      field
    }) {
      this.tooltipStyle = {
        left: `${event.pageX}px`,
        top: `${event.pageY}px`,
        position: 'absolute',
        zIndex: 2000,
        maxWidth: '300px',
        transition: 'all 0.5s ease '
      };

      if (id === null) {
        this.tooltipText = null;
        return;
      }

      let record = this.json[id];
      if (record === undefined) return;
      let value = record[field];
      if (value === undefined) return;
      let text = value.tooltip;
      if (text === undefined) return;
      this.tooltipText = text;
    },

    changeExtractorConfig(payload) {
      this.extractorConfigFieldRenderFunctions = payload.renderFunctions;
      this.extractorConfigFieldFilterFunctions = payload.filterFunctions;
      this.extractorConfigFieldSortByFunctions = payload.sortByFunctions;
      this.$emit('change:extractor-config', payload);
    },

    setInitialState() {
      let ids = Object.keys(this.json);
      this.filtered = ids;
      this.regmatched = ids;
      this.timsorted = ids;
      this.selectedIds = [...this.selected];
    },

    record(id) {
      return this.json[id];
    },

    fieldRenderFunction(field) {
      return dist_32(this.$fieldRenderFunctions, field, dist_31);
    },

    fieldSortByFunction(field) {
      return dist_32(this.$fieldSortByFunctions, field, dist_31);
    },

    fieldFilterFunction(field) {
      return dist_32(this.$fieldFilterFunctions, field, dist_31);
    },

    fieldRender(id, field) {
      return this.fieldRenderFunction(field)(id, field, this.record(id));
    },

    fieldSortBy(id, field) {
      return this.fieldSortByFunction(field)(id, field, this.record(id));
    },

    fieldFilter(id, field) {
      return this.fieldFilterFunction(field)(id, field, this.record(id));
    },

    setFilters({
      valid,
      error
    }) {
      this.filters = [...this.filters, ...valid.map(({
        filter
      }) => filter)];
      this.failedFilters = error.map(({
        filter
      }) => filter);
    },

    removeFilter(i) {
      let newFilters = [...this.filters];
      newFilters.splice(i, 1);
      this.filters = [...newFilters];
    },

    removeRegEx() {
      this.debounceRegEx('');
    },

    updateTimSort(newSortSpecifications) {
      this.sortSpecifications = [...newSortSpecifications];
    },

    toggleAllSelection() {
      if (this.selectAllModel === true) {
        this.selectedIds = this.timsorted;
      } else {
        this.selectedIds = [];
      }

      this.$emit('change:selected', this.selectedIds);
      return this.selectedIds;
    },

    headerToggleSortField(field) {
      this.toggleSortField(field);
      this.$emit('click:header', field);
      return field;
    },

    toggleSortField(field) {
      let newSortSpecs = dist_30(this.sortSpecifications, field);
      this.sortSpecifications = [...newSortSpecs];
    },

    fieldSortNumber(field) {
      return dist_28(this.sortSpecifications, field);
    },

    fieldSortDirection(field) {
      return dist_29(this.sortSpecifications, field);
    },

    fieldSortDirectionIcon(field) {
      let dir = this.fieldSortDirection(field);
      if (dir === undefined) return 'mdi-arrow-up';
      return dir ? 'mdi-arrow-up' : 'mdi-arrow-down';
    },

    inSelectedIds(id) {
      for (let i = 0; i < this.selectedIds.length; i++) {
        let other = this.selectedIds[i];
        if (id === other) return i;
      }

      return -1;
    },

    toggleSelect(id) {
      let inSelectedIds = this.inSelectedIds(id);

      if (inSelectedIds > -1) {
        this.selectedIds.splice(inSelectedIds, 1);
      } else {
        this.selectedIds.push(id);
      }

      this.$emit('change:selected', this.selectedIds);
      return this.selectedIds;
    },

    filterJSON() {
      if (this.filters.length === 0) {
        this.filtered = [...this.ids];
        return this.filtered;
      }

      this.isFiltering = true;
      this.status = 'Applying filters to data';
      this.filtered = [...dist_6(this.json, this.filters, this.cnfLogicConfig, this.cnfFunctionsConfig, this.cnfConditionalConfig, this.$fieldFilterFunctions)];
      this.isFiltering = false;
      this.status = '';
      this.$emit('change:filtered', this.filtered);
      return this.filtered;
    },

    regmatchJSON() {
      this.isRegmatching = true;
      this.status = 'Searching passing data for RegEX';
      let ids = this.filtered;

      if (this.globalRegEx === '') {
        this.regmatched = [...ids];
        this.status = '';
        return ids;
      }

      this.regmatched = [...dist_7(this.globalRegEx, this.json, ids, this.fields, this.$fieldFilterFunctions)];
      this.isRegmatching = false;
      this.status = '';
      this.$emit('change:regmatched', this.regmatched);
      return this.regmatched;
    },

    timSortJSON() {
      this.isTimSorting = true;
      this.status = 'Sorting remaining data';

      if (this.regmatched.length === 0) {
        this.timsorted = [];
      } else {
        this.timsorted = dist_27(this.json, this.sortSpecifications, this.regmatched, this.$fieldSortByFunctions, dist_25);
      }

      this.isTimSorting = false;
      this.status = '';
      this.$emit('change:timsort', this.timsorted);
      return this.timsorted;
    },

    clickRecord(event, item) {
      this.$emit('click:record', {
        event,
        id: item
      });
    },

    mousemoveRecord(event, item) {
      this.debounceMousemoveRecord({
        event,
        id: item
      });
    },

    mouseoverRecord(event, item) {
      this.$emit('mouseover:record', {
        event,
        id: item
      });
    },

    mouseleaveRecord(event, item) {
      this.debounceMousemoveRecord({
        event,
        id: null
      });
      this.$emit('mouseleave:record', {
        event,
        id: item
      });
    },

    clickRecordField(event, item, field) {
      this.$emit('click:record:field', {
        event,
        id: item,
        field
      });
    },

    mousemoveRecordField(event, item, field) {
      this.debounceMousemoveRecordField({
        event,
        id: item,
        field
      });
    },

    mouseoverRecordField(event, item, field) {
      this.debounceMousemoveRecordField({
        event,
        id: item,
        field
      });
      this.$emit('mouseover:record:field', {
        event,
        id: item,
        field
      });
    },

    mouseleaveRecordField(event, item, field) {
      this.debounceMousemoveRecordField({
        event,
        id: null,
        field
      });
      this.$emit('mouseleave:record:field', {
        event,
        id: item,
        field
      });
    }

  }
});

/* script */
const __vue_script__$8 = script$8;
/* template */

var __vue_render__$8 = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', {}, [_c('div', {
    staticClass: "v-records-table__tooltip",
    style: _vm.tooltipStyle
  }, [_vm._t("tooltip", [_vm.tooltipText !== null ? _c('v-card', [_c('v-card-text', [_vm._v("\n          " + _vm._s(_vm.tooltipText) + "\n        ")])], 1) : _vm._e()])], 2), _vm._v(" "), _c('v-card', {
    staticClass: "overflow-hidden",
    attrs: {
      "tile": "",
      "flat": ""
    }
  }, [_c('v-data-table', {
    attrs: {
      "loading": _vm.status !== '',
      "page": _vm.page,
      "items-per-page": _vm.recordsPerPage,
      "items": _vm.timsorted,
      "calculate-widths": true,
      "fixed-header": "",
      "headers-length": _vm.fields.length,
      "hide-default-header": "",
      "hide-default-footer": !_vm.showTable
    },
    on: {
      "update:items-per-page": function ($event) {
        _vm.recordsPerPage = $event;
      }
    },
    scopedSlots: _vm._u([{
      key: "top",
      fn: function () {
        return [_c('v-row', [_c('v-col', {
          attrs: {
            "cols": "12"
          }
        }, [_c('VTableProfileBtns', {
          staticClass: "mx-4",
          attrs: {
            "value": _vm.tableProfileIndex,
            "hasConfig": _vm.hasExtractorConfig
          },
          on: {
            "change": function ($event) {
              _vm.tableProfileIndex = $event === undefined ? 0 : $event;
            },
            "click:cog": function ($event) {
              _vm.configDrawerToggle = true;
            }
          }
        })], 1), _vm._v(" "), _c('div', [_vm._t("filters", [_vm.hasAtLeastOneFilter ? _c('v-col', {
          attrs: {
            "cols": "12"
          }
        }, [_c('VCnf', {
          staticClass: "mx-4",
          attrs: {
            "filters": _vm.filters,
            "fieldMap": _vm.fieldSynonymMap,
            "cnfLogicConfig": _vm.cnfLogicConfig,
            "cnfFunctionsConfig": _vm.cnfFunctionsConfig,
            "cnfConditionalConfig": _vm.cnfConditionalConfig,
            "regex": _vm.globalRegEx
          },
          on: {
            "click:close": _vm.removeFilter,
            "click:close:regex": _vm.removeRegEx
          }
        })], 1) : _vm._e()])], 2)], 1), _vm._v(" "), _c('v-row', [_c('v-col', {
          directives: [{
            name: "show",
            rawName: "v-show",
            value: _vm.tableProfileIndex === 0,
            expression: "tableProfileIndex===0"
          }],
          attrs: {
            "cols": "12"
          }
        }, [_c('VFrxtInput', {
          ref: "VFrxtInput",
          staticClass: "mx-4",
          attrs: {
            "tknFieldMap": _vm.tknFieldMap,
            "tknFunctionMap": _vm.tknFunctionMap,
            "tknConditionalMap": _vm.tknConditionalMap,
            "fieldTokens": _vm.fieldTokens,
            "functionTokens": _vm.functionTokens,
            "conditionalTokens": _vm.conditionalTokens,
            "PunctuationPattern": _vm.PunctuationPattern,
            "cnfLogicConfig": _vm.cnfLogicConfig,
            "cnfFunctionsConfig": _vm.cnfFunctionsConfig,
            "cnfConditionalConfig": _vm.cnfConditionalConfig,
            "debug": _vm.debug,
            "hint": "type statements in the form of 'logic field (function) test value'.",
            "label": "Free Text",
            "persistentHint": true,
            "appendIcon": "mdi-filter"
          },
          on: {
            "filters-from-text": _vm.setFilters
          }
        })], 1), _vm._v(" "), _c('v-col', {
          directives: [{
            name: "show",
            rawName: "v-show",
            value: _vm.tableProfileIndex === 1,
            expression: "tableProfileIndex===1"
          }],
          attrs: {
            "cols": "12"
          }
        }, [_c('VTimSort', {
          staticClass: "mx-4",
          attrs: {
            "sortSpecifications": _vm.sortSpecifications,
            "fieldMap": _vm.tknFieldMap
          },
          on: {
            "change": _vm.updateTimSort
          }
        })], 1), _vm._v(" "), _c('v-col', {
          directives: [{
            name: "show",
            rawName: "v-show",
            value: _vm.tableProfileIndex === 2,
            expression: "tableProfileIndex===2"
          }],
          attrs: {
            "cols": "12"
          }
        }, [_c('v-text-field', {
          staticClass: "mx-4",
          attrs: {
            "value": _vm.globalRegEx,
            "label": "Global Search",
            "clearable": "",
            "append-icon": "mdi-format-quote-close"
          },
          on: {
            "change": _vm.debounceRegEx,
            "input": _vm.debounceRegEx
          }
        })], 1)], 1)];
      },
      proxy: true
    }, {
      key: "progress",
      fn: function () {
        return [_c('v-row', {
          staticClass: "mx-4"
        }, [_c('v-col', [_vm._v("\n            Status: " + _vm._s(_vm.status) + "\n          ")])], 1), _vm._v(" "), _c('v-progress-linear', {
          attrs: {
            "color": "primary",
            "height": 10,
            "indeterminate": ""
          }
        })];
      },
      proxy: true
    }, {
      key: "header",
      fn: function (ref) {
        var headers = ref.props.headers;
        return [_vm.showTable ? _c('thead', [_c('tr', [_vm.showSelect ? _c('th', [_c('v-checkbox', {
          on: {
            "change": _vm.toggleAllSelection
          },
          model: {
            value: _vm.selectAllModel,
            callback: function ($$v) {
              _vm.selectAllModel = $$v;
            },
            expression: "selectAllModel"
          }
        })], 1) : _vm._e(), _vm._v(" "), _vm._l(_vm.fields, function (field, i) {
          return _c('VRecordsTableHeaderCell', {
            key: "header-" + field,
            attrs: {
              "field": field,
              "fieldText": _vm.headerFields[i],
              "fieldSortNumber": _vm.fieldSortNumber(field),
              "fieldSortIcon": _vm.fieldSortDirectionIcon(field)
            },
            on: {
              "click": _vm.headerToggleSortField
            }
          });
        })], 2)]) : _vm._e()];
      }
    }, {
      key: "item",
      fn: function (ref) {
        var item = ref.item;
        return [_vm.showTable ? _c('tr', {
          key: "item-" + item,
          on: {
            "click": function ($event) {
              return _vm.clickRecord($event, item);
            },
            "mousemove": function ($event) {
              return _vm.mousemoveRecord($event, item);
            },
            "mouseover": function ($event) {
              return _vm.mouseoverRecord($event, item);
            },
            "mouseleave": function ($event) {
              return _vm.mouseleaveRecord($event, item);
            }
          }
        }, [_vm.showSelect ? _c('td', [_c('v-checkbox', {
          attrs: {
            "input-value": _vm.inSelectedIds(item) > -1
          },
          on: {
            "change": function ($event) {
              return _vm.toggleSelect(item);
            }
          }
        })], 1) : _vm._e(), _vm._v(" "), _vm._l(_vm.fields, function (field) {
          return _c('td', {
            key: "item-" + item + "-" + field,
            domProps: {
              "innerHTML": _vm._s(_vm.fieldRender(item, field))
            },
            on: {
              "click": function ($event) {
                return _vm.clickRecordField($event, item, field);
              },
              "mousemove": function ($event) {
                return _vm.mousemoveRecordField($event, item, field);
              },
              "mouseover": function ($event) {
                return _vm.mouseoverRecordField($event, item, field);
              },
              "mouseleave": function ($event) {
                return _vm.mouseleaveRecordField($event, item, field);
              }
            }
          });
        })], 2) : _vm._e()];
      }
    }], null, true)
  }), _vm._v(" "), _vm.hasExtractorConfig ? _c('v-navigation-drawer', {
    attrs: {
      "temporary": "",
      "absolute": ""
    },
    model: {
      value: _vm.configDrawerToggle,
      callback: function ($$v) {
        _vm.configDrawerToggle = $$v;
      },
      expression: "configDrawerToggle"
    }
  }, [_c('v-list-item', [_c('v-list-item-content', [_c('v-list-item-title', [_vm._v("\n            Configuration Panel\n          ")])], 1)], 1), _vm._v(" "), _c('v-divider'), _vm._v(" "), _c('VExtractorConfig', {
    attrs: {
      "extractorConfig": _vm.extractorConfig,
      "functionLookup": _vm.functionLookup
    },
    on: {
      "change": _vm.changeExtractorConfig
    }
  })], 1) : _vm._e()], 1)], 1);
};

var __vue_staticRenderFns__$8 = [];
/* style */

const __vue_inject_styles__$8 = undefined;
/* scoped */

const __vue_scope_id__$8 = undefined;
/* module identifier */

const __vue_module_identifier__$8 = undefined;
/* functional template */

const __vue_is_functional_template__$8 = false;
/* style inject */

/* style inject SSR */

/* style inject shadow dom */

const __vue_component__$8 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$8,
  staticRenderFns: __vue_staticRenderFns__$8
}, __vue_inject_styles__$8, __vue_script__$8, __vue_scope_id__$8, __vue_is_functional_template__$8, __vue_module_identifier__$8, false, undefined, undefined, undefined);

/* eslint-disable import/prefer-default-export */

var components = /*#__PURE__*/Object.freeze({
  __proto__: null,
  VCnf: __vue_component__$2,
  VCnfChip: __vue_component__,
  VCnfOrGroup: __vue_component__$1,
  VExtractorConfig: __vue_component__$3,
  VFrxtInput: __vue_component__$4,
  VRecordsTable: __vue_component__$8,
  VRecordsTableHeaderCell: __vue_component__$7,
  VTableProfileBtns: __vue_component__$6,
  VTimSort: __vue_component__$5
});

// Import vue components
// eslint-disable-next-line @typescript-eslint/no-explicit-any

// install function executed by Vue.use()
const install = function installVFrxt(Vue) {
  if (install.installed) return;
  install.installed = true;
  Object.entries(components).forEach(([componentName, component]) => {
    Vue.component(componentName, component);
  });
}; // Create module definition for Vue.use()


const plugin = {
  install
}; // To auto-install on non-es builds, when vue is found

export default plugin;
export { __vue_component__$2 as VCnf, __vue_component__ as VCnfChip, __vue_component__$1 as VCnfOrGroup, __vue_component__$3 as VExtractorConfig, __vue_component__$4 as VFrxtInput, __vue_component__$8 as VRecordsTable, __vue_component__$7 as VRecordsTableHeaderCell, __vue_component__$6 as VTableProfileBtns, __vue_component__$5 as VTimSort };
