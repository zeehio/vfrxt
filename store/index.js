import Vue from 'vue'
import Vuex from 'vuex'


import state from '@/store/state'
import getters from '@/store/getters'
import actions from '@/store/actions'
import mutations from '@/store/mutations'

let isPages = process.env.NODE_ENV === 'GL_PAGES'


export const strict = false

export {
  mutations,
  actions,
  getters,
  state,
}
