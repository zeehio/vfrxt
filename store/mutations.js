import Vue from 'vue'
const mutations = {
  setIsFetching (state, bool) {
    state.isFetching = bool
  },
  incrementCardsFound (state, add) {
    state.cardsFound += add
  },
  setTotalCards (state, n) {
    state.totalCards = n
  },
  addCards (state, cards) {
    if (state.cards === null) state.cards = {}
    state.cards = {...state.cards, ...cards}
  },
  snackMessage (state, text) {
    state.snackModel = true
    state.snackText = text
  },
  setSnackModel (state, bool) {
    state.snackModel = bool
  },
  setTooltip (state, value) {
    if (value === null) {
      state.tooltipData = value
    } else {
      state.tooltipData = {...value}
    }
  }  
}

export default mutations
