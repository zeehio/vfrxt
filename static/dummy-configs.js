import {identity} from 'frxt'

export const fieldRenderFunctions = {
  array: (id, field, record) => {
    let n = id.replace('record', '')
    if (n % 2 === 0) return record[field]
    return `${record[field].length}-len array`
  },
  arrobj: (id, field, record) => {
    let n = id.replace('record', '')
    if (n % 2 === 0) return `${record[field].length}-len obj-array`
    let v = record[field][record[field].length-1].v
    return `last obj val ${v}`
  },
  url: (id, field, record) => {
    let n = id.replace('record', '')
    let u = `<a href="${record[field]}">link</a>`
    if (n % 2 === 0) return `${u}`
    return record[field]
  }
}

// set column names to something other than internal field name
export const fieldMap = {
  arrobj: 'Array of Objects',
  timsort1: 'TimSort',
}



// how the record table renders, filters and sorts fields can be
// dynamically set by the user if you provide the corresponding config

// first, we define some functions to work on the field "array"
const alternatingArray = (id, field, record) => {
  let n = id.replace('record', '')
  if (n % 2 === 0) return record[field]
  return `${record[field].length}-len array`
}
const lastOfArray = (id, field, record) => {
  let last = record[field][record[field].length-1]
  return `last el of arr is ${last}`
}
const lenOfArray = (id, field, record) => {
  return record[field].length
}

// then whe define a function that should return a function with signature
// (id, field, record)
const makeArrayFunction = (params)=>{
  let name = params.name
  if (name === 'alternatingArray') return alternatingArray
  if (name === 'lastOfArray') return lastOfArray
  if (name === 'lenOfArray') return lenOfArray
  return identity
}

// then we provide a function lookup
export const functionLookup = {
  makeArrayFunction
}

// lastly we define the config
export const extractorConfig = {
  array: { // <--- field which is configurable
    alternatingProfile: { // <-- option name
      renderConfig: { //<--- how it should render
        functionName: 'makeArrayFunction',
        params: { name: 'alternatingArray' }
      },
      filterConfig: { //<--- how it should filter
        functionName: 'makeArrayFunction',
        params: { name: 'identity' }
      },
      sortByConfig: { //<--- how it should sort
        functionName: 'makeArrayFunction',
        params: { name: 'lenOfArray' }
      }
    },
    lastElementProfile: {
      renderConfig: {
        functionName: 'makeArrayFunction',
        params: { name: 'lastOfArray' }
      },
      filterConfig: {
        functionName: 'makeArrayFunction',
        params: { name: 'identity' }
      },
      sortByConfig: {
        functionName: 'makeArrayFunction',
        params: { name: 'lenOfArray' }
      }
    }
  }
}
