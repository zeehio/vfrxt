Suppose you have the following filters:

```vue
import {configs} from 'frxt'

let filters = [
    {
      logic: 'and',
      function: 'identity',
      field: 'name',
      conditional: 'ss',
      input: 'ana'
    },
    {
      logic: 'or',
      function: 'identity',
      field: 'type',
      conditional: 'eq',
      input: 'fruit'
    },
    {
      logic: 'and',
      function: 'identity',
      field: 'price',
      conditional: 'lte',
      input: '3'
    },
  ]
<v-cnf
  :fieldMap="{name:'name', type:'type', price:'price'}"
  :filters="filters"
  :cnfLogicConfig="configs.defaultCNFLogicConfig"
  :cnfFunctionsConfig="configs.defaultCNFFunctionConfig"
  :cnfConditionalConfig="configs.defaultCNFConditionalConfig"
/>
```
